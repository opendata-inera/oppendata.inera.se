# Logstash filters
This folder contains filters used by logstash to parse log posts. Using logstash filters allows the Kibana application to filter the log posts on various parameters such as host and error codes.
For more information about logstash, see https://www.elastic.co/guide/en/logstash/current/index.html. 

## Directory structure

| path  | description   |
|---|---|
| ckan/  | Contains the logstash filter used to parse CKAN log posts |
| mirth/ | Contains the logstash filer used to parse Mirth Connect log posts |

## Usage
The logstash-filter file should be placed in the /etc/logstash/conf.d directory on the server where the CKAN or Mirth Connect application is running.