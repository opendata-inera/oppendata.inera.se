# Inera Jmeter tests #

This README documents how to run Jmeter to test API calls in the Inera implementation of Mirth.
Jmeter is a java application used mainly to perform load testing.
These Jmeter tests are used to test if Inera's Mirth implementation can handle the expected amount of requests and to facilitate calls to multiple APIs. 

## Jmeter version compatibility
Describes what versions of Jmeter the test plans are compatible with. 

| version | tested | working | notes |
|---|---|---|---|
| 3.2 | yes | yes |- |

## Plugins
The following plugins are recommended and can be installed using the Plugin Manager.

|plugin|notes|
|---|---|
|3 Basic Graphs||
|5 Additional Graphs||
|Custom Thread Groups||

## Directory structure

| path  | description   |
|---|---|
| ~~input_files~~  | ~~Folder containing the text files containing data used for the API calls to Mirth that require an ID.~~ Not used for current test plans. IDs are now defined in the test plan's Global variables. |
| test_runs | Folder to store reports generated after a jmeter test run. Used by the 'Simple Data Writer' when running jmeter in GUI mode. |
| *.jmx | The various Jmeter test plans used to send HTTP requests to Mirth. The names of the test plans should preferably reflect the version of the production environment. The version can be found by looking at the tags in the Oppendata Bitbucket-repo. 
| README.md | This file |

## Keep in mind
1. Jmeter will reference the folder where Inera.jmx is located as the base folder. Therefore, do not move the Inera.jmx file to any other destination without moving the subfolders input_files and test_runs as well.
2. Do not change the names of the text files in the input_files folder since that will prevent Jmeter to find them. 
3. Test run files can get pretty big, so only commit test runs that might be of interest for others. Otherwise move them from the test_runs folder to a local folder of your choice.

## Getting started
Jmeter is platform independent meaning that it will run on (theoretically) any platform.
1. Make sure that you have Java 8 or later installed, otherwise download it from http://www.oracle.com/technetwork/java/javase/downloads/index.html. 
2. Download the latest version of Jmeter from http://jmeter.apache.org/download_jmeter.cgi to a destination of your choice.
3. Install any plugins necessary (see the Plugins section above for more information).

## Starting the Jmeter GUI ##
For creating tests and debugging them you should use the Jmeter GUI. However, for load testing it is strongly advised to run the tests from the command line (see below).
1. Open the bin folder located in the Jmeter base folder.
2. Run the jmeter executable file.
3. Load the Inera.jmx test plan.

## Running the tests from the command line ##
All load testing should be done using the Non-GUI version of Jmeter by running the test plan from the command line.
1. In the Jmeter GUI, make sure to disable all listeners (File writers, Result Listeners etc.).
2. Close the Jmeter GUI.
3. Open the terminal and go to the bin folder in the Jmeter base folder.
4. The following parameters need to be specified when running the test plan:
   TEST_PLAN_PATH = path to the Inera.jmx file (e.g. C:/Inera/Inera.jmx).
   LOG_PATH = path to the log file generated after the test plan has finished (e.g. C:/Inera/test_runs/test_run_1.csv).
   REPORT_PATH = path to the directory where the report files will be generated. The directory should not exist or be empty. 
   The report files contains aditional statistics of the test run. (e.g. C:/Inera/test_runs/test_run_1_report).
5. Run the following command and the tests will start running:
   Windows: 	`jmeter -n -t TEST_PLAN_PATH -l LOG_PATH -e -o REPORT_PATH`
   Unix:		`./jmeter -n -t TEST_PLAN_PATH -l LOG_PATH -e -o REPORT_PATH`

## Stopping the test run from the command line ##
1. Open a new terminal window.
2. Go to the bin folder in the Jmeter base folder.
3. Run the following command:
   Windows: `stoptest.cmd`
   Unix: `./stoptest.sh`
4. The test run will now stop and the log file and report files will be generated.

## Getting around the proxy 
1. Open ODP_Version2.1.jmx (or whichever is the latest version)
2. Go to HTTP Request Defaults
3. Press the Advanced tab
4. In the Proxy Server Name or IP field insert stoc.proxy.corp.ssg, in the Port Number field, insert 8080
5. In the Username and Password fields, insert your company username and password.