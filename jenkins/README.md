# README #

This README would normally document whatever steps are necessary to get your application up and running.

## What is this repository for? ##

This repository is the beginning steps of setting up Jenkins for ODP, to be concluded

## Topics ##

* Getting around issues

## Getting around issues ##

* The first thing you need to do after having downloaded Jenkins is open a command prompt and browse to the Jenkins directory and run the command "java –jar Jenkins.war". After the command is run, various tasks will run, one of which is the extraction of the war file which is done by an embedded webserver called winstone. This command should finish with "INFO: Jenkins is fully up and running" at the bottom. All too often though, this is not the case. If you're getting errors doing this, the only solution thus far seems to be to rebooting the computer and running the same command again. It took me about five tries before it worked. Another issue you will run into is the fact that the proxy will leave you with errors when you try to download the plugins at the Jenkins introduction screen at localhost:8080. Remember, Jenkins will not run if you have anything running on port 8080, which is normally blocked by Mirth Connect, so remember to stop Mirth in order to run Jenkins. It seems like you simply can't download the plugins at the introduction screen when you first start Jenkins if you're behind a proxy. This is a known Jenkins issue and there seems to be no solution to this. Instead, continue with the Jenkins registration and once you're at the Jenkins dashboard, click on "Manage Jenkins" and then on "Manage plugins". Once there, click on the "Advanced" tab. The server should be "stoc.proxy.corp.ssg" and the port 8080. User name and password is the same username and password as the Jenkins account you created. The available plugins can be found under the "Available" tab, but it seems like not all plugins can be installed, even though they are listed as available. More to follow.