# Upgrading CKAN 2.6 to 2.8 #
This document contains information regarding what steps need to be performed when/after upgrading CKAN from 2.6.2 to version 2.8.0.  

## Table of Contents ##
1. Upgrading
2. After upgrading
3. Testing
4. Known bugs

## 1. Upgrading ##
1. Take a look at the [release notes for 2.8.0](http://docs.ckan.org/en/2.8/changelog.html#v-2-8-0-2018-05-09) and perform any steps necessary. 
2. Follow the steps in the [CKAN upgrade guide](http://docs.ckan.org/en/2.8/maintaining/upgrading/index.html#upgrading).

## 2. After upgrading ##

### 2.1 Modify `postgres.py` ###
The `/lib/ckan/default/src/ckan/ckanext/datastore/backend/postgres.py` file by default parses the JSON data returned from CKAN using a `LazyJSONObject`. The problem is that this library is not compatible with the `simplejson` library included in the CKAN install.
This results in intermittent server errors when trying to retrieve data from the CKAN datastore, and will print an error of the following type in the logs:

`[Wed Jul 04 14:34:30.598321 2018] [wsgi:error] [pid 28403] [remote 10.250.23.0:36652] TypeError: <LazyJSONObject u'[{"_id":1,"hsaId":"SE162321000222","name":"Rikske"},{"_id":2,"hsaId":"SE2321000131","name":null},{"_id":3,"hsaId":"T_SERVICES_-10FH","name":"SIGNE"}]'> is not JSON serializable`

To fix this bug, modify line 1273 in the `postgres.py` file accordingly:
From: `records = LazyJSONObject(v)`
To: `records = json.loads(v)`

Reference to this solution can be found [here](https://github.com/ckan/ckan/issues/4299).

### 2.2 Re-run `datastore set-permissions` 
As explained in the [release notes for 2.8.0](http://docs.ckan.org/en/2.8/changelog.html#v-2-8-0-2018-05-09):

>This version requires re-running the datastore set-permissions command (assuming you are using the DataStore). See: [Set permissions](http://docs.ckan.org/en/2.8/maintaining/datastore.html#datastore-set-permissions). 
Otherwise new and updated datasets will not be searchable in DataStore and the logs will contain this error:
`ProgrammingError: (psycopg2.ProgrammingError) function populate_full_text_trigger() does not exist`.
>

If Basefarm are responsible for upgrading the CKAN version, they should set permissions by running the following commands:
1. `paster --plugin=ckan datastore set-permissions -c /etc/ckan/default/production.ini > output.sql`
2. `cat output.sql | psql -U postgres -W -h {POSTGRESQL_URL}`
Example command for TEST environment: 
`cat output.sql | psql -U postgres -W -h ine-tib-pgsql1.in1`

### 2.3 CKAN website ###

#### 2.3.1 Make datasets public ####
If none of the datasets in CKAN are visible on the website, make sure that they have been set to public. It seems that the visibility is not kept between versions.
1. Log in with an admin account
2. Click the **Organizations** button and then choose the organization that you want to update.
3. Click the **Manage** button.
4. Click the **Datasets** tab.
5. Mark all the datasets that you want to make public, then click the **Make public** button.

### 3. Testing ###
Test all the CKAN APIs that are used in the Opendata platform. See the [CKAN API guide](http://docs.ckan.org/en/2.8/api/index.html) for details regarding what APIs are available and what data, parameters etc. they accept.
The main APIs used in the Opendata platform are:
* `datastore_search` 
* `datastore_create`
* `resource_search`
* `resource_delete`

A good start is to post some data to CKAN by calling the `datastore_create` API, and verifying that the call returns the statuscode 200. After that, try to retrieve some data using the `datastore_search` API and verifying that the returned data returned is the expected.    
Continue by trying to delete the data and the resource by using the `resource_delete` API (CKAN 2.8.0 by default deletes the data in the datastore when deleting the resource, which was not the case in 2.6.2). There should therefore be no need to explicitly call the `datastore_delete` API after upgrading.

Also verify that the CKAN website is working correctly, by logging in and navigating through the different pages, creating a new dataset, deleting a dataset, creating a new user etc.

### 4. Known bugs ###

#### 4.1 CKAN website ####

##### 4.1.1 Erroneous link in Dataset #####
Clicking on the **Datasets** button in the top-bar brings you to the Datasets page. The **Datasets** button in the top left corner of the the screen, under the Inera logo should reload the current page, but instead refers to an erroneous URL: `/dataset_search?action=search&controller=package`. 