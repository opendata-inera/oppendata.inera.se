package se.inera.oppendata.mirth.ntjp.testing;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import se.inera.oppendata.mirth.ntjp.utility.EnvironmentHandler;
import se.inera.oppendata.mirth.ntjp.utility.FileHandler;
import se.inera.oppendata.mirth.ntjp.utility.JsonUtility;
import se.inera.oppendata.mirth.ntjp.utility.MirthEnvironment;
import se.inera.oppendata.mirth.rest.CustomResponse;
import se.inera.oppendata.mirth.rest.RestClient;
import se.inera.oppendata.mirth.rest.StatusCodes;
import se.inera.oppendata.mirth.ssl.SSLContextCreator;

import javax.net.ssl.SSLContext;
import java.util.HashMap;

/**
 * Created by davsan on 2018-01-29.
 */
public class TestGetRealValues {

    private static MirthEnvironment environment = MirthEnvironment.test;

    @Test
    public static void shouldGetRealMeasurement(){
        RestClient restClient = CreateRestClient();
        EnvironmentHandler environmentHandler = new EnvironmentHandler(environment);
        String url = environmentHandler.getMirthMeasurementsUrl();
        FileHandler fileHandler = new FileHandler(environmentHandler);
        String measurement = fileHandler.getRealMeasurement();
        JSONObject measurementJson = JsonUtility.parse(measurement);
        String measurementId = (String)measurementJson.get("measurementId");
        CustomResponse response = restClient.get(url + '/' + measurementId);
        Assert.assertEquals(response.status, StatusCodes.OK);
        JsonUtility.assertEquals(measurement, response.body);
    }

    @Test
    public static void shouldGetRealAggregatedQualityReport(){
        RestClient restClient = CreateRestClient();
        EnvironmentHandler environmentHandler = new EnvironmentHandler(environment);
        String url = environmentHandler.getAggregatedQualityReportsUrl();
        FileHandler fileHandler = new FileHandler(environmentHandler);
        String report = fileHandler.getRealAggregatedQualityReport();
        JSONObject reportJson = JsonUtility.parse(report);
        String aggregatedQualityReportId = (String)reportJson.get("aggregatedQualityReportId");
        CustomResponse response = restClient.get(url + '/' + aggregatedQualityReportId);
        Assert.assertEquals(response.status, StatusCodes.OK);
        JsonUtility.assertEquals(report, response.body);
    }

    @Test
    public static void shouldGetRealPerformingOrganization(){
        RestClient restClient = CreateRestClient();
        EnvironmentHandler environmentHandler = new EnvironmentHandler(environment);
        String url = environmentHandler.getPerformingOrganizationsUrl();
        FileHandler fileHandler = new FileHandler(environmentHandler);
        String performingOrganization = fileHandler.getRealPerformingOrganization();
        JSONObject performingOrganizationJson = JsonUtility.parse(performingOrganization);
        String performingOrganizationId = (String)performingOrganizationJson.get("performingOrganizationId");
        CustomResponse response = restClient.get(url + '/' +performingOrganizationId);
        Assert.assertEquals(response.status, StatusCodes.OK);
        JsonUtility.assertEquals(performingOrganization, response.body);
    }


    public static void shouldGetRealAggregatedQualityReportsForYear2018(){
        RestClient restClient = CreateRestClient();
        EnvironmentHandler environmentHandler = new EnvironmentHandler(environment);
        String url = environmentHandler.getAggregatedQualityReportsUrl();
        FileHandler fileHandler = new FileHandler(environmentHandler);
        String reports = fileHandler.getRealAggregatedQualityReports2018();

        HashMap<String, String> params = new HashMap();
        params.put("reportingPeriodFrom", "2018-01-01");

        CustomResponse response = restClient.get(url, null, params);
        Assert.assertEquals(response.status, StatusCodes.OK);
        JsonUtility.assertEquals(reports, response.body);
    }

    private static RestClient CreateRestClient(){
        SSLContext sslContext = SSLContextCreator.createSSLContext(true);
        RestClient restClient = new RestClient(sslContext);
        return restClient;
    }
}
