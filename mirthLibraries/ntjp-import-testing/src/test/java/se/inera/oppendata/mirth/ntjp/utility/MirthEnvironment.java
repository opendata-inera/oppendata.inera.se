package se.inera.oppendata.mirth.ntjp.utility;

/**
 * Created by davsan on 2018-06-05.
 */
public enum MirthEnvironment {
    none,
    test,
    qa,
    prod
}
