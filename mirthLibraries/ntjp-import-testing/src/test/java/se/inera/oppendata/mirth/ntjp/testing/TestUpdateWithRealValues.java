package se.inera.oppendata.mirth.ntjp.testing;

import org.testng.Assert;
import org.testng.annotations.Test;
import se.inera.oppendata.mirth.ntjp.utility.EnvironmentHandler;
import se.inera.oppendata.mirth.ntjp.utility.FileHandler;
import se.inera.oppendata.mirth.ntjp.utility.MirthEnvironment;
import se.inera.oppendata.mirth.rest.CustomResponse;
import se.inera.oppendata.mirth.rest.RestClient;
import se.inera.oppendata.mirth.rest.StatusCodes;
import se.inera.oppendata.mirth.ssl.SSLContextCreator;

import javax.net.ssl.SSLContext;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by davsan on 2018-01-29.
 */
public class TestUpdateWithRealValues {
    private static MirthEnvironment environment = MirthEnvironment.test;

    @Test
    public static void shouldUpdateMeasurementsWithRealValue(){
        RestClient restClient = CreateRestClient();
        EnvironmentHandler environmentHandler = new EnvironmentHandler(environment);
        String url = environmentHandler.getMeasurementsUpdateUrl();
        FileHandler fileHandler = new FileHandler(environmentHandler);
        String measurement = "["+fileHandler.getRealMeasurement()+"]";
        CustomResponse response = restClient.post(url, measurement, environmentHandler.getUpdateHeaders(), null);
        Assert.assertEquals(response.status, StatusCodes.OK);
    }

    @Test
    public static void shouldUpdateAggregatedQualityReportWithRealValue(){
        RestClient restClient = CreateRestClient();
        EnvironmentHandler environmentHandler = new EnvironmentHandler(environment);
        String url = environmentHandler.getAggregatedQualityReportsUpdateUrl();
        FileHandler fileHandler = new FileHandler(environmentHandler);
        String report = "["+fileHandler.getRealAggregatedQualityReport()+"]";
        CustomResponse response = restClient.post(url, report, environmentHandler.getUpdateHeaders(), null);
        Assert.assertEquals(response.status, StatusCodes.OK);
    }

    @Test
    public static void shouldUpdatePerformingOrganizationsWithRealValue(){
        RestClient restClient = CreateRestClient();
        EnvironmentHandler environmentHandler = new EnvironmentHandler(environment);
        String url = environmentHandler.getPerformingOrganizationsUpdateUrl();
        FileHandler fileHandler = new FileHandler(environmentHandler);
        String performingOrganization = "["+fileHandler.getRealPerformingOrganization()+"]";
        CustomResponse response = restClient.post(url, performingOrganization, environmentHandler.getUpdateHeaders(), null);
        Assert.assertEquals(response.status, StatusCodes.OK);
    }


    public static void shouldUpdateAggregatedQualityReportsForYear2018WithRealValues(MirthEnvironment environment){
        SSLContext sslContext = SSLContextCreator.createSSLContext(true);
        RestClient restClient = new RestClient(sslContext);
        EnvironmentHandler environmentHandler = new EnvironmentHandler(environment);
        String url = environmentHandler.getAggregatedQualityReportsUpdateUrl();
        FileHandler fileHandler = new FileHandler(environmentHandler);
        String reports = fileHandler.getRealAggregatedQualityReports2018();
        CustomResponse response = restClient.post(url, reports, environmentHandler.getUpdateHeaders(), null);
        Assert.assertEquals(response.status, StatusCodes.OK);
    }


    private static RestClient CreateRestClient(){
        SSLContext sslContext = SSLContextCreator.createSSLContext(true);
        RestClient restClient = new RestClient(sslContext);
        return restClient;
    }
}
