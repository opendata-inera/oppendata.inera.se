package se.inera.oppendata.mirth.ntjp.testing;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.skyscreamer.jsonassert.comparator.JSONCompareUtil;
import org.testng.Assert;
import org.testng.annotations.Test;
import se.inera.oppendata.mirth.ntjp.comparer.OldPlatformEnvironmentHandler;
import se.inera.oppendata.mirth.ntjp.utility.EnvironmentHandler;
import se.inera.oppendata.mirth.ntjp.utility.JsonUtility;
import se.inera.oppendata.mirth.ntjp.utility.MirthEnvironment;
import se.inera.oppendata.mirth.rest.CustomResponse;
import se.inera.oppendata.mirth.rest.RestClient;
import se.inera.oppendata.mirth.rest.StatusCodes;
import se.inera.oppendata.mirth.ssl.SSLContextCreator;

import javax.net.ssl.SSLContext;
import java.util.HashMap;

/**
 * Created by davsan on 2018-01-29.
 */
public class TestCompareNumberOfMeasurementsWithOtherPlatform {

    private static String reportingPeriodFrom = "2018-01-01";

    @Test
    public static void shouldHaveSameAggregatedQualityReports(){
        RestClient restClient = createRestClient();
        EnvironmentHandler qaEnvHandler = new EnvironmentHandler(MirthEnvironment.qa);
        EnvironmentHandler prodEnvHandler = new EnvironmentHandler(MirthEnvironment.prod);

        HashMap<String, String> params = new HashMap();
        params.put("fields", "measureId,aggregatedQualityReportId");

        CustomResponse respQa = restClient.get(qaEnvHandler.getAggregatedQualityReportsUrl(), null, params);
        Assert.assertEquals(respQa.status, StatusCodes.OK);

        CustomResponse respProd = restClient.get(prodEnvHandler.getAggregatedQualityReportsUrl(), null, params);
        Assert.assertEquals(respProd.status, StatusCodes.OK);
        JsonUtility.assertEquals(respQa.body, respProd.body);
    }

    @Test
    public static void shouldHaveSameNumberOfMeasurementsPerAggregatedQualityReportForASpecificYear(){
        RestClient restClient = createRestClient();
        EnvironmentHandler qaEnvHandler = new EnvironmentHandler(MirthEnvironment.qa);
        EnvironmentHandler prodEnvHandler = new EnvironmentHandler(MirthEnvironment.prod);

        HashMap<String, String> params = new HashMap();
        params.put("fields", "measureId,aggregatedQualityReportId");
        params.put("reportingPeriodFrom", reportingPeriodFrom);

        CustomResponse respQa = restClient.get(qaEnvHandler.getAggregatedQualityReportsUrl(), null, params);
        Assert.assertEquals(respQa.status, StatusCodes.OK);

        CustomResponse respProd = restClient.get(prodEnvHandler.getAggregatedQualityReportsUrl(), null, params);
        Assert.assertEquals(respProd.status, StatusCodes.OK);
        JsonUtility.assertEquals(respQa.body, respProd.body);
        JSONArray prodReports = getPosts(respProd);

        params.clear();
        params.put("fields","measurementId");
        params.put("limit", "1");
        long totalDiff = 0;
        JSONArray totalDiffJson = new JSONArray();
        for(int i = 0; i < prodReports.size(); i++){
            System.out.println("Comparing... "+(i+1)+"/"+prodReports.size());
            JSONObject report = (JSONObject)prodReports.get(i);
            String reportId = report.get("aggregatedQualityReportId").toString();
            String measureId = report.get("measureId").toString();
            params.put("aggregatedQualityReportId", reportId);
            params.put("measureId", measureId);

            CustomResponse qaMeasurementsResp = restClient.get(qaEnvHandler.getMirthMeasurementsUrl(), null, params);
            Assert.assertEquals(qaMeasurementsResp.status, StatusCodes.OK);

            CustomResponse prodMeasurementsResp = restClient.get(prodEnvHandler.getMirthMeasurementsUrl(), null, params);
            Assert.assertEquals(prodMeasurementsResp.status, StatusCodes.OK);

            JSONObject qaJson = JsonUtility.parse(qaMeasurementsResp.body);
            JSONObject prodJson = JsonUtility.parse(prodMeasurementsResp.body);

            long qaTotal = qaJson.get("total") == null ? 0 : (long)qaJson.get("total");
            long prodTotal = prodJson.get("total") == null ? 0 : (long)prodJson.get("total");
            if(qaTotal != prodTotal){
                System.out.println("Number of measurements for aggregatedQualityReportId " + reportId + " not equal.");
                long diff = Math.abs(qaTotal - prodTotal);
                System.out.println("Diff: "+diff);
                totalDiff += diff;
                JSONObject newJson = new JSONObject();
                newJson.put("reportId", reportId);
                newJson.put("qaTotal", qaTotal);
                newJson.put("prodTotal", prodTotal);
                newJson.put("diff", diff);
                totalDiffJson.add(newJson);
            }

        }
        if(!totalDiffJson.isEmpty()){
            System.out.println(totalDiffJson.toJSONString());
        }
        Assert.assertEquals(totalDiff, 0);
    }


    public static void checkWhatMeasurementsAreMissing(){
        String attributeToCompare = "measurementId";
        String aggregatedQualityReportId = "d5b1a3fe44a214a96fcc3530431a48b6";
        String measureId = "43f08212-9015-4369-a7ac-9dfd9ed939af";
        RestClient restClient = createRestClient();
        EnvironmentHandler qaEnvHandler = new EnvironmentHandler(MirthEnvironment.qa);
        EnvironmentHandler prodEnvHandler = new EnvironmentHandler(MirthEnvironment.prod);
        JSONArray qaMeasurements = getMeasurements(qaEnvHandler, aggregatedQualityReportId, measureId, attributeToCompare);
        JSONArray prodMeasurements = getMeasurements(prodEnvHandler, aggregatedQualityReportId, measureId, attributeToCompare);
        JSONArray missingFromQa = getMissingValues(prodMeasurements, qaMeasurements, attributeToCompare);
        JSONArray missingFromProd = getMissingValues(qaMeasurements, prodMeasurements, attributeToCompare);
        printMissingValues(missingFromQa, missingFromProd);
    }

    private static JSONArray getMeasurements(EnvironmentHandler environmentHandler, String aggregatedQualityReportId, String measureId, String attributeToCompare){
        HashMap<String, String> params = new HashMap();
        params.put("aggregatedQualityReportId", aggregatedQualityReportId);
        params.put("measureId", measureId);
        params.put("fields", attributeToCompare);

        SSLContext sslContext = SSLContextCreator.createSSLContext(true);
        RestClient restClient = new RestClient(sslContext);
        CustomResponse response = restClient.get(environmentHandler.getMirthMeasurementsUrl(), null, params);
        Assert.assertEquals(response.status, StatusCodes.OK);
        JSONArray measurements = JsonUtility.parseToArray(response.body);
        System.out.println("Found "+measurements.size()+ " measurements for report: "+aggregatedQualityReportId+" and measureId: "+measureId);
        return measurements;
    }

    private static JSONArray getMissingValues(JSONArray arrayWithValues, JSONArray arrayToSearch, String attributeToSearchFor){
        JSONArray missingValues = new JSONArray();
        for(int i = 0; i < arrayWithValues.size(); i++){
            JSONObject entry1 = (JSONObject)arrayWithValues.get(i);
            Assert.assertNotNull(entry1.get(attributeToSearchFor));
            Object value1 = entry1.get(attributeToSearchFor);
            boolean valueFound = false;

            for(int j = 0; j < arrayToSearch.size(); j++){
                JSONObject entry2 = (JSONObject)arrayToSearch.get(j);
                Assert.assertNotNull(entry2.get(attributeToSearchFor));
                Object value2 = entry2.get(attributeToSearchFor);
                if(value1.equals(value2)){
                    valueFound = true;
                    break;
                }
            }
            if(!valueFound){
                missingValues.add(value1.toString());
            }
        }
        return missingValues;
    }

    private static void printMissingValues(JSONArray missingFromQa, JSONArray missingFromProd){
        System.out.println("---------------MISSING FROM QA-----------------");
        System.out.println(missingFromQa.toJSONString());
        System.out.println("---------------MISSING FROM PROD-----------------");
        System.out.println(missingFromProd.toJSONString());
    }

    private static RestClient createRestClient(){
        SSLContext sslContext = SSLContextCreator.createSSLContext(true);
        RestClient restClient = new RestClient(sslContext);
        return restClient;
    }

    private static JSONArray getPosts(CustomResponse response){
        JSONArray posts = JsonUtility.parseToArray(response.body);
        return posts;
    }
}
