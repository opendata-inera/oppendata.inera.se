package se.inera.oppendata.mirth.ntjp.comparer;

/**
 * Created by davsan on 2017-11-28.
 */
public class OldPlatformEnvironmentHandler {

    private static OldPlatformEnvironment ENVIRONMENT = OldPlatformEnvironment.QA;

    private static final String QA_BASE_PATH = "http://opendata-qa.jelastic.elastx.net/";
    private static final String PROD_BASE_PATH = "http://opendata-qa.jelastic.elastx.net/";
    private static final String REPORTING_SUFFIX = "followup-groupoutcomes-qualityreporting/v2/";
    private static final String MEASURES_SUFFIX = "followup-groupoutcomes-qualitymeasures/v2/";
    private static final String PERFORMING_ORGANIZATIONS_ENDPOINT = "performingOrganizations/";
    private static final String AGGREGATED_QUALITY_REPORTS_ENDPOINT = "aggregatedQualityReports/";

    public enum OldPlatformEnvironment{
        QA,
        PROD
    }

    public enum ServiceType{
        REPORTING,
        MEASURES
    }

    public static void setEnvironment(OldPlatformEnvironment environment){
        ENVIRONMENT = environment;
    }

    public static String getAggregatedQualityReportsUrl(){
        return createFullUrl(AGGREGATED_QUALITY_REPORTS_ENDPOINT, ServiceType.REPORTING);
    }

    public static String getPerformingOrganizationsUrl(){
        return createFullUrl(PERFORMING_ORGANIZATIONS_ENDPOINT, ServiceType.REPORTING);
    }

    private static String createFullUrl(String endpoint, ServiceType serviceType){
        return getUrlWithServiceType(serviceType) + endpoint;
    }

    private static String getUrlWithServiceType(ServiceType serviceType){
        if(serviceType == ServiceType.REPORTING){
            return getBaseUrl() + REPORTING_SUFFIX;
        }else if(serviceType == ServiceType.MEASURES){
            return getBaseUrl() + MEASURES_SUFFIX;
        }else{
            return "";
        }
    }

    private static String getBaseUrl(){
        if(ENVIRONMENT == OldPlatformEnvironment.QA){
            return QA_BASE_PATH;
        }else if(ENVIRONMENT == OldPlatformEnvironment.PROD){
            return PROD_BASE_PATH;
        }else{
            return "";
        }
    }

}


