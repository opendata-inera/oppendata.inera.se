package se.inera.oppendata.mirth.ntjp.testing;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import se.inera.oppendata.mirth.ntjp.utility.EnvironmentHandler;
import se.inera.oppendata.mirth.ntjp.utility.FileHandler;
import se.inera.oppendata.mirth.ntjp.utility.JsonUtility;
import se.inera.oppendata.mirth.ntjp.utility.MirthEnvironment;
import se.inera.oppendata.mirth.rest.CustomResponse;
import se.inera.oppendata.mirth.rest.RestClient;
import se.inera.oppendata.mirth.rest.StatusCodes;
import se.inera.oppendata.mirth.ssl.SSLContextCreator;

import javax.net.ssl.SSLContext;
import java.util.HashMap;

/**
 * Created by davsan on 2018-01-29.
 */
public class TestGetFakeValues {

    private static MirthEnvironment environment = MirthEnvironment.test;

    @Test
    public static void shouldGetFakeMeasurement(){
        RestClient restClient = CreateRestClient();
        EnvironmentHandler environmentHandler = new EnvironmentHandler(environment);
        String url = environmentHandler.getMirthMeasurementsUrl();
        FileHandler fileHandler = new FileHandler(environmentHandler);
        String measurement = fileHandler.getFakeMeasurement();
        JSONObject measurementJson = JsonUtility.parse(measurement);
        String measurementId = (String)measurementJson.get("measurementId");
        CustomResponse response = restClient.get(url + '/' + measurementId);
        Assert.assertEquals(response.status, StatusCodes.OK);
        JsonUtility.assertEquals(measurement, response.body);
    }

    @Test
    public static void shouldGetFakeAggregatedQualityReport(){
        RestClient restClient = CreateRestClient();
        EnvironmentHandler environmentHandler = new EnvironmentHandler(environment);
        String url = environmentHandler.getAggregatedQualityReportsUrl();
        FileHandler fileHandler = new FileHandler(environmentHandler);
        String report = fileHandler.getFakeAggregatedQualityReport();
        JSONObject reportJson = JsonUtility.parse(report);
        String aggregatedQualityReportId = (String)reportJson.get("aggregatedQualityReportId");
        CustomResponse response = restClient.get(url + '/' + aggregatedQualityReportId);
        Assert.assertEquals(response.status, StatusCodes.OK);
        JsonUtility.assertEquals(report, response.body);
    }

    @Test
    public static void shouldGetFakePerformingOrganization(){
        RestClient restClient = CreateRestClient();
        EnvironmentHandler environmentHandler = new EnvironmentHandler(environment);
        String url = environmentHandler.getPerformingOrganizationsUrl();
        FileHandler fileHandler = new FileHandler(environmentHandler);
        String performingOrganization = fileHandler.getFakePerformingOrganization();
        JSONObject performingOrganizationJson = JsonUtility.parse(performingOrganization);
        String performingOrganizationId = (String)performingOrganizationJson.get("performingOrganizationId");
        CustomResponse response = restClient.get(url + '/' +performingOrganizationId);
        Assert.assertEquals(response.status, StatusCodes.OK);
        JsonUtility.assertEquals(performingOrganization, response.body);
    }


    public static void shouldGetFakeAggregatedQualityReportsForYear2018(){
        RestClient restClient = CreateRestClient();
        EnvironmentHandler environmentHandler = new EnvironmentHandler(environment);
        String url = environmentHandler.getAggregatedQualityReportsUrl();
        FileHandler fileHandler = new FileHandler(environmentHandler);
        JSONArray reports = JsonUtility.parseToArray(fileHandler.getRealAggregatedQualityReports2018());
        for(int i = 0; i < reports.size(); i++){
            JSONObject report = (JSONObject) reports.get(i);
            report.put("measurementChecksum", "fakevalue");
        }
        String reportsJsonString = reports.toJSONString();

        HashMap<String, String> params = new HashMap();
        params.put("reportingPeriodFrom", "2018-01-01");

        CustomResponse response = restClient.get(url, null, params);
        Assert.assertEquals(response.status, StatusCodes.OK);
        JsonUtility.assertEquals(reportsJsonString, response.body);
    }

    private static RestClient CreateRestClient(){
        SSLContext sslContext = SSLContextCreator.createSSLContext(true);
        RestClient restClient = new RestClient(sslContext);
        return restClient;
    }
}
