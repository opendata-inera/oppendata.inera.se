package se.inera.oppendata.mirth.ntjp.comparer;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import se.inera.oppendata.mirth.ntjp.utility.EnvironmentHandler;
import se.inera.oppendata.mirth.ntjp.utility.MirthEnvironment;
import se.inera.oppendata.mirth.rest.CustomResponse;
import se.inera.oppendata.mirth.rest.RestClient;
import se.inera.oppendata.mirth.rest.StatusCodes;
import se.inera.oppendata.mirth.ssl.SSLContextCreator;
import se.inera.oppendata.mirth.ntjp.utility.JsonUtility;

import javax.net.ssl.SSLContext;
import java.util.HashMap;

/**
 * Created by davsan on 2017-11-28.
 */
public class OldAndNewPlatformTest {

    private static MirthEnvironment environment = MirthEnvironment.qa;


    public static void comparePerformingOrganizationsValues(){
       String attributeToLookFor = "performingOrganizationId";
       RestClient restClient = createRestClient();
       CustomResponse respOld = restClient.get(OldPlatformEnvironmentHandler.getPerformingOrganizationsUrl());
       Assert.assertEquals(respOld.status, StatusCodes.OK);
       HashMap<String, String> params = new HashMap();
       EnvironmentHandler environmentHandler = new EnvironmentHandler(environment);
       params.put("authorization", environmentHandler.getApiKey());
       params.put("fields", attributeToLookFor);
       CustomResponse respNew = restClient.get(environmentHandler.getPerformingOrganizationsUrl(), null, params);
       Assert.assertEquals(respNew.status, StatusCodes.OK);
       JSONArray oldPosts = getPosts(respOld);
       JSONArray newPosts = getPosts(respNew);
       JSONArray missingFromNew = getMissingValues(oldPosts, newPosts, attributeToLookFor);
       JSONArray missingFromOld = getMissingValues(newPosts, oldPosts, attributeToLookFor);
       printMissingValues(missingFromNew, missingFromOld);
    }


    public static void compareAggregatedQualityReports(){
        String attributeToLookFor = "aggregatedQualityReportId";
        RestClient restClient = createRestClient();
        CustomResponse respOld = restClient.get(OldPlatformEnvironmentHandler.getAggregatedQualityReportsUrl());
        Assert.assertEquals(respOld.status, StatusCodes.OK);
        HashMap<String, String> params = new HashMap();
        EnvironmentHandler environmentHandler = new EnvironmentHandler(environment);
        params.put("authorization", environmentHandler.getApiKey());
        params.put("fields", attributeToLookFor);
        CustomResponse respNew = restClient.get(environmentHandler.getAggregatedQualityReportsUrl(), null, params);
        Assert.assertEquals(respNew.status, StatusCodes.OK);
        JSONArray oldPosts = getPosts(respOld);
        JSONArray newPosts = getPosts(respNew);
        JSONArray missingFromNew = getMissingValues(oldPosts, newPosts, attributeToLookFor);
        JSONArray missingFromOld = getMissingValues(newPosts, oldPosts, attributeToLookFor);
        printMissingValues(missingFromNew, missingFromOld);
    }

    private static RestClient createRestClient(){
        SSLContext sslContext = SSLContextCreator.createSSLContext(true);
        RestClient restClient = new RestClient(sslContext);
        return restClient;
    }

    private static JSONArray getPosts(CustomResponse response){
        JSONArray posts = JsonUtility.parseToArray(response.body);
        return posts;
    }

    private static JSONArray getMissingValues(JSONArray arrayWithValues, JSONArray arrayToSearch, String attributeToSearchFor){
        JSONArray missingValues = new JSONArray();
        for(int i = 0; i < arrayWithValues.size(); i++){
            JSONObject entry1 = (JSONObject)arrayWithValues.get(i);
            Assert.assertNotNull(entry1.get(attributeToSearchFor));
            Object value1 = entry1.get(attributeToSearchFor);
            boolean valueFound = false;

            for(int j = 0; j < arrayToSearch.size(); j++){
                JSONObject entry2 = (JSONObject)arrayToSearch.get(j);
                Assert.assertNotNull(entry2.get(attributeToSearchFor));
                Object value2 = entry2.get(attributeToSearchFor);
                if(value1.equals(value2)){
                    valueFound = true;
                    break;
                }
            }
            if(!valueFound){
                missingValues.add(value1.toString());
            }
        }
        return missingValues;
    }

    private static void printMissingValues(JSONArray missingFromNew, JSONArray missingFromOld){
        System.out.println("---------------MISSING FROM NEW-----------------");
        System.out.println(missingFromNew.toJSONString());
        System.out.println("---------------MISSING FROM OLD-----------------");
        System.out.println(missingFromOld.toJSONString());
    }
}
