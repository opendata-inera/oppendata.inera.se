package se.inera.oppendata.mirth.ntjp.utility;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by davsan on 2017-10-25.
 */
public class FileHandler {
    private static final String JSON_FILES_BASE_FOLDER = "environment/";
    private static final String REAL_MEASUREMENT_FILENAME = "measurement_real.json";
    private static final String FAKE_MEASUREMENT_FILENAME = "measurement_fake.json";
    private static final String REAL_AGGREGATED_QUALITY_REPORT_FILENAME = "aggregatedQualityReport_real.json";
    private static final String FAKE_AGGREGATED_QUALITY_REPORT_FILENAME = "aggregatedQualityReport_fake.json";
    private static final String REAL_PERFORMING_ORGANIZATION_FILENAME = "performingOrganization_real.json";
    private static final String FAKE_PERFORMING_ORGANIZATION_FILENAME = "performingOrganization_fake.json";
    private static final String REAL_AGGREGATED_QUALITY_REPORTS_2018_FILENAME = "aggregatedQualityReports_real_2018.json";

    private EnvironmentHandler environmentHandler;

    public FileHandler(EnvironmentHandler environmentHandler){
        this.environmentHandler = environmentHandler;
    }

    public String getFakeMeasurement(){
        return readFileAsString(JSON_FILES_BASE_FOLDER+environmentHandler.getEnvironmentAsString(), FAKE_MEASUREMENT_FILENAME);
    }

    public String getRealMeasurement(){
        return readFileAsString(JSON_FILES_BASE_FOLDER+environmentHandler.getEnvironmentAsString(), REAL_MEASUREMENT_FILENAME);
    }

    public String getFakeAggregatedQualityReport(){
        return readFileAsString(JSON_FILES_BASE_FOLDER+environmentHandler.getEnvironmentAsString(), FAKE_AGGREGATED_QUALITY_REPORT_FILENAME);
    }

    public String getRealAggregatedQualityReport(){
        return readFileAsString(JSON_FILES_BASE_FOLDER+environmentHandler.getEnvironmentAsString(), REAL_AGGREGATED_QUALITY_REPORT_FILENAME);
    }

    public String getFakePerformingOrganization(){
        return readFileAsString(JSON_FILES_BASE_FOLDER+environmentHandler.getEnvironmentAsString(), FAKE_PERFORMING_ORGANIZATION_FILENAME);
    }

    public String getRealPerformingOrganization(){
        return readFileAsString(JSON_FILES_BASE_FOLDER+environmentHandler.getEnvironmentAsString(), REAL_PERFORMING_ORGANIZATION_FILENAME);
    }

    public String getRealAggregatedQualityReports2018(){
        return readFileAsString(JSON_FILES_BASE_FOLDER+environmentHandler.getEnvironmentAsString(), REAL_AGGREGATED_QUALITY_REPORTS_2018_FILENAME);
    }


    public static String getFilePath(String foldername, String filename){
        String filePath = FileHandler.class.getClassLoader().getResource(foldername+"/"+filename).getPath();
        if(filePath.startsWith("/")){
            filePath = filePath.substring(1);
        }
        return filePath;
    }



    public static String readFileAsString(String foldername,String filename){
        String filePath = getFilePath(foldername,filename);
        try {
            byte[] jsonBytes = readBytesFromFile(filePath);
            return new String(jsonBytes);
        }catch(IOException e){
            return e.getMessage();
        }
    }

    public static byte[] readBytesFromFile(String filepath) throws IOException {
        Path path = Paths.get(filepath);
        byte[] fileContent = Files.readAllBytes(path);
        return fileContent;
    }

    public static void saveToFile(String content, String filePath){
        try {
            byte[] contentAsBytes = content.getBytes("UTF-8");
            FileOutputStream fos = new FileOutputStream(filePath);
            fos.write(contentAsBytes);
            fos.close();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
