package se.inera.oppendata.mirth.ntjp.utility;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by davsan on 2018-01-29.
 */
public class EnvironmentHandler {

    private static final String MIRTH_PRODUCTION_URL = "https://integration.oppendata.inera.se/api/";
    private static final String MIRTH_TEST_URL = "https://test.integration.oppendata.inera.se/api/";
    private static final String MIRTH_QA_URL = "https://qa.integration.oppendata.inera.se/api/";
    private static final String API_KEY = "INSERT_API_KEY";
    private static final String MEASUREMENTS_ENDPOINT = "qualityreporting/v1/measurements";
    private static final String AGGREGATED_QUALITY_REPORTS_ENDPOINT = "qualityreporting/v1/aggregatedQualityReports";
    private static final String PERFORMING_ORGANIZATIONS_ENDPOINT = "qualityreporting/v1/performingOrganizations";
    private static final String UPDATE_ENDPOINT = "misc/v1/update/";
    private static final String CONTENT_TYPE = "application/json;charset=utf-8";


    private MirthEnvironment environment;

    public EnvironmentHandler(MirthEnvironment environment){
        this.environment = environment;
    }

    public String getEnvironmentAsString(){
        return environment.toString();
    }

    public String getApiKey(){
        return API_KEY;
    }

    public String getMeasurementsUpdateUrl(){
        return getBaseUrl() + UPDATE_ENDPOINT + MEASUREMENTS_ENDPOINT.replaceAll("/", "-");
    }

    public String getAggregatedQualityReportsUpdateUrl(){
        return getBaseUrl() + UPDATE_ENDPOINT + AGGREGATED_QUALITY_REPORTS_ENDPOINT.replaceAll("/", "-");
    }

    public String getPerformingOrganizationsUpdateUrl(){
        return getBaseUrl() + UPDATE_ENDPOINT + PERFORMING_ORGANIZATIONS_ENDPOINT.replaceAll("/", "-");
    }

    public String getMirthMeasurementsUrl(){
        return getBaseUrl()+ MEASUREMENTS_ENDPOINT;
    }

    public String getAggregatedQualityReportsUrl(){
        return getBaseUrl()+ AGGREGATED_QUALITY_REPORTS_ENDPOINT;
    }

    public String getPerformingOrganizationsUrl(){
        return getBaseUrl()+ PERFORMING_ORGANIZATIONS_ENDPOINT;
    }

    private String getBaseUrl(){
        if(environment == MirthEnvironment.test){
            return MIRTH_TEST_URL;
        }else if(environment == MirthEnvironment.qa){
            return MIRTH_QA_URL;
        }else if(environment == MirthEnvironment.prod){
            return MIRTH_PRODUCTION_URL;
        }else{
            return "";
        }
    }

    public Map getUpdateHeaders(){
        Map<String, String> headers = new HashMap<>();
        headers.put("Authorization", API_KEY);
        headers.put("Content-Type", CONTENT_TYPE);
        return headers;
    }
}
