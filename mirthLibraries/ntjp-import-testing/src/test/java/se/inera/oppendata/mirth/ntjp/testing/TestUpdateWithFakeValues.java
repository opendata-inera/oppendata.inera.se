package se.inera.oppendata.mirth.ntjp.testing;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import se.inera.oppendata.mirth.ntjp.utility.EnvironmentHandler;
import se.inera.oppendata.mirth.ntjp.utility.FileHandler;
import se.inera.oppendata.mirth.ntjp.utility.JsonUtility;
import se.inera.oppendata.mirth.ntjp.utility.MirthEnvironment;
import se.inera.oppendata.mirth.rest.CustomResponse;
import se.inera.oppendata.mirth.rest.RestClient;
import se.inera.oppendata.mirth.rest.StatusCodes;
import se.inera.oppendata.mirth.ssl.SSLContextCreator;

import javax.net.ssl.SSLContext;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by davsan on 2018-01-29.
 */
public class TestUpdateWithFakeValues {

    private static MirthEnvironment environment = MirthEnvironment.test;

    @Test
    public static void shouldUpdateMeasurementsWithFakeValue(){
        RestClient restClient = CreateRestClient();
        EnvironmentHandler environmentHandler = new EnvironmentHandler(environment);
        String url = environmentHandler.getMeasurementsUpdateUrl();
        FileHandler fileHandler = new FileHandler(environmentHandler);
        String measurement = "["+fileHandler.getFakeMeasurement()+"]";
        CustomResponse response = restClient.post(url, measurement, environmentHandler.getUpdateHeaders(), null);
        Assert.assertEquals(response.status, StatusCodes.OK);
    }

    @Test
    public static void shouldUpdateAggregatedQualityReportWithFakeValue(){
        RestClient restClient = CreateRestClient();
        EnvironmentHandler environmentHandler = new EnvironmentHandler(environment);
        String url = environmentHandler.getAggregatedQualityReportsUpdateUrl();
        FileHandler fileHandler = new FileHandler(environmentHandler);
        String report = "["+fileHandler.getFakeAggregatedQualityReport()+"]";
        CustomResponse response = restClient.post(url, report, environmentHandler.getUpdateHeaders(), null);
        Assert.assertEquals(response.status, StatusCodes.OK);
    }

    @Test
    public static void shouldUpdatePerformingOrganizationsWithFakeValue(){
        RestClient restClient = CreateRestClient();
        EnvironmentHandler environmentHandler = new EnvironmentHandler(environment);
        String url = environmentHandler.getPerformingOrganizationsUpdateUrl();
        FileHandler fileHandler = new FileHandler(environmentHandler);
        String performingOrganization = "["+fileHandler.getFakePerformingOrganization()+"]";
        CustomResponse response = restClient.post(url, performingOrganization, environmentHandler.getUpdateHeaders(), null);
        Assert.assertEquals(response.status, StatusCodes.OK);
    }


    public static void shouldUpdateAggregatedQualityReportsForYear2018WithFakeMeasurementChecksums(){
        RestClient restClient = CreateRestClient();
        EnvironmentHandler environmentHandler = new EnvironmentHandler(environment);
        String url = environmentHandler.getAggregatedQualityReportsUpdateUrl();
        FileHandler fileHandler = new FileHandler(environmentHandler);
        JSONArray reports = JsonUtility.parseToArray(fileHandler.getRealAggregatedQualityReports2018());
        for(int i = 0; i < reports.size(); i++){
            JSONObject report = (JSONObject) reports.get(i);
            report.put("measurementChecksum", "fakevalue");
        }
        String reportsJsonString = reports.toJSONString();

        CustomResponse response = restClient.post(url, reportsJsonString, environmentHandler.getUpdateHeaders(), null);
        Assert.assertEquals(response.status, StatusCodes.OK);
    }

    private static RestClient CreateRestClient(){
        SSLContext sslContext = SSLContextCreator.createSSLContext(true);
        RestClient restClient = new RestClient(sslContext);
        return restClient;
    }

}
