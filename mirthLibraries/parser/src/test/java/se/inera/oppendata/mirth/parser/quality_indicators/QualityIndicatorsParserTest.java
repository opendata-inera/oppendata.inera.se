package se.inera.oppendata.mirth.parser.quality_indicators;

import org.json.simple.JSONObject;
import org.testng.annotations.Test;
import se.inera.oppendata.mirth.parser.utility.FileHandler;
import se.inera.oppendata.mirth.parser.utility.JsonUtility;
import se.inera.oppendata.mirth.soap.quality_indicators.*;

/**
 * Created by davsan on 2017-10-18.
 */
public class QualityIndicatorsParserTest {
    private static final String JSON_FILES_FOLDER = "jsonAssertionFiles/quality_indicators";
    private static final String DEFAULT_REASON_CODE_SYSTEM = "2.16.840.1.114222.4.11.875";
    private static final String DEFAULT_PERFORMING_ORGANIZATION_TYPE_SYSTEM = "5447717431505822d0f4f4e8";

    @Test
    public static void shouldCreateMeasurementJsonObjectWithTopLevelAttributesSpecifiedAsNull(){
        MeasurementJsonObject jsonObject = createMeasurementJsonObjectWithTopLevelAttributesSpecifiedAsNull();
        String expectedJson = FileHandler.readFileAsString(JSON_FILES_FOLDER, "singleMeasurementWithTopLevelAttributesSpecifiedAsNull.json");
        String json = jsonObject.toJSONString();
        JsonUtility.assertEquals(expectedJson, json);
    }

    @Test
    public static void shouldCreateMeasurementJsonObjectWithSecondLevelAttributesSpecifiedAsNull(){
        MeasurementJsonObject jsonObject = createMeasurementJsonObjectWithSecondLevelAttributesSpecifiedAsNull();
        String expectedJson = FileHandler.readFileAsString(JSON_FILES_FOLDER, "singleMeasurementWithSecondLevelAttributesSpecifiedAsNull.json");
        String json = jsonObject.toJSONString();
        JsonUtility.assertEquals(expectedJson, json);
    }

    @Test
    public static void shouldCreateMeasurementJsonObjectWithPerformingOrgIdAndPerformingOrgHsaId(){
        MeasurementJsonObject jsonObject = createMeasurementJsonObject(PerformingOrganizationIdType.HSA);
        String expectedJson = FileHandler.readFileAsString(JSON_FILES_FOLDER, "singleMeasurementWithPerformingOrgIdAndPerformingOrgHsaId.json");
        String json = jsonObject.toJSONString();
        JsonUtility.assertEquals(expectedJson, json);
    }

    @Test
    public static void shouldCreateMeasurementJsonObjectWithPerformingOrgIdButWithNullPerformingOrgHsaId(){
        MeasurementJsonObject jsonObject = createMeasurementJsonObject(PerformingOrganizationIdType.ORGANIZATION);
        String expectedJson = FileHandler.readFileAsString(JSON_FILES_FOLDER, "singleMeasurementWithPerformingOrgIdButWithNullPerformingOrgHsaId.json");
        String json = jsonObject.toJSONString();
        JsonUtility.assertEquals(expectedJson, json);
    }

    @Test
    public static void shouldCreateAggregatedQualityReportWithoutSynchronizationTime(){
        AggregatedQualityReportJsonObject jsonObject = createAggregatedQualityReportJsonObjectWithoutSynchronizationTime();
        String expectedJson = FileHandler.readFileAsString(JSON_FILES_FOLDER, "singleAggregatedQualityReportWithoutSynchronizationTime.json");
        String json = jsonObject.toJSONString();
        JsonUtility.assertEquals(expectedJson, json);
    }

    @Test
    public static void shouldCreateAggregatedQualityReportWithTopLevelAttributesSpecifiedAsNull(){
        AggregatedQualityReportJsonObject jsonObject = createAggregatedQualityReportJsonObjectWithTopLevelAttributesSpecifiedAsNull();
        String expectedJson = FileHandler.readFileAsString(JSON_FILES_FOLDER, "singleAggregatedQualityReportWithTopLevelAttributesSpecifiedAsNull.json");
        String json = jsonObject.toJSONString();
        JsonUtility.assertEquals(expectedJson, json);
    }

    @Test
    public static void shouldCreateAggregatedQualityReportWithSynchronizationTime(){
        AggregatedQualityReportJsonObject jsonObject = createAggregatedQualityReportJsonObjectWithSynchronizationTime();
        String expectedJson = FileHandler.readFileAsString(JSON_FILES_FOLDER, "singleAggregatedQualityReportWithoutSynchronizationTime.json");
        JSONObject expectedJsonObject = JsonUtility.parse(expectedJson);
        expectedJsonObject.put(AggregatedQualityReportJsonAttributes.SYNCHRONIZATION_TIME, jsonObject.getSynchronizationTime());
        String json = jsonObject.toJSONString();
        expectedJson = expectedJsonObject.toJSONString();
        JsonUtility.assertEquals(expectedJson, json);
    }

    @Test
    public static void shouldCreatePerformingOrganizationWithTopLevelAttributesSpecifiedAsNull(){
        PerformingOrganizationType performingOrganizationType = TestDataUtility.createPerformingOrganizationTypeWithTopLevelAttributesSpecifiedAsNull();
        PerformingOrganizationJsonObject jsonObject = new PerformingOrganizationJsonObject(performingOrganizationType);
        String expectedJson = FileHandler.readFileAsString(JSON_FILES_FOLDER, "performingOrganizationWithTopLevelAttributesSpecifiedAsNull.json");
        String jsonString = jsonObject.toJSONString();
        JsonUtility.assertEquals(expectedJson, jsonString);
    }

    @Test
    public static void shouldCreatePerformingOrganizationWithHsaIdAndWithNullAsOrganizationPartOf(){
        PerformingOrganizationType performingOrganizationType = TestDataUtility.createPerformingOrganizationTypeWithoutAsOrganizationPartOf(PerformingOrganizationIdType.HSA);
        PerformingOrganizationJsonObject.setPerformingOrganizationTypeSystem(DEFAULT_PERFORMING_ORGANIZATION_TYPE_SYSTEM);
        PerformingOrganizationJsonObject jsonObject = new PerformingOrganizationJsonObject(performingOrganizationType);
        String expectedJson = FileHandler.readFileAsString(JSON_FILES_FOLDER, "performingOrganizationWithHsaIdAndWithNullAsOrganizationPartOf.json");
        String jsonString = jsonObject.toJSONString();
        JsonUtility.assertEquals(expectedJson, jsonString);
    }

    @Test
    public static void shouldCreatePerformingOrganizationWithOrgIdAndWithNullAsOrganizationPartOf(){
        PerformingOrganizationType performingOrganizationType = TestDataUtility.createPerformingOrganizationTypeWithoutAsOrganizationPartOf(PerformingOrganizationIdType.ORGANIZATION);
        PerformingOrganizationJsonObject.setPerformingOrganizationTypeSystem(DEFAULT_PERFORMING_ORGANIZATION_TYPE_SYSTEM);
        PerformingOrganizationJsonObject jsonObject = new PerformingOrganizationJsonObject(performingOrganizationType);
        String expectedJson = FileHandler.readFileAsString(JSON_FILES_FOLDER, "performingOrganizationWithOrganizationIdAndWithNullAsOrganizationPartOf.json");
        String jsonString = jsonObject.toJSONString();
        JsonUtility.assertEquals(expectedJson, jsonString);
    }

    @Test
    public static void shouldCreatePerformingOrganizationWithHsaIdAndTwoAsOrganizationOfChildren(){
        PerformingOrganizationType performingOrganizationType = TestDataUtility.createPerformingOrganizationTypeWithTwoAsOrganizationPartOf(PerformingOrganizationIdType.HSA);
        PerformingOrganizationJsonObject.setPerformingOrganizationTypeSystem(DEFAULT_PERFORMING_ORGANIZATION_TYPE_SYSTEM);
        PerformingOrganizationJsonObject.setIncludePerformingOrganizationIdInChildren(true);
        PerformingOrganizationJsonObject jsonObject = new PerformingOrganizationJsonObject(performingOrganizationType);
        String expectedJson = FileHandler.readFileAsString(JSON_FILES_FOLDER, "performingOrganizationWithHsaIdAndTwoAsOrganizationPartOfChildren.json");
        String jsonString = jsonObject.toJSONString();
        JsonUtility.assertEquals(expectedJson, jsonString);
    }

    @Test
    public static void shouldCreatePerformingOrganizationWithOrgIdAndTwoAsOrganizationOfChildren(){
        PerformingOrganizationType performingOrganizationType = TestDataUtility.createPerformingOrganizationTypeWithTwoAsOrganizationPartOf(PerformingOrganizationIdType.ORGANIZATION);
        PerformingOrganizationJsonObject.setPerformingOrganizationTypeSystem(DEFAULT_PERFORMING_ORGANIZATION_TYPE_SYSTEM);
        PerformingOrganizationJsonObject.setIncludePerformingOrganizationIdInChildren(true);
        PerformingOrganizationJsonObject jsonObject = new PerformingOrganizationJsonObject(performingOrganizationType);
        String expectedJson = FileHandler.readFileAsString(JSON_FILES_FOLDER, "performingOrganizationWithOrganizationIdAndTwoAsOrganizationPartOfChildren.json");
        String jsonString = jsonObject.toJSONString();
        JsonUtility.assertEquals(expectedJson, jsonString);
    }

    @Test
    public static void shouldCreatePerformingOrganizationWithHsaIdAndTwoAsOrganizationOfChildrenFromMeasurementType(){
        MeasurementType measurementType = TestDataUtility.createMeasurementType(PerformingOrganizationIdType.HSA);
        PerformingOrganizationJsonObject.setPerformingOrganizationTypeSystem(DEFAULT_PERFORMING_ORGANIZATION_TYPE_SYSTEM);
        PerformingOrganizationJsonObject.setIncludePerformingOrganizationIdInChildren(true);
        PerformingOrganizationJsonObject jsonObject = new PerformingOrganizationJsonObject(measurementType);
        String expectedJson = FileHandler.readFileAsString(JSON_FILES_FOLDER, "performingOrganizationWithHsaIdAndTwoAsOrganizationPartOfChildren.json");
        String jsonString = jsonObject.toJSONString();
        JsonUtility.assertEquals(expectedJson, jsonString);
    }

    @Test
    public static void shouldCreatePerformingOrganizationWithOrgIdAndTwoAsOrganizationOfChildrenFromMeasurementType(){
        MeasurementType measurementType = TestDataUtility.createMeasurementType(PerformingOrganizationIdType.ORGANIZATION);
        PerformingOrganizationJsonObject.setPerformingOrganizationTypeSystem(DEFAULT_PERFORMING_ORGANIZATION_TYPE_SYSTEM);
        PerformingOrganizationJsonObject.setIncludePerformingOrganizationIdInChildren(true);
        PerformingOrganizationJsonObject jsonObject = new PerformingOrganizationJsonObject(measurementType);
        String expectedJson = FileHandler.readFileAsString(JSON_FILES_FOLDER, "performingOrganizationWithOrganizationIdAndTwoAsOrganizationPartOfChildren.json");
        String jsonString = jsonObject.toJSONString();
        JsonUtility.assertEquals(expectedJson, jsonString);
    }

    @Test
    public static void shouldNotIncludePerformingOrganizationIdInAsOrganizationOfChildren(){
        MeasurementType measurementType = TestDataUtility.createMeasurementType(PerformingOrganizationIdType.HSA);
        PerformingOrganizationJsonObject.setPerformingOrganizationTypeSystem(DEFAULT_PERFORMING_ORGANIZATION_TYPE_SYSTEM);
        PerformingOrganizationJsonObject.setIncludePerformingOrganizationIdInChildren(false);
        PerformingOrganizationJsonObject jsonObject = new PerformingOrganizationJsonObject(measurementType);
        String expectedJson = FileHandler.readFileAsString(JSON_FILES_FOLDER, "performingOrganizationWithTwoAsOrganizationPartOfChildrenWithoutIds.json");
        String jsonString = jsonObject.toJSONString();
        JsonUtility.assertEquals(expectedJson, jsonString);
    }

    private static AggregatedQualityReportJsonObject createAggregatedQualityReportJsonObjectWithoutSynchronizationTime(){
        CustomAggregatedQualityReportDetails reportingDetails = TestDataUtility.createReportingDetails();
        AggregatedQualityReportJsonObject jsonObject = new AggregatedQualityReportJsonObject();
        jsonObject.addAggregatedQualityReportId(TestDataUtility.AGGREGATED_QUALITY_REPORT_ID);
        jsonObject.addMeasureId(TestDataUtility.MEASURE_ID);
        jsonObject.addReportDetails(reportingDetails);
        return jsonObject;
    }

    private static AggregatedQualityReportJsonObject createAggregatedQualityReportJsonObjectWithTopLevelAttributesSpecifiedAsNull(){
        CustomAggregatedQualityReportDetails reportingDetails = TestDataUtility.createReportingDetailsWithNullValues();
        AggregatedQualityReportJsonObject jsonObject = new AggregatedQualityReportJsonObject(null, reportingDetails,null);
        return jsonObject;
    }

    private static AggregatedQualityReportJsonObject createAggregatedQualityReportJsonObjectWithSynchronizationTime(){
        CustomAggregatedQualityReportDetails reportingDetails = TestDataUtility.createReportingDetails();
        String measureId = TestDataUtility.MEASURE_ID;
        String aggregatedQualityReportId = TestDataUtility.AGGREGATED_QUALITY_REPORT_ID;
        AggregatedQualityReportJsonObject jsonObject = new AggregatedQualityReportJsonObject(aggregatedQualityReportId, reportingDetails, measureId);
        jsonObject.addSynchronizationTimeFromCurrentLocalTime();
        return jsonObject;
    }

    private static MeasurementJsonObject createMeasurementJsonObject(PerformingOrganizationIdType performingOrganizationIdType){
        MeasurementType measurementType = TestDataUtility.createMeasurementType(performingOrganizationIdType);
        MeasurementJsonObject.setReasonCodeSystem(DEFAULT_REASON_CODE_SYSTEM);
        MeasurementJsonObject measurementJsonObject = new MeasurementJsonObject();
        measurementJsonObject.addMeasurementId(TestDataUtility.MEASUREMENT_ID);
        measurementJsonObject.addMeasurementType(measurementType);
        measurementJsonObject.addParentMeasureId(TestDataUtility.PARENT_MEASURE_ID);
        measurementJsonObject.addAggregatedQualityReportId(TestDataUtility.AGGREGATED_QUALITY_REPORT_ID);
        return measurementJsonObject;
    }

    private static MeasurementJsonObject createMeasurementJsonObjectWithTopLevelAttributesSpecifiedAsNull(){
        MeasurementType measurementType = TestDataUtility.createMeasurementTypeWithTopLevelAttributesSpecifiedAsNull();
        MeasurementJsonObject measurementJsonObject = new MeasurementJsonObject();
        measurementJsonObject.addMeasurementId(null);
        measurementJsonObject.addMeasurementType(measurementType);
        measurementJsonObject.addParentMeasureId(null);
        measurementJsonObject.addAggregatedQualityReportId(null);
        return measurementJsonObject;
    }

    private static MeasurementJsonObject createMeasurementJsonObjectWithSecondLevelAttributesSpecifiedAsNull(){
        MeasurementType measurementType = TestDataUtility.createMeasurementTypeWithSecondLevelAttributesSpecifiedAsNull();
        MeasurementJsonObject.setReasonCodeSystem(null);
        MeasurementJsonObject measurementJsonObject = new MeasurementJsonObject();
        measurementJsonObject.addMeasurementId(TestDataUtility.MEASUREMENT_ID);
        measurementJsonObject.addParentMeasureId(TestDataUtility.PARENT_MEASURE_ID);
        measurementJsonObject.addAggregatedQualityReportId(TestDataUtility.AGGREGATED_QUALITY_REPORT_ID);
        measurementJsonObject.addMeasurementType(measurementType);
        return measurementJsonObject;
    }
}
