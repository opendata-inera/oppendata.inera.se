package se.inera.oppendata.mirth.parser.quality_indicators;

import se.inera.oppendata.mirth.soap.quality_indicators.*;

import java.math.BigInteger;

/**
 * Created by davsan on 2017-10-25.
 */
public class TestDataUtility {
    public static final String AGGREGATED_QUALITY_REPORT_ID = "1234-5678-91011";
    public static final String PARENT_MEASURE_ID = "parent-measure-12345";
    public static final String MEASURE_ID = "measure-id-12345";
    public static final String MEASUREMENT_ID = "measurement-id-12345";
    public static final String SYSTEM_ID = "system-id-12345";

    public static MeasurementType createMeasurementType(PerformingOrganizationIdType performingOrganizationIdType){
        MeasurementType measurementType = new MeasurementType();
        measurementType.setMeasureId(new IIType("dummy", MEASURE_ID));
        measurementType.setMeasureIdVersionNumber(BigInteger.valueOf(1));
        measurementType.setMeasurePeriod(new DatePeriodType("20170101", "20171231"));
        measurementType.setFirstServiceEncounter("20171018");
        measurementType.setLastServiceEncounter("20171118");
        PerformingOrganizationType pot = createPerformingOrganizationTypeWithTwoAsOrganizationPartOf(performingOrganizationIdType);
        measurementType.setPerformingOrganization(pot);
        MissingMeasureType mmt = new MissingMeasureType();
        mmt.setReasonCode(MissingMeasureReasonCodeEnum.MSK);
        measurementType.setMissingMeasure(mmt);

        ContinuousVariableMeasureType cvmt = new ContinuousVariableMeasureType();
        cvmt.setMeasurePopulation(1.234);
        cvmt.setValue(1234567.89);
        ConfidenceInterval95PercentType cipt = new ConfidenceInterval95PercentType();
        cipt.setLow(12.34);
        cipt.setHigh(56.78);
        cvmt.setConfidenceInterval95Percent(cipt);
        cvmt.setStandardDeviation(1.234);
        cvmt.setCoverage(100.234);
        cvmt.setExclusions(200.234);
        cvmt.setReferenceIntervalValue(300.234);

        ProportionMeasureType pmt = new ProportionMeasureType();
        pmt.setConfidenceInterval95Percent(cipt);
        pmt.setRate(12.3456);
        pmt.setDenominator(123.456);
        pmt.setNumerator(2000.0);
        pmt.setStandardDeviation(12.34);
        pmt.setCoverage(987.654);
        pmt.setExclusions(999.00);
        pmt.setReferenceIntervalRate(12345.6);

        CohortMeasureType cmt = new CohortMeasureType();
        cmt.setConfidenceInterval95Percent(cipt);
        cmt.setCohort(12.3456);
        cmt.setStandardDeviation(12.34);
        cmt.setCoverage(987.654);
        cmt.setExclusions(999.00);
        cmt.setReferenceIntervalValue(12345.6);

        SourceSystemType sst = new SourceSystemType();
        sst.setSystemId(new IIType("system-root", SYSTEM_ID));
        sst.setName("system-name-12345");

        measurementType.setContinuousVariableMeasure(cvmt);
        measurementType.setProportionMeasure(pmt);
        measurementType.setCohortMeasure(cmt);
        measurementType.setSourceSystem(sst);
        return measurementType;
    }

    public static MeasurementType createMeasurementTypeWithTopLevelAttributesSpecifiedAsNull(){
        MeasurementType measurementType = new MeasurementType();
        measurementType.setMeasureId(null);
        measurementType.setMeasureIdVersionNumber(null);
        measurementType.setMeasurePeriod(null);
        measurementType.setFirstServiceEncounter(null);
        measurementType.setLastServiceEncounter(null);
        measurementType.setPerformingOrganization(null);
        measurementType.setMissingMeasure(null);
        measurementType.setContinuousVariableMeasure(null);
        measurementType.setProportionMeasure(null);
        measurementType.setCohortMeasure(null);
        measurementType.setSourceSystem(null);
        return measurementType;
    }

    public static MeasurementType createMeasurementTypeWithSecondLevelAttributesSpecifiedAsNull(){
        MeasurementType measurementType = new MeasurementType();
        measurementType.setMeasureId(new IIType(null, null));
        measurementType.setMeasureIdVersionNumber(BigInteger.valueOf(1));
        measurementType.setMeasurePeriod(new DatePeriodType(null, null));
        measurementType.setFirstServiceEncounter("19000101");
        measurementType.setLastServiceEncounter("19000101");
        measurementType.setPerformingOrganization(null);
        MissingMeasureType mmt = new MissingMeasureType();
        mmt.setReasonCode(null);
        measurementType.setMissingMeasure(mmt);

        ContinuousVariableMeasureType cvmt = new ContinuousVariableMeasureType();
        ProportionMeasureType pmt = new ProportionMeasureType();
        CohortMeasureType cmt = new CohortMeasureType();
        SourceSystemType sst = new SourceSystemType();
        measurementType.setContinuousVariableMeasure(cvmt);
        measurementType.setProportionMeasure(pmt);
        measurementType.setCohortMeasure(cmt);
        measurementType.setSourceSystem(sst);

        return measurementType;
    }


    public static CustomAggregatedQualityReportDetails createReportingDetails(){
        ReportingSystemType reportingSystem = new ReportingSystemType();
        reportingSystem.setHsaId("SE-01234");
        reportingSystem.setName("My reporting system name");
        ReportingOrganizationType reportingOrganization = new ReportingOrganizationType();
        reportingOrganization.setHsaId("SE-56789");
        reportingOrganization.setName("My reporting organization name");
        DatePeriodType reportingPeriod = new DatePeriodType("20170101", "20171231");
        String measurementChecksum = "CHECKSUM-123456789";
        CustomAggregatedQualityReportDetails reportingDetails = new CustomAggregatedQualityReportDetails(reportingPeriod, reportingSystem, reportingOrganization, measurementChecksum);
        return reportingDetails;
    }

    public static CustomAggregatedQualityReportDetails createReportingDetailsWithNullValues(){
        ReportingSystemType reportingSystem = new ReportingSystemType();
        ReportingOrganizationType reportingOrganization = new ReportingOrganizationType();
        DatePeriodType reportingPeriod = new DatePeriodType(null, null);
        String measurementChecksum = null;
        CustomAggregatedQualityReportDetails reportingDetails = new CustomAggregatedQualityReportDetails(reportingPeriod, reportingSystem, reportingOrganization, measurementChecksum);
        return reportingDetails;
    }


    public static PerformingOrganizationType createPerformingOrganizationTypeWithoutAsOrganizationPartOf(PerformingOrganizationIdType idType){
        PerformingOrganizationType pot = new PerformingOrganizationType();
        if(idType == PerformingOrganizationIdType.HSA){
            pot.setHsaId("HSA-ID-123");
        }else{
            pot.setOrganizationId(new IIType("123-root", "id-123"));
        }
        pot.setOrganizationName("My org name 123");
        pot.setOrganizationType(OrganizationTypeCodeEnum.KOMMUN);
        return pot;
    }

    public static PerformingOrganizationType createPerformingOrganizationTypeWithTopLevelAttributesSpecifiedAsNull(){
        PerformingOrganizationType pot = new PerformingOrganizationType();
        return pot;
    }

    public static PerformingOrganizationType createPerformingOrganizationTypeWithTwoAsOrganizationPartOf(PerformingOrganizationIdType idTypeOfFirstEntry){
        PerformingOrganizationType pot1 = createPerformingOrganizationTypeWithoutAsOrganizationPartOf(idTypeOfFirstEntry);

        PerformingOrganizationType pot2 = new PerformingOrganizationType();
        pot2.setOrganizationId(new IIType("456-root", "id-456"));

        pot2.setOrganizationName("My org name 456");
        pot2.setOrganizationType(OrganizationTypeCodeEnum.LAND);

        PerformingOrganizationType pot3 = new PerformingOrganizationType();
        pot3.setOrganizationId(new IIType("789-root", "id-789"));
        pot3.setOrganizationName("My org name 789");
        pot3.setOrganizationType(OrganizationTypeCodeEnum.SJUKHUS);

        pot2.setAsOrganizationPartOf(pot3);
        pot1.setAsOrganizationPartOf(pot2);
        return pot1;
    }

}
