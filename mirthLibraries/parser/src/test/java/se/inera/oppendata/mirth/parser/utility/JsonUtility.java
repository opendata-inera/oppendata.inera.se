package se.inera.oppendata.mirth.parser.utility;

import org.json.JSONException;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;

/**
 * Created by davsan on 2017-10-25.
 */
public class JsonUtility {

    public static void assertEquals(String expectedJson, String json){
        try {
            JSONAssert.assertEquals(expectedJson, json, JSONCompareMode.NON_EXTENSIBLE);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static void assertNotEquals(String expectedJson, String json){
        try {
            JSONAssert.assertNotEquals(expectedJson, json, JSONCompareMode.NON_EXTENSIBLE);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static JSONObject parse(String jsonString){
        JSONParser parser = new JSONParser();
        try {
            return (JSONObject)parser.parse(jsonString);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
}
