package se.inera.oppendata.mirth.parser.xml;

import java.io.*;

import org.testng.Assert;
import org.testng.annotations.Test;
import se.inera.oppendata.mirth.parser.utility.FileHandler;
import se.inera.oppendata.mirth.parser.utility.JsonUtility;

/**
 * Created by davsan on 2017-07-18.
 */
public class XMLParserTest {

    private static final String XML_FILES_FOLDER = "xmlTestFiles";
    private static final String JSON_FILES_FOLDER = "jsonAssertionFiles";
    private static final String[] MEASURES_ARRAY_TAGS = {"regionalIndicator","subCategoryCode", "descriptionSource", "measureGroupCode", "denominatorInclusionValueSetId", "denominatorExclusionValueSetId", "numeratorInclusionValueSetId", "numeratorExclusionValueSetId", "measurePopulationInclusionValueSetId", "measurePopulationExclusionValueSetId","measuringFrequency", "organizationLevel","availableReportingSystemHsaId"};
    private static final String[] VALUE_SETS_ARRAY_TAGS = {"codeInValueSet"};
    private static final String[] PERFORMING_ORGANIZATIONS_ARRAY_TAGS = {"organizationalTarget"};

    @Test
    public static void shouldCreateSimpleJsonObject(){
        XMLParser parser = new XMLParser();
        String json = createJson(parser,"simpleXmlElement.xml");
        String expectedJson = readJsonFile("simpleJsonObject.json");
        JsonUtility.assertEquals(expectedJson, json);
    }

    @Test
    public static void testParentWithOneChildFormsArray(){
        XMLParser parser = new XMLParser();
        parser.addParentTagNameToInterpretAsArrayRoot("elements");
        String json = createJson(parser,"oneElement.xml");
        String expectedJson = readJsonFile("parentWithOneChildArray.json");
        JsonUtility.assertEquals(expectedJson, json);
    }

    @Test
    public static void testParentWithOneChildDoesNotFormArray(){
        String json = createJson("oneElement.xml");
        String expectedJson = readJsonFile("parentWithOneChild.json");
        String unexpectedJson = readJsonFile("parentWithOneChildArray.json");
        JsonUtility.assertEquals(expectedJson, json);
        JsonUtility.assertNotEquals(unexpectedJson, json);
    }

    @Test
    public static void testParentIsInvalidArrayRoot(){
        XMLParser parser = new XMLParser();
        parser.addParentTagNameToInterpretAsArrayRoot("elements");
        String json = createJson(parser,"twoElementsAndOneExtra.xml");
        Assert.assertTrue(json.contains("XML Parse Exception"));
    }

    @Test
    public static void testTwoChildrenInSuccessionWithSameTagFormArray(){
        String json = createJson("twoElements.xml");
        String expectedJson = readJsonFile("twoElementsArray.json");
        JsonUtility.assertEquals(expectedJson, json);
    }

    @Test
    public static void testTwoChildrenNotInSuccessionWithSameTagThrowsParsingError(){
        String json = createJson("twoElementsWithSameTagNotInDirectSuccession.xml");
        Assert.assertTrue(json.contains("XML Parse Exception"));
    }

    @Test
    public static void testSingleChildFormsArray(){
        XMLParser parser = new XMLParser();
        parser.addTagNameThatShouldAlwaysBePartOfArray("element");
        String json = createJson(parser, "oneElement.xml");
        String expectedJson = readJsonFile("childElementAsSingleArray.json");
        JsonUtility.assertEquals(expectedJson, json);
    }

    @Test
    public static void testSingleChildDoesNotFormArray(){
        String json = createJson("oneElement.xml");
        String expectedJson = readJsonFile("testNoOneElementArray.json");
        JsonUtility.assertEquals(expectedJson, json);
    }

    @Test
    public static void testEmbeddedArrays(){
        XMLParser parser = new XMLParser();
        parser.addParentTagNameToInterpretAsArrayRoot("elements");
        String json = createJson(parser,"embeddedArrays.xml");
        String expectedJson = readJsonFile("embeddedArrays.json");
        JsonUtility.assertEquals(expectedJson, json);
    }

    @Test
    public static void testEmptyElement(){
        String json = createJson("emptyElement.xml");
        String expectedJson = "{}";
        JsonUtility.assertEquals(expectedJson, json);
    }

    @Test
    public static void testEmptyArray(){
        String json = createJson("emptyArray.xml");
        String expectedJson = "{}";
        JsonUtility.assertEquals(expectedJson, json);
    }

    @Test
    public static void testEmptyElementsAreRemoved() {
        XMLParser parser = new XMLParser();
        parser.addParentTagNameToInterpretAsArrayRoot("elements");
        String json = createJson(parser, "multipleEmptyElements.xml");
        String expectedJson = readJsonFile("emptyElementsRemoved.json");
        JsonUtility.assertEquals(expectedJson, json);
    }

    @Test
    public static void shouldParseValuesAsStringsWhenTagsAreSpecified(){
        XMLParser parser = new XMLParser();
        parser.addParentTagNameToInterpretAsArrayRoot("values");
        setupValuesThatShouldBeInterpretedAsStrings(parser);
        String json = createJson(parser, "testCorrectTypeParsing.xml");
        String expectedJson = readJsonFile("allValuesAreStrings.json");
        JsonUtility.assertEquals(expectedJson, json);
    }

    @Test
    public static void shouldParseValuesAsStringsWhenInterpretAllValuesAsStringsIsTrue(){
        XMLParser parser = new XMLParser();
        parser.addParentTagNameToInterpretAsArrayRoot("values");
        parser.setInterpretAllValuesAsStrings(true);
        String json = createJson(parser, "testCorrectTypeParsing.xml");
        String expectedJson = readJsonFile("allValuesAreStrings.json");
        JsonUtility.assertEquals(expectedJson, json);
    }

    private static void setupValuesThatShouldBeInterpretedAsStrings(XMLParser parser){
        parser.addTagNameThatShouldAlwaysBeInterpretedAsString("someInt");
        parser.addTagNameThatShouldAlwaysBeInterpretedAsString("someFloat");
        parser.addTagNameThatShouldAlwaysBeInterpretedAsString("someFalseBoolean");
        parser.addTagNameThatShouldAlwaysBeInterpretedAsString("someTrueBoolean");
    }

    @Test
    public static void shouldParseValuesAsCorrectTypes(){
        XMLParser parser = new XMLParser();
        parser.addParentTagNameToInterpretAsArrayRoot("values");
        String json = createJson(parser, "testCorrectTypeParsing.xml");
        String expectedJson = readJsonFile("allValuesHaveTheCorrectTypes.json");
        JsonUtility.assertEquals(expectedJson, json);
    }

    @Test
    public static void testMeasures(){
        XMLParser parser = new XMLParser();
        parser.addParentTagNameToInterpretAsArrayRoot("measures");
        parser.addTagNameThatShouldAlwaysBeInterpretedAsString("measureId");
        for(int i = 0; i < MEASURES_ARRAY_TAGS.length; i++){
            parser.addTagNameThatShouldAlwaysBePartOfArray(MEASURES_ARRAY_TAGS[i]);
        }
        String json = createJson(parser, "measuresSnippet.xml");
        String expectedJson = readJsonFile("measuresSnippet.json");
        JsonUtility.assertEquals(expectedJson, json);
    }

    @Test
    public static void testMeasureFormerVersions(){
        XMLParser parser = new XMLParser();
        parser.addParentTagNameToInterpretAsArrayRoot("measureFormerVersions");
        parser.addTagNameThatShouldAlwaysBeInterpretedAsString("measureId");
        for(int i = 0; i < MEASURES_ARRAY_TAGS.length; i++){
            parser.addTagNameThatShouldAlwaysBePartOfArray(MEASURES_ARRAY_TAGS[i]);
        }
        String json = createJson(parser, "measureFormerVersionsSnippet.xml");
        String expectedJson = readJsonFile("measureFormerVersionsSnippet.json");
        JsonUtility.assertEquals(expectedJson, json);
    }

    @Test
    public static void testCodes(){
        XMLParser parser = new XMLParser();
        parser.addParentTagNameToInterpretAsArrayRoot("codes");
        parser.addTagNameThatShouldAlwaysBeInterpretedAsString("codeId");
        String json = createJson(parser, "codesFull.xml");
        String expectedJson = readJsonFile("codesFull.json");
        JsonUtility.assertEquals(expectedJson, json);
    }

    @Test
    public static void testCodeSystems(){
        XMLParser parser = new XMLParser();
        parser.addParentTagNameToInterpretAsArrayRoot("codeSystems");
        String json = createJson(parser, "codeSystemsFull.xml");
        String expectedJson = readJsonFile("codeSystemsFull.json");
        JsonUtility.assertEquals(expectedJson, json);
    }

    @Test
    public static void testTargetMeasurements(){
        XMLParser parser = new XMLParser();
        parser.addParentTagNameToInterpretAsArrayRoot("targetMeasurements");
        parser.addTagNameThatShouldAlwaysBeInterpretedAsString("targetMeasureId");
        String json = createJson(parser, "targetMeasurementsFull.xml");
        String expectedJson = readJsonFile("targetMeasurementsFull.json");
        JsonUtility.assertEquals(expectedJson, json);
    }

    @Test
    public static void testValueSets(){
        XMLParser parser = new XMLParser();
        parser.addParentTagNameToInterpretAsArrayRoot("valueSets");
        for(int i = 0; i < VALUE_SETS_ARRAY_TAGS.length; i++){
            parser.addTagNameThatShouldAlwaysBePartOfArray(VALUE_SETS_ARRAY_TAGS[i]);
        }
        String json = createJson(parser, "valueSetsFull.xml");
        String expectedJson = readJsonFile("valueSetsFull.json");
        JsonUtility.assertEquals(expectedJson, json);
    }

    @Test
    public static void testPerformingOrganizations(){
        XMLParser parser = new XMLParser();
        parser.addParentTagNameToInterpretAsArrayRoot("performingOrganizations");
        parser.addTagNameThatShouldAlwaysBeInterpretedAsString("performingOrganizationHsaId");
        for(int i = 0; i < PERFORMING_ORGANIZATIONS_ARRAY_TAGS.length; i++){
            parser.addTagNameThatShouldAlwaysBePartOfArray(PERFORMING_ORGANIZATIONS_ARRAY_TAGS[i]);
        }
        String json = createJson(parser, "performingOrganizationsFull.xml");
        String expectedJson = readJsonFile("performingOrganizationsFull.json");
        JsonUtility.assertEquals(expectedJson, json);
    }



    private static String createJson(String filename){
        return createJson(new XMLParser(), filename);
    }

    private static String createJson(XMLParser parser, String filename){
        try{
            String json = tryParseXmlToJson(parser, filename);
            return json;
        }catch (IOException e){
            return e.getMessage();
        }
    }

    private static String tryParseXmlToJson(XMLParser xmlParser, String filename) throws IOException{
        String filePath = getXmlFilePath(filename);
        byte[] xml = FileHandler.readBytesFromFile(filePath);
        String json = xmlParser.parseToJsonString(xml, "");
        return json;
    }

    private static String getXmlFilePath(String filename){
        return FileHandler.getFilePath(XML_FILES_FOLDER, filename);
    }

    private static String readJsonFile(String filename){
        return FileHandler.readFileAsString(JSON_FILES_FOLDER, filename);
    }



}
