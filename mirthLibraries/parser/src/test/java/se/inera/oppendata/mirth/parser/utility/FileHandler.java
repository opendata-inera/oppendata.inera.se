package se.inera.oppendata.mirth.parser.utility;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by davsan on 2017-10-25.
 */
public class FileHandler {

    public static String getFilePath(String foldername, String filename){
        String filePath = FileHandler.class.getClassLoader().getResource(foldername+"/"+filename).getPath();
        if(filePath.startsWith("/")){
            filePath = filePath.substring(1);
        }
        return filePath;
    }

    public static String readFileAsString(String foldername,String filename){
        String filePath = getFilePath(foldername,filename);
        try {
            byte[] jsonBytes = readBytesFromFile(filePath);
            return new String(jsonBytes);
        }catch(IOException e){
            return e.getMessage();
        }
    }

    public static byte[] readBytesFromFile(String filepath) throws IOException {
        Path path = Paths.get(filepath);
        byte[] fileContent = Files.readAllBytes(path);
        return fileContent;
    }

    public static void saveToFile(String content, String filePath){
        try {
            byte[] contentAsBytes = content.getBytes("UTF-8");
            FileOutputStream fos = new FileOutputStream(filePath);
            fos.write(contentAsBytes);
            fos.close();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
