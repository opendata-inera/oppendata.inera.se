package se.inera.oppendata.mirth.parser.quality_indicators;

import org.json.simple.JSONObject;
import se.inera.oppendata.mirth.soap.quality_indicators.*;

import java.math.BigInteger;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * Created by davsan on 2017-10-23.
 */
public class MeasurementJsonObject extends JSONObject {
    private static String reasonCodeSystem = "";

    public static void setReasonCodeSystem(String reasonCodeSystem){
        MeasurementJsonObject.reasonCodeSystem = reasonCodeSystem;
    }

    public static String getReasonCodeSystem(){
        return reasonCodeSystem;
    }

    public static void resetReasonCodeSystem(){
        reasonCodeSystem = "";
    }

    public MeasurementJsonObject(){
    }

    public MeasurementJsonObject(MeasurementType measurementType, String aggregatedQualityReportId){
        addMeasurementType(measurementType);
        addAggregatedQualityReportId(aggregatedQualityReportId);
    }

    public MeasurementJsonObject(String measurementId, MeasurementType measurementType, String aggregatedQualityReportId){
        addMeasurementType(measurementType);
        addAggregatedQualityReportId(aggregatedQualityReportId);
        addMeasurementId(measurementId);
    }

    public void addMeasurementId(String measurementId){
        put(MeasurementJsonAttributes.MEASUREMENT_ID, measurementId);
    }

    public void addAggregatedQualityReportId(String aggregatedQualityReportId){
        put(MeasurementJsonAttributes.AGGREGATED_QUALITY_REPORT_ID, aggregatedQualityReportId);
    }

    public void addParentMeasureId(String parentMeasureId){
        put(MeasurementJsonAttributes.PARENT_MEASURE_ID, parentMeasureId);
    }

    public void addMeasurementType(MeasurementType measurementType){
        addMeasureId(measurementType.getMeasureId());
        addMeasureVersion(measurementType.getMeasureIdVersionNumber());
        addMeasurePeriod(measurementType.getMeasurePeriod());
        addFirstServiceEncounter(measurementType.getFirstServiceEncounter());
        addLastServiceEncounter(measurementType.getLastServiceEncounter());
        addPerformingOrganization(measurementType.getPerformingOrganization());
        addMissingMeasurement(measurementType.getMissingMeasure());
        addContinuousVariableMeasure(measurementType.getContinuousVariableMeasure());
        addProportionMeasure(measurementType.getProportionMeasure());
        addCohortMeasure(measurementType.getCohortMeasure());
        addSourceSystem(measurementType.getSourceSystem());
    }

    public String getMeasurementId(){
        if(get(MeasurementJsonAttributes.MEASUREMENT_ID) == null){
            return null;
        }
        return get(MeasurementJsonAttributes.MEASUREMENT_ID).toString();
    }

    public String getPerformingOrganizationId(){
        if(get(MeasurementJsonAttributes.PERFORMING_ORGANIZATION_ID) == null){
            return null;
        }
        return get(MeasurementJsonAttributes.PERFORMING_ORGANIZATION_ID).toString();
    }

    public String getMeasureId(){
        if(get(MeasurementJsonAttributes.MEASURE_ID) == null){
            return null;
        }
        return get(MeasurementJsonAttributes.MEASURE_ID).toString();
    }

    private void addMeasureId(IIType measureId){
        if(measureId != null){
            if(measureId.getExtension() != null){
                put(MeasurementJsonAttributes.MEASURE_ID, measureId.getExtension());
                return;
            }
        }
        put(MeasurementJsonAttributes.MEASURE_ID, null);
    }

    private void addMeasureVersion(BigInteger measureIdVersionNumber){
        String measureVersion = null;
        if(measureIdVersionNumber != null){
            measureVersion = measureIdVersionNumber.toString();
        }
        put(MeasurementJsonAttributes.MEASURE_VERSION, measureVersion);
    }

    private void addFirstServiceEncounter(String firstServiceEncounter){
        String date = null;
        if(firstServiceEncounter != null){
            date = LocalDate.parse(firstServiceEncounter, DateTimeFormatter.BASIC_ISO_DATE).toString();
        }
        put(MeasurementJsonAttributes.FIRST_SERVICE_ENCOUNTER, date);
    }

    private void addLastServiceEncounter(String lastServiceEncounter){
        String date = null;
        if(lastServiceEncounter != null){
            date = LocalDate.parse(lastServiceEncounter, DateTimeFormatter.BASIC_ISO_DATE).toString();
        }
        put(MeasurementJsonAttributes.LAST_SERVICE_ENCOUNTER, date);
    }

    private void addMeasurePeriod(DatePeriodType measurePeriod){
        String start = null;
        String end = null;
        if(measurePeriod != null){
            start = measurePeriod.getStart(DateTimeFormatter.BASIC_ISO_DATE);
            end = measurePeriod.getEnd(DateTimeFormatter.BASIC_ISO_DATE);
        }
        put(MeasurementJsonAttributes.MEASURE_PERIOD_START, start);
        put(MeasurementJsonAttributes.MEASURE_PERIOD_END, end);
    }

    private void addPerformingOrganization(PerformingOrganizationType performingOrganizationType){
        String hsaId = null;
        String orgId = null;
        if(performingOrganizationType != null){
            if(performingOrganizationType.getHsaId() != null){
                hsaId = performingOrganizationType.getHsaId();
                orgId = hsaId;
            }else if(performingOrganizationType.getOrganizationId() != null){
                orgId = performingOrganizationType.getOrganizationId().getExtension();
            }
        }
        put(MeasurementJsonAttributes.PERFORMING_ORGANIZATION_HSA_ID, hsaId);
        put(MeasurementJsonAttributes.PERFORMING_ORGANIZATION_ID, orgId);
    }

    private void addMissingMeasurement(MissingMeasureType missingMeasureType){
        if(missingMeasureType != null){
            JSONObject missingMeasure = new JSONObject();
            JSONObject reasonCode = null;
            if(missingMeasureType.getReasonCode() != null){
                reasonCode = new JSONObject();
                String code = missingMeasureType.getReasonCode().value();
                reasonCode.put(SharedJsonAttributes.CODE, code);
                reasonCode.put(SharedJsonAttributes.SYSTEM, reasonCodeSystem);
            }
            missingMeasure.put(MeasurementJsonAttributes.REASON_CODE, reasonCode);
            put(MeasurementJsonAttributes.MISSING_MEASUREMENT, missingMeasure);
        }else{
            put(MeasurementJsonAttributes.MISSING_MEASUREMENT, null);
        }
    }

    private void addContinuousVariableMeasure(ContinuousVariableMeasureType continuousVariableMeasureType){
        if(continuousVariableMeasureType != null){
            JSONObject continuousVariableMeasure = new JSONObject();
            continuousVariableMeasure.put(MeasurementJsonAttributes.MEASURE_POPULATION, continuousVariableMeasureType.getMeasurePopulation());
            continuousVariableMeasure.put(MeasurementJsonAttributes.VALUE, continuousVariableMeasureType.getValue());

            addConfidenceInterval(continuousVariableMeasure, continuousVariableMeasureType.getConfidenceInterval95Percent());
            addStandardDeviation(continuousVariableMeasure, continuousVariableMeasureType.getStandardDeviation());
            addCoverage(continuousVariableMeasure, continuousVariableMeasureType.getCoverage());
            addExclusions(continuousVariableMeasure, continuousVariableMeasureType.getExclusions());
            addReferenceIntervalValue(continuousVariableMeasure, continuousVariableMeasureType.getReferenceIntervalValue());

            put(MeasurementJsonAttributes.CONTINUOUS_VARIABLE_MEASURE, continuousVariableMeasure);

        }else{
            put(MeasurementJsonAttributes.CONTINUOUS_VARIABLE_MEASURE, null);
        }
    }

    private void addProportionMeasure(ProportionMeasureType proportionMeasureType){
        if(proportionMeasureType != null){
            JSONObject proportionMeasure = new JSONObject();
            proportionMeasure.put(MeasurementJsonAttributes.RATE, proportionMeasureType.getRate());
            proportionMeasure.put(MeasurementJsonAttributes.DENOMINATOR, proportionMeasureType.getDenominator());
            proportionMeasure.put(MeasurementJsonAttributes.NUMERATOR, proportionMeasureType.getNumerator());

            addConfidenceInterval(proportionMeasure, proportionMeasureType.getConfidenceInterval95Percent());
            addStandardDeviation(proportionMeasure, proportionMeasureType.getStandardDeviation());
            addCoverage(proportionMeasure, proportionMeasureType.getCoverage());
            addExclusions(proportionMeasure, proportionMeasureType.getExclusions());
            addReferenceIntervalRate(proportionMeasure, proportionMeasureType.getReferenceIntervalRate());

            put(MeasurementJsonAttributes.PROPORTION_MEASURE, proportionMeasure);
        }else{
            put(MeasurementJsonAttributes.PROPORTION_MEASURE, null);
        }
    }

    private void addCohortMeasure(CohortMeasureType cohortMeasureType){
        if(cohortMeasureType != null){
            JSONObject cohortMeasure = new JSONObject();
            cohortMeasure.put(MeasurementJsonAttributes.COHORT, cohortMeasureType.getCohort());
            addConfidenceInterval(cohortMeasure, cohortMeasureType.getConfidenceInterval95Percent());
            addCoverage(cohortMeasure, cohortMeasureType.getCoverage());
            addStandardDeviation(cohortMeasure, cohortMeasureType.getStandardDeviation());
            addExclusions(cohortMeasure, cohortMeasureType.getExclusions());
            addReferenceIntervalValue(cohortMeasure, cohortMeasureType.getReferenceIntervalValue());

            put(MeasurementJsonAttributes.COHORT_MEASURE, cohortMeasure);
        }else{
            put(MeasurementJsonAttributes.COHORT_MEASURE, null);
        }
    }

    private void addConfidenceInterval(JSONObject jsonObj, ConfidenceInterval95PercentType confidenceInterval){
        if(confidenceInterval != null){
            jsonObj.put(MeasurementJsonAttributes.CONFIDENCE_INTERVAL_95_PERCENT_LOW , confidenceInterval.getLow());
            jsonObj.put(MeasurementJsonAttributes.CONFIDENCE_INTERVAL_95_PERCENT_HIGH , confidenceInterval.getHigh());
        }else{
            jsonObj.put(MeasurementJsonAttributes.CONFIDENCE_INTERVAL_95_PERCENT_LOW , null);
            jsonObj.put(MeasurementJsonAttributes.CONFIDENCE_INTERVAL_95_PERCENT_HIGH , null);
        }
    }

    private void addStandardDeviation(JSONObject jsonObj, Double standardDeviation){
        jsonObj.put(MeasurementJsonAttributes.STANDARD_DEVIATION, standardDeviation);
    }

    private void addCoverage(JSONObject jsonObj, Double coverage){
        jsonObj.put(MeasurementJsonAttributes.COVERAGE, coverage);
    }

    private void addExclusions(JSONObject jsonObj, Double exclusions){
        jsonObj.put(MeasurementJsonAttributes.EXCLUSIONS, exclusions);
    }

    private void addReferenceIntervalValue(JSONObject jsonObj, Double referenceIntervalValue){
        jsonObj.put(MeasurementJsonAttributes.REFERENCE_INTERVAL_VALUE, referenceIntervalValue);
    }

    private void addReferenceIntervalRate(JSONObject jsonObj, Double referenceIntervalRate){
        jsonObj.put(MeasurementJsonAttributes.REFERENCE_INTERVAL_RATE, referenceIntervalRate);
    }

    private void addSourceSystem(SourceSystemType sourceSystemType){
        if(sourceSystemType != null){
            JSONObject sourceSystemJsonObj = new JSONObject();
            sourceSystemJsonObj.put(MeasurementJsonAttributes.NAME, sourceSystemType.getName());
            String systemId = null;
            if(sourceSystemType.getSystemId() != null && sourceSystemType.getSystemId().getExtension() != null){
                systemId = sourceSystemType.getSystemId().getExtension();
            }
            sourceSystemJsonObj.put(MeasurementJsonAttributes.SYSTEM_ID, systemId);
            put(MeasurementJsonAttributes.SOURCE_SYSTEM, sourceSystemJsonObj);
        }else{
            put(MeasurementJsonAttributes.SOURCE_SYSTEM, null);
        }
    }
}
