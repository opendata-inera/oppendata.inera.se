package se.inera.oppendata.mirth.parser.quality_indicators;

import org.json.simple.JSONObject;
import se.inera.oppendata.mirth.soap.quality_indicators.IIType;
import se.inera.oppendata.mirth.soap.quality_indicators.MeasurementType;
import se.inera.oppendata.mirth.soap.quality_indicators.OrganizationTypeCodeEnum;
import se.inera.oppendata.mirth.soap.quality_indicators.PerformingOrganizationType;

/**
 * Created by davsan on 2017-10-26.
 */
public class PerformingOrganizationJsonObject extends JSONObject {
    private static String performingOrganizationTypeSystem = "";
    private static boolean includePerformingOrganizationIdInChildren = false;

    public static void setPerformingOrganizationTypeSystem(String performingOrganizationTypeSystem){
        PerformingOrganizationJsonObject.performingOrganizationTypeSystem = performingOrganizationTypeSystem;
    }

    public static String getPerformingOrganizationTypeSystem(){
        return performingOrganizationTypeSystem;
    }

    public static void resetPerformingOrganizationTypeSystem(){
        performingOrganizationTypeSystem = "";
    }

    public static boolean includePerformingOrganizationIdInChildren(){
        return includePerformingOrganizationIdInChildren;
    }

    public static void setIncludePerformingOrganizationIdInChildren(boolean includePerformingOrganizationIdInChildren){
        PerformingOrganizationJsonObject.includePerformingOrganizationIdInChildren = includePerformingOrganizationIdInChildren;
    }

    public PerformingOrganizationJsonObject(){}

    public PerformingOrganizationJsonObject(PerformingOrganizationType performingOrganizationType){
        addPerformingOrganizationType(performingOrganizationType, true);
    }

    public PerformingOrganizationJsonObject(MeasurementType measurementType){
        if(measurementType != null){
            addPerformingOrganizationType(measurementType.getPerformingOrganization(), true);
        }
    }

    private void addPerformingOrganizationType(PerformingOrganizationType performingOrganizationType, boolean isParentObject){
        if(performingOrganizationType != null){
            addHsaId(performingOrganizationType.getHsaId(), isParentObject);
            if(isParentObject || includePerformingOrganizationIdInChildren){
                addId(performingOrganizationType.getOrganizationId());
            }

            addName(performingOrganizationType.getOrganizationName());
            addType(performingOrganizationType.getOrganizationType());
            if(performingOrganizationType.getAsOrganizationPartOf() != null){
                PerformingOrganizationJsonObject jsonObj = new PerformingOrganizationJsonObject();
                jsonObj.addPerformingOrganizationType(performingOrganizationType.getAsOrganizationPartOf(), false);
                put(PerformingOrganizationJsonAttributes.AS_ORGANIZATION_PART_OF, jsonObj);
            }else{
                put(PerformingOrganizationJsonAttributes.AS_ORGANIZATION_PART_OF, null);
            }
        }
    }

    private void addId(IIType iiType){
        if(iiType != null && iiType.getExtension() != null){
            put(PerformingOrganizationJsonAttributes.PERFORMING_ORGANIZATION_ID, iiType.getExtension());
        }
    }

    private void addHsaId(String hsaId, boolean isParent){
        put(PerformingOrganizationJsonAttributes.PERFORMING_ORGANIZATION_HSA_ID, hsaId);
        if(isParent){
            put(PerformingOrganizationJsonAttributes.PERFORMING_ORGANIZATION_ID, hsaId);
        }
    }

    private void addName(String name){
        put(PerformingOrganizationJsonAttributes.PERFORMING_ORGANIZATION_NAME, name);
    }

    private void addType(OrganizationTypeCodeEnum orgTypeEnum){
        JSONObject jsonObject = null;
        if(orgTypeEnum != null){
            jsonObject = new JSONObject();
            String code = orgTypeEnum.value();
            jsonObject.put(SharedJsonAttributes.CODE, code);
            jsonObject.put(SharedJsonAttributes.SYSTEM, performingOrganizationTypeSystem);
        }
        put(PerformingOrganizationJsonAttributes.PERFORMING_ORGANIZATION_TYPE, jsonObject);
    }
}
