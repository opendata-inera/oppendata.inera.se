package se.inera.oppendata.mirth.parser.xml;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.management.modelmbean.XMLParseException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by davsan on 2017-07-18.
 */
public class XMLParser {
    private DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();;
    private DocumentBuilder builder;
    private ArrayList<String> parentTagNamesToInterpretAsArrayRoot;
    private ArrayList<String> tagNamesAlwaysPartOfArray;
    private ArrayList<String> tagNamesToAlwaysInterpretAsStrings;
    private boolean interpretAllValuesAsStrings;


    public XMLParser(){
        initDocumentBuilder();
        interpretAllValuesAsStrings = false;
        parentTagNamesToInterpretAsArrayRoot = new ArrayList<>();
        tagNamesAlwaysPartOfArray = new ArrayList<>();
        tagNamesToAlwaysInterpretAsStrings = new ArrayList<>();
    }

    private void initDocumentBuilder(){
        try {
            builder = factory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    public void addParentTagNameToInterpretAsArrayRoot(String tagName){
        if(!parentTagNamesToInterpretAsArrayRoot.contains(tagName)){
            parentTagNamesToInterpretAsArrayRoot.add(tagName);
        }
    }

    public void addTagNameThatShouldAlwaysBePartOfArray(String tagName){
        if(!tagNamesAlwaysPartOfArray.contains(tagName)){
            tagNamesAlwaysPartOfArray.add(tagName);
        }
    }

    public void addTagNameThatShouldAlwaysBeInterpretedAsString(String tagName){
        if(!tagNamesToAlwaysInterpretAsStrings.contains(tagName)){
            tagNamesToAlwaysInterpretAsStrings.add(tagName);
        }
    }

    public void setInterpretAllValuesAsStrings(boolean interpretAllValuesAsStrings){
        this.interpretAllValuesAsStrings = interpretAllValuesAsStrings;
    }

    public JSONObject parseToJsonObject(byte[] xml, String newRootElementName){
        try{
            Document doc = createDocFromXml(xml);
            Element root = doc.getDocumentElement();
            if(newRootElementName != null && !newRootElementName.isEmpty()){
                doc.renameNode(root, root.getNamespaceURI(), newRootElementName);
            }
            JSONObject jsonObject = new JSONObject();
            Object obj = createJsonFromXML(root);
            if(obj != null){
                jsonObject.put(root.getTagName(), obj);
            }
            return jsonObject;
        }catch(Exception e){
            JSONObject obj = new JSONObject();
            obj.put("error", e.getMessage());
            return obj;
        }
    }

    public String parseToJsonString(byte[] xml, String newRootElementName){
        return parseToJsonObject(xml, newRootElementName).toJSONString();
    }


    private String getPrefixFreeTagName(String tagName){
        if(tagName.contains(":")){
            String[] splitTagName = tagName.split(":");
            return splitTagName[splitTagName.length-1];
        }
        return tagName;
    }

    private Document createDocFromXml(byte[] xml) throws DocumentBuilderException{
        ByteArrayInputStream input = new ByteArrayInputStream(xml);
        try {
            return builder.parse(input);
        } catch (SAXException e) {
            throw new DocumentBuilderException(e.getMessage());
        } catch (IOException e) {
            throw new DocumentBuilderException(e.getMessage());
        }
    }


    private void addXmlListAsChildrenInXmlElement(XMLList xmlList, Element xmlElement){
        for(int i = 0; i < xmlList.size(); i++){
            xmlElement.appendChild(xmlList.get(i));
        }
    }

    private void removeAllChildren(Element xmlElement){
        for(int i = 0; i < xmlElement.getChildNodes().getLength(); i++){
            xmlElement.removeChild(xmlElement.getChildNodes().item(i));
        }
    }

    private Object createJsonFromXML(Element xmlElement) throws XMLParseException{
        XMLList children = new XMLList(xmlElement.getChildNodes());
        if(children.isEmpty()){
            String text = getLastXmlChildText(xmlElement);
            return parseToCorrectType(text, xmlElement.getTagName());
        }else{
            if(isArrayRoot(children, xmlElement)){
                JSONArray jsonArray = new JSONArray();
                addAllChildrenToJsonArray(children, jsonArray);
                if(!jsonArray.isEmpty()){
                    return jsonArray;
                }
                return null;
            }
            else{
                JSONObject newJsonObj = new JSONObject();
                int i = 0;
                int siblingIndex;
                ArrayList<String>  childTagNames = new ArrayList<>();
                while(i < children.size()){
                    siblingIndex = i+1;
                    Element child = children.get(i);
                    String tag = child.getTagName();
                    if(childTagNames.contains(tag)){
                        throw new XMLParseException("Two elements at the same level, with the same tag should be in direct succession.");
                    }
                    childTagNames.add(tag);
                    JSONArray jsonArray = new JSONArray();
                    while(siblingIndex < children.size()){
                        Element sibling = children.get(siblingIndex);
                        if(sibling.getTagName() == tag){
                            Object obj = createJsonFromXML(sibling);
                            addNonEmptyJsonObjectToJsonArray(jsonArray, obj);
                        }
                        else{
                            break;
                        }
                        siblingIndex++;
                    }

                    if(!jsonArray.isEmpty() || shouldAlwaysBeAnArray(tag)){
                        Object obj = createJsonFromXML(child);
                        addNonEmptyJsonObjectToJsonArray(jsonArray, obj);
                    }
                    if(!jsonArray.isEmpty()){
                        newJsonObj.put(tag,jsonArray);
                    }else{
                        Object obj = createJsonFromXML(child);
                        if(obj != null){
                            newJsonObj.put(child.getTagName(), obj);
                        }
                    }

                    i = siblingIndex;
                }
                return newJsonObj.size() == 0 ? null : newJsonObj;
            }
        }

    }


    private ArrayList<Element> getXMLChildren(Element xmlElement){
        ArrayList<Element> xmlChildren = new ArrayList<>();
        NodeList allChildren = xmlElement.getChildNodes();
        for(int i= 0; i < allChildren.getLength(); i++){
            if(isXMLElement(allChildren.item(i))){
                xmlChildren.add((Element)allChildren.item(i));
            }
        }
        return xmlChildren;
    }

    private boolean isArrayRoot(XMLList children, Element parent) throws XMLParseException{
        if(parentTagNamesToInterpretAsArrayRoot.contains(parent.getTagName())){
            if(children.numberOfDistinctTagNames() > 1){
                throw new XMLParseException("More than one tag name found among the children.");
            }
            return true;
        }
        return false;
    }

    private boolean isXMLElement(Node node){
        return node.getNodeType() == Node.ELEMENT_NODE;
    }

    private boolean shouldAlwaysBeAnArray(String tagName){
        return tagNamesAlwaysPartOfArray.contains(tagName);
    }

    private String getLastXmlChildText(Element xmlElement){
        if(xmlElement.hasChildNodes()){
            return xmlElement.getFirstChild().getTextContent();
        }
        return null;
    }

    private Object parseToCorrectType(String text, String tagName){
        if(text == null){
            return null;
        }

        if(interpretAllValuesAsStrings || text.contains(" ") || tagNamesToAlwaysInterpretAsStrings.contains(tagName)){
            return text;
        }

        Scanner sc = new Scanner(text);
        if(sc.hasNextBoolean()){
            return sc.nextBoolean();
        }
        else if(sc.hasNextInt()){
            return sc.nextInt();
        }
        else if(sc.hasNextFloat()){
            return sc.nextFloat();
        }
        else{
            return text;
        }
    }

    private void addAllChildrenToJsonArray(XMLList children, JSONArray jsonArray) throws  XMLParseException{
        for(int i = 0; i < children.size(); i++){
            Object obj = createJsonFromXML(children.get(i));
            addNonEmptyJsonObjectToJsonArray(jsonArray, obj);
        }
    }

    private void addNonEmptyJsonObjectToJsonArray(JSONArray jsonArray, Object obj){
        if(obj != null){
            jsonArray.add(obj);
        }
    }
}
