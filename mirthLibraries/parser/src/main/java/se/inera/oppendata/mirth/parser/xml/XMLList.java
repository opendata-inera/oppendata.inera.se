package se.inera.oppendata.mirth.parser.xml;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.ArrayList;

/**
 * Created by davsan on 2017-07-28.
 */
public class XMLList {
    private ArrayList<Element> xmlElements;
    private ArrayList<String> tagNames;

    public XMLList(){
        tagNames = new ArrayList<>();
        xmlElements = new ArrayList<>();
    }

    public XMLList(NodeList nodes){
        tagNames = new ArrayList<>();
        xmlElements = new ArrayList<>();
        append(nodes);
    }

    public void add(Element xmlElement){
        xmlElements.add(xmlElement);
        if(!tagNames.contains(xmlElement.getTagName())){
            tagNames.add(xmlElement.getTagName());
        }
    }

    public boolean remove(Element xmlElement){
        String tagName = xmlElement.getTagName();
        boolean success = xmlElements.remove(xmlElement);
        if(success && !containsElementWithTagName(tagName)){
            tagNames.remove(tagName);
        }
        return success;
    }

    public Element get(int index){
        return xmlElements.get(index);
    }

    public ArrayList<String> getAllTagNames(){
        return tagNames;
    }

    public boolean hasTagName(String tagName){
        return tagNames.contains(tagName);
    }

    public int numberOfDistinctTagNames(){
        return tagNames.size();
    }

    public boolean isEmpty(){
        return xmlElements.isEmpty();
    }

    public int size(){
        return xmlElements.size();
    }

    private boolean containsElementWithTagName(String tagName){
        for(int i = 0; i < xmlElements.size(); i++){
            if(xmlElements.get(i).getTagName() == tagName){
                return true;
            }
        }
        return false;
    }

    private void append(NodeList nodeList){
        for(int i = 0; i < nodeList.getLength(); i++){
            if(nodeList.item(i).getNodeType() == Node.ELEMENT_NODE){
                add((Element)nodeList.item(i));
            }
        }
    }
}
