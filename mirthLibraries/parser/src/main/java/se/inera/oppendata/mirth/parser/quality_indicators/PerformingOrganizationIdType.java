package se.inera.oppendata.mirth.parser.quality_indicators;

/**
 * Created by davsan on 2017-11-23.
 */
public enum PerformingOrganizationIdType {
    HSA,
    ORGANIZATION
}
