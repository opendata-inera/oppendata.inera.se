package se.inera.oppendata.mirth.parser.quality_indicators;

/**
 * Created by davsan on 2017-10-23.
 */
public class PerformingOrganizationJsonAttributes {

    public final static String PERFORMING_ORGANIZATION_ID = "performingOrganizationId";
    public final static String PERFORMING_ORGANIZATION_HSA_ID = "performingOrganizationHsaId";
    public final static String AS_ORGANIZATION_PART_OF = "asOrganizationPartOf";
    public final static String PERFORMING_ORGANIZATION_TYPE = "performingOrganizationType";
    public final static String PERFORMING_ORGANIZATION_NAME = "performingOrganizationName";

}
