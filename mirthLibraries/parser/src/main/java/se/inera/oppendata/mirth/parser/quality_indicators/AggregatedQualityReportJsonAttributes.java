package se.inera.oppendata.mirth.parser.quality_indicators;

/**
 * Created by davsan on 2017-10-23.
 */
public class AggregatedQualityReportJsonAttributes {

    public final static String MEASURE_ID = "measureId";
    public final static String MEASUREMENT_CHECKSUM = "measurementChecksum";
    public final static String AGGREGATED_QUALITY_REPORT_ID = "aggregatedQualityReportId";
    public final static String REPORTING_PERIOD_FROM = "reportingPeriodFrom";
    public final static String REPORTING_PERIOD_TO = "reportingPeriodTo";
    public final static String SYNCHRONIZATION_TIME = "synchronizationTime";
    public final static String REPORTING_SYSTEM_HSA_ID = "reportingSystemHsaId";
    public final static String REPORTING_ORGANIZATION_HSA_ID = "reportingOrganizationHsaId";


}
