package se.inera.oppendata.mirth.parser.quality_indicators;

/**
 * Created by davsan on 2017-10-18.
 */
public class MeasurementJsonAttributes {

    public final static String MEASUREMENT_ID = "measurementId";
    public final static String MEASURE_ID = "measureId";
    public final static String MEASURE_VERSION = "measureVersion";
    public final static String MEASURE_PERIOD_START = "measurePeriodStart";
    public final static String MEASURE_PERIOD_END = "measurePeriodEnd";
    public final static String FIRST_SERVICE_ENCOUNTER = "firstServiceEncounter";
    public final static String LAST_SERVICE_ENCOUNTER = "lastServiceEncounter";
    public final static String PERFORMING_ORGANIZATION_HSA_ID = "performingOrganizationHsaId";
    public final static String PERFORMING_ORGANIZATION_ID = "performingOrganizationId";

    public final static String MISSING_MEASUREMENT = "missingMeasurement";
    public final static String REASON_CODE  = "reasonCode";

    public final static String CONTINUOUS_VARIABLE_MEASURE = "continuousVariableMeasure";
    public final static String MEASURE_POPULATION = "measurePopulation";
    public final static String VALUE = "value";

    public final static String PROPORTION_MEASURE = "proportionMeasure";
    public final static String RATE = "rate";
    public final static String DENOMINATOR = "denominator";
    public final static String NUMERATOR = "numerator";

    public final static String COHORT_MEASURE = "cohortMeasure";
    public final static String COHORT = "cohort";

    public final static String CONFIDENCE_INTERVAL_95_PERCENT_LOW = "confidenceInterval95PercentLow";   //Also used in ProportionMeasure
    public final static String CONFIDENCE_INTERVAL_95_PERCENT_HIGH = "confidenceInterval95PercentHigh"; //Also used in ProportionMeasure
    public final static String STANDARD_DEVIATION = "standardDeviation";
    public final static String COVERAGE = "coverage";
    public final static String EXCLUSIONS = "exclusions";
    public final static String REFERENCE_INTERVAL_VALUE = "referenceIntervalValue";
    public final static String REFERENCE_INTERVAL_RATE = "referenceIntervalRate";

    public final static String PARENT_MEASURE_ID = "parentMeasureId";
    public final static String AGGREGATED_QUALITY_REPORT_ID = "aggregatedQualityReportId";
    public final static String SYSTEM_ID = "systemId";
    public final static String SOURCE_SYSTEM = "sourceSystem";
    public final static String NAME = "name";

}
