package se.inera.oppendata.mirth.parser.xml;

/**
 * Created by davsan on 2017-07-21.
 */
public class DocumentBuilderException extends Exception {
    public DocumentBuilderException(String message){
        super(message);
    }
}
