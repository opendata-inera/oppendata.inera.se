package se.inera.oppendata.mirth.parser.quality_indicators;

import org.json.simple.JSONArray;
import se.inera.oppendata.mirth.soap.quality_indicators.*;

import java.util.List;

/**
 * Created by davsan on 2017-10-18.
 */
public class QualityIndicatorsParser {
    private JSONArray measurementsJson;
    private JSONArray performingOrganizationsJson;
    private JSONArray aggregatedQualityReportsJson;
    private String parentMeasureId;
    private String aggregatedQualityReportId;

    public QualityIndicatorsParser(){
        measurementsJson = new JSONArray();
        aggregatedQualityReportsJson = new JSONArray();
    }

    public void setParentMeasureId(String parentMeasureId){
        this.parentMeasureId = parentMeasureId;
    }

    public void setAggregatedQualityReportId(String aggregatedQualityReportId){
        this.aggregatedQualityReportId = aggregatedQualityReportId;
    }

    public void parseAggregatedQualityReportDetailsToJson(String measureId, CustomAggregatedQualityReportDetails reportingDetails){
        AggregatedQualityReportJsonObject aggregatedQualityReportJsonObject = new AggregatedQualityReportJsonObject(aggregatedQualityReportId, reportingDetails, measureId);
        aggregatedQualityReportsJson.add(aggregatedQualityReportJsonObject);
    }

    public void parseMeasurementsToJson(List<MeasurementType> measurements){
        if(measurements != null){
            for(int i = 0; i < measurements.size(); i++){

                MeasurementType measurementType = measurements.get(i);
                parseMeasurementType(measurementType);
                parsePerformingOrganizationTypeToJson(measurementType.getPerformingOrganization());
            }
        }
    }

    public void parseMeasurementType(MeasurementType measurementType){
        if(measurementType != null){
            MeasurementJsonObject measurementJsonObj = new MeasurementJsonObject(measurementType, aggregatedQualityReportId);
            if(parentMeasureId != null){
                measurementJsonObj.addParentMeasureId(parentMeasureId);
            }
            measurementsJson.add(measurementJsonObj);
        }
    }

    public void parsePerformingOrganizationTypeToJson(PerformingOrganizationType performingOrganizationType){
        if(performingOrganizationType != null){
            PerformingOrganizationJsonObject jsonObj = new PerformingOrganizationJsonObject(performingOrganizationType);
            performingOrganizationsJson.add(jsonObj);
        }
    }

    public String getMeasurementsAsJsonString(){
        return measurementsJson.toJSONString();
    }

    public String getAggregatedQualityReportsAsJsonString(){
        return aggregatedQualityReportsJson.toJSONString();
    }

    public String getLastAggregatedQualityReportAsString(){
            int lastIndex = aggregatedQualityReportsJson.size()-1;
            if(lastIndex != -1){
                return aggregatedQualityReportsJson.get(lastIndex).toString();
            }
            return null;
    }

    public JSONArray getMeasurementsJson(){
        return measurementsJson;
    }

    public void clearMeasurements(){
        measurementsJson.clear();
    }



}
