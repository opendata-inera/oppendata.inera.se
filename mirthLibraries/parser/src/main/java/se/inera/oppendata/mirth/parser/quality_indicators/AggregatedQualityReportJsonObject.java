package se.inera.oppendata.mirth.parser.quality_indicators;

import org.json.simple.JSONObject;
import se.inera.oppendata.mirth.soap.quality_indicators.CustomAggregatedQualityReportDetails;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * Created by davsan on 2017-10-23.
 */
public class AggregatedQualityReportJsonObject extends JSONObject {

    public AggregatedQualityReportJsonObject(String aggregatedQualityReportId, CustomAggregatedQualityReportDetails reportDetails, String measureId){
        addAggregatedQualityReportId(aggregatedQualityReportId);
        addReportDetails(reportDetails);
        addMeasureId(measureId);
    }

    public AggregatedQualityReportJsonObject(){}

    public void addReportDetails(CustomAggregatedQualityReportDetails customAggregatedQualityReportDetails){
        if(customAggregatedQualityReportDetails != null){
            put(AggregatedQualityReportJsonAttributes.MEASUREMENT_CHECKSUM, customAggregatedQualityReportDetails.getMeasurementChecksum());
            put(AggregatedQualityReportJsonAttributes.REPORTING_PERIOD_FROM, customAggregatedQualityReportDetails.getReportingPeriodStart(DateTimeFormatter.BASIC_ISO_DATE));
            put(AggregatedQualityReportJsonAttributes.REPORTING_PERIOD_TO, customAggregatedQualityReportDetails.getReportingPeriodEnd(DateTimeFormatter.BASIC_ISO_DATE));
            put(AggregatedQualityReportJsonAttributes.REPORTING_ORGANIZATION_HSA_ID, customAggregatedQualityReportDetails.getReportingOrganizationHsaId());
            put(AggregatedQualityReportJsonAttributes.REPORTING_SYSTEM_HSA_ID, customAggregatedQualityReportDetails.getReportingSystemHsaId());
        }
    }

    public void addMeasureId(String measureId){
        put(AggregatedQualityReportJsonAttributes.MEASURE_ID, measureId);
    }

    public void addSynchronizationTimeFromCurrentLocalTime(){
        String synchronizationTime = LocalDate.now().toString();
        put(AggregatedQualityReportJsonAttributes.SYNCHRONIZATION_TIME, synchronizationTime);
    }

    public void addSynchronizationTime(String date){
        put(AggregatedQualityReportJsonAttributes.SYNCHRONIZATION_TIME, date);
    }

    public void addAggregatedQualityReportId(String reportId){
        put(AggregatedQualityReportJsonAttributes.AGGREGATED_QUALITY_REPORT_ID, reportId);
    }

    public String getAggregatedQualityReportId(){
        if(get(AggregatedQualityReportJsonAttributes.AGGREGATED_QUALITY_REPORT_ID) == null){
            return null;
        }
        return get(AggregatedQualityReportJsonAttributes.AGGREGATED_QUALITY_REPORT_ID).toString();
    }

    public String getMeasureId(){
        if(get(AggregatedQualityReportJsonAttributes.MEASURE_ID) == null){
            return null;
        }
        return get(AggregatedQualityReportJsonAttributes.MEASURE_ID).toString();
    }

    public String getSynchronizationTime(){
        if(get(AggregatedQualityReportJsonAttributes.SYNCHRONIZATION_TIME) == null){
            return null;
        }
        return get(AggregatedQualityReportJsonAttributes.SYNCHRONIZATION_TIME).toString();
    }

    public String getMeasurementChecksum(){
        if(get(AggregatedQualityReportJsonAttributes.MEASUREMENT_CHECKSUM) == null){
            return null;
        }
        return get(AggregatedQualityReportJsonAttributes.MEASUREMENT_CHECKSUM).toString();
    }

    public String getReportingPeriodStart(){
        if(get(AggregatedQualityReportJsonAttributes.REPORTING_PERIOD_FROM) == null){
            return null;
        }
        return get(AggregatedQualityReportJsonAttributes.REPORTING_PERIOD_FROM).toString();
    }

    public String getReportingPeriodEnd(){
        if(get(AggregatedQualityReportJsonAttributes.REPORTING_PERIOD_TO) == null){
            return null;
        }
        return get(AggregatedQualityReportJsonAttributes.REPORTING_PERIOD_TO).toString();
    }

    public String getReportingOrganizationHsaId(){
        if(get(AggregatedQualityReportJsonAttributes.REPORTING_ORGANIZATION_HSA_ID) == null){
            return null;
        }
        return get(AggregatedQualityReportJsonAttributes.REPORTING_ORGANIZATION_HSA_ID).toString();
    }

    public String getReportingSystemHsaId(){
        if(get(AggregatedQualityReportJsonAttributes.REPORTING_SYSTEM_HSA_ID) == null){
            return null;
        }
        return get(AggregatedQualityReportJsonAttributes.REPORTING_SYSTEM_HSA_ID).toString();
    }

}
