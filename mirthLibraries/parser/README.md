## Parser library for Mirth Connect
This is a Parser library to be used in mirth connect. It offers the functionality to parse data to various formats. The following packages are included in the project.
1. se.inera.oppendata.mirth.parser.xml - Used to parse xml files to json
2. se.inera.oppendata.mirth.parser.quality_indicators - Used to create json objects from responses retrieved from the QualityIndicatorsSoapClient (see soap-client project).

## Installation
1. Install Java.
2. Install Maven.
3. Package the library using Maven from the root folder of this repository: mvn clean package
4. Grap the resulting jar (with dependencies) and copy it to the mirth subfolder custom-lib, eg /opt/mirth-connect/custom-lib: cp target/parser-1.0-jar-with-dependencies <INSERT_custom-lib_PATH_HERE>

## Usage within mirth
1. Connect and login to mirth connect using the administrator client.
2. Update resources (no need to restart mirth), click: SETTINGS - RESOURCES - RELOAD RESOURCE
3. Insert a javascript component in a channel (no need to add dependencies)


## 1. xml

## 1.1 Example code
var xmlByteArray = FileUtil.decode(xmlAsBase64String);
    xmlParser = new Packages.se.inera.oppendata.mirth.parser.xml.XMLParser();
    replacementRootName = "records";                                          //replaces the root xml tag with "records"
xmlParser.addParentTagNameToInterpretAsArrayRoot(replacementRootName);        //All children to "records" will be put in a json array on the format:   {records: [child1, child2] } 
xmlParser.addTagNameThatShouldAlwaysBePartOfArray("xml_tag_1");         //Xml elements with "xml_tag_1" tag will be put in an array, even if single. 
xmlParser.addTagNameThatShouldAlwaysBeInterpretedAsString("xml_tag_2"); //The value of an xml element with "xml_tag_2" will always be formatted as a String. 
var json = xmlParser.parseToJson(xmlByteArray, replacementRootName);   

## 1.2 Reference
* XMLParser - A parser used to transform xml to json
  
  * XMLParser()

  * void addParentTagNameToInterpretAsArrayRoot(String tagName) - Specifies an xml tag that should be used as the name of a json array. The values of the children of this xml element 
                                                                  will be elements in the json array.

      * Example: xmlParser.addParentTagNameToInterpretAsArrayRoot("records");

        * XML                             * JSON        
        <records>                         { records: ["abc1234", "def5678"] }
          <element>abc1234</element>
          <element>def5678</element>    
        </records>

  * void addTagNameThatShouldAlwaysBePartOfArray(String tagName) - Specifies an xml tag that should always form a json array, even if only one such xml tag is found.

      * Example: xmlParser.addParentTagNameToInterpretAsArrayRoot("element");
        
        * XML                             * JSON
        <element>abc1234</element>        { element : ["abc1234"] }  

  * void addTagNameThatShouldAlwaysBeInterpretedAsString(String tagName) - Specifies an xml tag whose value should always be interpreted as a String, even if it could be formatted as 
                                                                          an integer, boolean etc. 
      * Example: xmlParser.addTagNameThatShouldAlwaysBeInterpretedAsString("id");
       
       * XML                              * JSON
       <element>                          { element: { id: "1234"} }  
          <id>1234</id>
       </element>                                                                      

  * void setInterpretAllValuesAsStrings(boolean interpretAllValuesAsStrings) - Forces the values of all xml elements (that has no xml children) to be interpreted as strings.
  * String parseToJson(byte[] xml, String newRootElementName) - Parses a byte array to a json string. Replaces the root xml tag with the newRootElementName. If newRootElementName is 
                                                                null or an empty string, it will be ignored.


* XMLList - Utility class used by the XMLParser to store xmlElements and tagNames.
* DocumentBuilderException - Exception used by the XMLParser.

## 2. quality_indicators

## 2.1 Custom external libraries
1. soap-client. Directory: /libs. - The soap-client itself is not used by the QualityIndicatorsParser, but the quality indicators specific classes are.

## 2.2 Example code
var logicalAddress = "SE2321000131-S000000015190";
    reportId = "123456789";
    measureId = "99010";
    customResponse = soapClient.sendMessage(logicalAddress);
    reportDetails = customResponse.getReportDetails();
    reportJson = new Packages.se.inera.oppendata.mirth.parser.quality_indicators.AggregatedQualityReportJsonObject(reportId, reportDetails, measureId);
    measurements = customResponse.getMeasurements();

logger.debug(reportJson.toJSONString());

for(var j = 0; j < measurements.size(); j++){
  var measurement = measurements.get(j);
  var measurementJson = new Packages.se.inera.oppendata.mirth.parser.quality_indicators.MeasurementJsonObject(measurement, reportId);
  logger.debug(measurementJson.toJSONString());
  var performingOrganizationJson = new Packages.se.inera.oppendata.mirth.parser.quality_indicators.PerformingOrganizationJsonObject(measurement);
  logger.debug(performingOrganizationJson.toJSONString());
}

## 2.3 Reference

* AggregatedQualityReportJsonObject - A json object created from a reportId, reportDetails and a measureId. Extends the org.json.simple.JSONObject class.
  
  * AggregatedQualityReportJsonObject() - Creates an empty json object
  * AggregatedQualityReportJsonObject(String aggregatedQualityReportId, CustomAggregatedQualityReportDetails reportDetails, String measureId) - Creates a json object from the 
                                                                                                                                                  provided parameters by calling the add methods below. 

  * void addReportDetails(CustomAggregatedQualityReportDetails customAggregatedQualityReportDetails)
  * void addMeasureId(String measureId)
  * void addSynchronizationTimeFromCurrentTime() - Adds the "synchronizationTime" attribute from the current time, on the format YYYY-MM-DD.
  * void addAggregatedQualityReportId(String reportId)
  * String getAggregatedQualityReportId()
  * String getMeasureId()
  * String getSynchronizationTime()
  * String getMeasurementChecksum()
  * String getReportingPeriodStart()
  * String getReportingPeriodEnd()
  * String getReportingOrganizationHsaId()
  * String getReportingSystemHsaId()

* MeasurementJsonObject - A json object created from a measurementType and an aggregatedQualityReportId. Extends the org.json.simple.JSONObject class.
  
  * static void setSystem(String system) - Sets the static system variable. By default, it has the value of "2.16.840.1.114222.4.11.875".
  * static String getSystem()
  * static void resetSystemToDefault()

  * MeasurementJsonObject() - Creates an empty json object
  * MeasurementJsonObject(MeasurementType measurementType, String aggregatedQualityReportId) - Creates a json object from the provided parameters by calling the 
                                                                                addRandomMeasurementId, addAggregatedQualityReportId and addMeasurementType methods.

  * void addRandomMeasurementId() - Creates a random UUID string as the measurementId.
  * void addAggregatedQualityReportId(String aggregatedQualityReportId)
  * void addMeasurementType(MeasurementType measurementType)
  * void addParentMeasureId(String parentMeasureId)

* PerformingOrganizationJsonObject - A json object created from a performingOrganizationType. Extends the org.json.simple.JSONObject class.
  
  * static void setSystem(String system) - Sets the static system variable. By default, it has the value of "5447717431505822d0f4f4e8".
  * static String getSystem()
  * static void resetSystemToDefault()

  * PerformingOrganizationJsonObject() - Creates an empty json object
  * PerformingOrganizationJsonObject(PerformingOrganizationType performingOrganizationType) - Creates a json object from the performingOrganizationType parameter.
  * PerformingOrganizationJsonObject(MeasurementType measurementType) - Creates a json object by extracting the performingOrganizationType object from the measurementType object.

  * This class has no public methods

* AggregatedQualityReportJsonAttributes - Contains the json attributes used by AggregatedQualityReportJsonObject
* MeasurementJsonAttributes - Contains the json attributes used by MeasurementJsonObject
* PerformingOrganizationJsonAttributes - Contains the json attributes used by PerformingOrganizationJsonObject
* SharedJsonAttributes - Contains json attributes that may be used by several classes
