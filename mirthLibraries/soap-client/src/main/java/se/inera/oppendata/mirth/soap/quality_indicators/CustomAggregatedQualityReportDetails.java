package se.inera.oppendata.mirth.soap.quality_indicators;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * Created by davsan on 2017-10-23.
 */
public class CustomAggregatedQualityReportDetails {
    private DatePeriodType reportingPeriod;
    private ReportingSystemType reportingSystem;
    private ReportingOrganizationType reportingOrganization;
    private String measurementChecksum;
    private static final String JANUARY_FIRST = "0101";
    private static final String DECEMBER_LAST = "1231";

    public CustomAggregatedQualityReportDetails(DatePeriodType reportingPeriod, ReportingSystemType reportingSystem, ReportingOrganizationType reportingOrganization, String measurementChecksum){
        this.reportingPeriod = reportingPeriod;
        this.reportingSystem = reportingSystem;
        this.reportingOrganization = reportingOrganization;
        setMeasurementChecksum(measurementChecksum);
    }

    public CustomAggregatedQualityReportDetails(String year, ReportingSystemType reportingSystem, ReportingOrganizationType reportingOrganization, String measurementChecksum){
        reportingPeriod = new DatePeriodType(year+JANUARY_FIRST, year+DECEMBER_LAST);
        this.reportingSystem = reportingSystem;
        this.reportingOrganization = reportingOrganization;
        setMeasurementChecksum(measurementChecksum);
    }

    public void setMeasurementChecksum(String measurementChecksum){
        this.measurementChecksum = measurementChecksum;
    }

    public String getReportingPeriodStart(){
        return reportingPeriod.getStart();
    }

    public String getReportingPeriodEnd(){
        return reportingPeriod.getEnd();
    }

    public String getReportingPeriodStart(DateTimeFormatter dateTimeFormatter){
        return reportingPeriod.getStart(dateTimeFormatter);
    }

    public String getReportingPeriodEnd(DateTimeFormatter dateTimeFormatter){
        return reportingPeriod.getEnd(dateTimeFormatter);
    }

    public String getReportingSystemHsaId(){
        return reportingSystem.getHsaId();
    }

    public String getReportingOrganizationHsaId(){
        return reportingOrganization.getHsaId();
    }

    public String getMeasurementChecksum(){
        return measurementChecksum;
    }

    public String getReportingOrganizationName(){
        return reportingOrganization.getName();
    }

}
