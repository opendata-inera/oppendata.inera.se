package se.inera.oppendata.mirth.soap.quality_indicators;

/**
 * Created by davsan on 2017-09-21.
 */
public class FaultCodes {
    public static final String INVALID_LOGICAL_ADDRESS ="VP004";
    public static final String AUTHORIZATION_MISSING = "VP007";
}
