
package se.inera.oppendata.mirth.soap.quality_indicators;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the se.inera.oppendata.mirth.soap package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GetQualityIndicatorsResponse_QNAME = new QName("urn:riv:followup:groupoutcomes:qualityreporting:GetQualityIndicatorsResponder:2", "GetQualityIndicatorsResponse");
    private final static QName _ServiceContract_QNAME = new QName("urn:riv:itintegration:registry:1", "ServiceContract");
    private final static QName _GetQualityIndicators_QNAME = new QName("urn:riv:followup:groupoutcomes:qualityreporting:GetQualityIndicatorsResponder:2", "GetQualityIndicators");
    private final static QName _LogicalAddress_QNAME = new QName("urn:riv:itintegration:registry:1", "LogicalAddress");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: se.inera.oppendata.mirth.soap
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetQualityIndicatorsResponseType }
     * 
     */
    public GetQualityIndicatorsResponseType createGetQualityIndicatorsResponseType() {
        return new GetQualityIndicatorsResponseType();
    }

    /**
     * Create an instance of {@link GetQualityIndicatorsType }
     * 
     */
    public GetQualityIndicatorsType createGetQualityIndicatorsType() {
        return new GetQualityIndicatorsType();
    }

    /**
     * Create an instance of {@link AggregatedQualityReportType }
     * 
     */
    public AggregatedQualityReportType createAggregatedQualityReportType() {
        return new AggregatedQualityReportType();
    }

    /**
     * Create an instance of {@link ConfidenceInterval95PercentType }
     * 
     */
    public ConfidenceInterval95PercentType createConfidenceInterval95PercentType() {
        return new ConfidenceInterval95PercentType();
    }

    /**
     * Create an instance of {@link MissingMeasureType }
     * 
     */
    public MissingMeasureType createMissingMeasureType() {
        return new MissingMeasureType();
    }

    /**
     * Create an instance of {@link ContinuousVariableMeasureType }
     * 
     */
    public ContinuousVariableMeasureType createContinuousVariableMeasureType() {
        return new ContinuousVariableMeasureType();
    }

    /**
     * Create an instance of {@link IIType }
     * 
     */
    public IIType createIIType() {
        return new IIType();
    }

    /**
     * Create an instance of {@link CohortMeasureType }
     * 
     */
    public CohortMeasureType createCohortMeasureType() {
        return new CohortMeasureType();
    }

    /**
     * Create an instance of {@link DatePeriodType }
     * 
     */
    public DatePeriodType createDatePeriodType() {
        return new DatePeriodType();
    }

    /**
     * Create an instance of {@link SourceSystemType }
     * 
     */
    public SourceSystemType createSourceSystemType() {
        return new SourceSystemType();
    }

    /**
     * Create an instance of {@link ReportingSystemType }
     * 
     */
    public ReportingSystemType createReportingSystemType() {
        return new ReportingSystemType();
    }

    /**
     * Create an instance of {@link MeasurementType }
     * 
     */
    public MeasurementType createMeasurementType() {
        return new MeasurementType();
    }

    /**
     * Create an instance of {@link PerformingOrganizationType }
     * 
     */
    public PerformingOrganizationType createPerformingOrganizationType() {
        return new PerformingOrganizationType();
    }

    /**
     * Create an instance of {@link ReportingOrganizationType }
     * 
     */
    public ReportingOrganizationType createReportingOrganizationType() {
        return new ReportingOrganizationType();
    }

    /**
     * Create an instance of {@link ProportionMeasureType }
     * 
     */
    public ProportionMeasureType createProportionMeasureType() {
        return new ProportionMeasureType();
    }

    /**
     * Create an instance of {@link ServiceContractType }
     * 
     */
    public ServiceContractType createServiceContractType() {
        return new ServiceContractType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetQualityIndicatorsResponseType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:riv:followup:groupoutcomes:qualityreporting:GetQualityIndicatorsResponder:2", name = "GetQualityIndicatorsResponse")
    public JAXBElement<GetQualityIndicatorsResponseType> createGetQualityIndicatorsResponse(GetQualityIndicatorsResponseType value) {
        return new JAXBElement<GetQualityIndicatorsResponseType>(_GetQualityIndicatorsResponse_QNAME, GetQualityIndicatorsResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ServiceContractType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:riv:itintegration:registry:1", name = "ServiceContract")
    public JAXBElement<ServiceContractType> createServiceContract(ServiceContractType value) {
        return new JAXBElement<ServiceContractType>(_ServiceContract_QNAME, ServiceContractType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetQualityIndicatorsType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:riv:followup:groupoutcomes:qualityreporting:GetQualityIndicatorsResponder:2", name = "GetQualityIndicators")
    public JAXBElement<GetQualityIndicatorsType> createGetQualityIndicators(GetQualityIndicatorsType value) {
        return new JAXBElement<GetQualityIndicatorsType>(_GetQualityIndicators_QNAME, GetQualityIndicatorsType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:riv:itintegration:registry:1", name = "LogicalAddress")
    public JAXBElement<String> createLogicalAddress(String value) {
        return new JAXBElement<String>(_LogicalAddress_QNAME, String.class, null, value);
    }

}
