package se.inera.oppendata.mirth.soap.quality_indicators;

/**
 * Created by davsan on 2017-11-14.
 *
 * These variables are usually found in the JAXWSProperties class. However, Maven compilation fails when using this class, perhaps
 * due to JAXWSProperties being deprecated.
 *
 */
public class CustomJAXWSProperties {
    public static final String SSL_SOCKET_FACTORY = "com.sun.xml.internal.ws.transport.https.client.SSLSocketFactory";
    public static final String CONNECT_TIMEOUT = "com.sun.xml.internal.ws.connect.timeout";
    public static final String REQUEST_TIMEOUT = "com.sun.xml.internal.ws.request.timeout";
}
