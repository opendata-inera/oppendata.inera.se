
package se.inera.oppendata.mirth.soap.quality_indicators;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.w3c.dom.Element;


/**
 * En IndikatorRapport skapas av ett rapporterande system och är huvudklassen vid överföring av indikatorer. För vajre indikatorrapport kommer ett eller flera indikatorVärden.
 * 
 * <p>Java class for AggregatedQualityReportType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AggregatedQualityReportType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="measurementChecksum" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="reportingPeriod" type="{urn:riv:followup:groupoutcomes:qualityreporting:2}DatePeriodType"/>
 *         &lt;element name="reportingSystem" type="{urn:riv:followup:groupoutcomes:qualityreporting:2}ReportingSystemType"/>
 *         &lt;element name="reportingOrganization" type="{urn:riv:followup:groupoutcomes:qualityreporting:2}ReportingOrganizationType"/>
 *         &lt;element name="measurement" type="{urn:riv:followup:groupoutcomes:qualityreporting:2}MeasurementType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;any processContents='lax' namespace='##other' maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AggregatedQualityReportType", propOrder = {
    "measurementChecksum",
    "reportingPeriod",
    "reportingSystem",
    "reportingOrganization",
    "measurement",
    "any"
})
public class AggregatedQualityReportType {

    @XmlElement(required = true)
    protected String measurementChecksum;
    @XmlElement(required = true)
    protected DatePeriodType reportingPeriod;
    @XmlElement(required = true)
    protected ReportingSystemType reportingSystem;
    @XmlElement(required = true)
    protected ReportingOrganizationType reportingOrganization;
    protected List<MeasurementType> measurement;
    @XmlAnyElement(lax = true)
    protected List<Object> any;

    /**
     * Gets the value of the measurementChecksum property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMeasurementChecksum() {
        return measurementChecksum;
    }

    /**
     * Sets the value of the measurementChecksum property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMeasurementChecksum(String value) {
        this.measurementChecksum = value;
    }

    /**
     * Gets the value of the reportingPeriod property.
     * 
     * @return
     *     possible object is
     *     {@link DatePeriodType }
     *     
     */
    public DatePeriodType getReportingPeriod() {
        return reportingPeriod;
    }

    /**
     * Sets the value of the reportingPeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link DatePeriodType }
     *     
     */
    public void setReportingPeriod(DatePeriodType value) {
        this.reportingPeriod = value;
    }

    /**
     * Gets the value of the reportingSystem property.
     * 
     * @return
     *     possible object is
     *     {@link ReportingSystemType }
     *     
     */
    public ReportingSystemType getReportingSystem() {
        return reportingSystem;
    }

    /**
     * Sets the value of the reportingSystem property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReportingSystemType }
     *     
     */
    public void setReportingSystem(ReportingSystemType value) {
        this.reportingSystem = value;
    }

    /**
     * Gets the value of the reportingOrganization property.
     * 
     * @return
     *     possible object is
     *     {@link ReportingOrganizationType }
     *     
     */
    public ReportingOrganizationType getReportingOrganization() {
        return reportingOrganization;
    }

    /**
     * Sets the value of the reportingOrganization property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReportingOrganizationType }
     *     
     */
    public void setReportingOrganization(ReportingOrganizationType value) {
        this.reportingOrganization = value;
    }

    /**
     * Gets the value of the measurement property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the measurement property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMeasurement().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MeasurementType }
     * 
     * 
     */
    public List<MeasurementType> getMeasurement() {
        if (measurement == null) {
            measurement = new ArrayList<MeasurementType>();
        }
        return this.measurement;
    }

    /**
     * Gets the value of the any property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the any property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAny().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Element }
     * {@link Object }
     * 
     * 
     */
    public List<Object> getAny() {
        if (any == null) {
            any = new ArrayList<Object>();
        }
        return this.any;
    }

}
