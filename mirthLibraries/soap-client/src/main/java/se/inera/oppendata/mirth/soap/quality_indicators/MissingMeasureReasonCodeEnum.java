
package se.inera.oppendata.mirth.soap.quality_indicators;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MissingMeasureReasonCodeEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="MissingMeasureReasonCodeEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="MSK"/>
 *     &lt;enumeration value="UNK"/>
 *     &lt;enumeration value="INV"/>
 *     &lt;enumeration value="NI"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "MissingMeasureReasonCodeEnum")
@XmlEnum
public enum MissingMeasureReasonCodeEnum {

    MSK,
    UNK,
    INV,
    NI;

    public String value() {
        return name();
    }

    public static MissingMeasureReasonCodeEnum fromValue(String v) {
        return valueOf(v);
    }

}
