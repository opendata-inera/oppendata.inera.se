
package se.inera.oppendata.mirth.soap.quality_indicators;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.9-b130926.1035
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "GetQualityIndicatorsResponderService", targetNamespace = "urn:riv:followup:groupoutcomes:qualityreporting:GetQualityIndicators:2:rivtabp21", wsdlLocation = "https://qa.esb.ntjp.se/vp/followup/groupoutcomes/qualityreporting/GetQualityIndicators/2/rivtabp21?wsdl")
public class GetQualityIndicatorsResponderService
    extends Service
{

    private final static URL GETQUALITYINDICATORSRESPONDERSERVICE_WSDL_LOCATION;
    private final static WebServiceException GETQUALITYINDICATORSRESPONDERSERVICE_EXCEPTION;
    private final static QName GETQUALITYINDICATORSRESPONDERSERVICE_QNAME = new QName("urn:riv:followup:groupoutcomes:qualityreporting:GetQualityIndicators:2:rivtabp21", "GetQualityIndicatorsResponderService");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("https://qa.esb.ntjp.se/vp/followup/groupoutcomes/qualityreporting/GetQualityIndicators/2/rivtabp21?wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        GETQUALITYINDICATORSRESPONDERSERVICE_WSDL_LOCATION = url;
        GETQUALITYINDICATORSRESPONDERSERVICE_EXCEPTION = e;
    }


    public GetQualityIndicatorsResponderService() {
        super(__getWsdlLocation(), GETQUALITYINDICATORSRESPONDERSERVICE_QNAME);
    }

    public GetQualityIndicatorsResponderService(WebServiceFeature... features) {
        super(__getWsdlLocation(), GETQUALITYINDICATORSRESPONDERSERVICE_QNAME, features);
    }

    public GetQualityIndicatorsResponderService(URL wsdlLocation) {
        super(wsdlLocation, GETQUALITYINDICATORSRESPONDERSERVICE_QNAME);
    }

    public GetQualityIndicatorsResponderService(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, GETQUALITYINDICATORSRESPONDERSERVICE_QNAME, features);
    }

    public GetQualityIndicatorsResponderService(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public GetQualityIndicatorsResponderService(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns GetQualityIndicatorsResponderInterface
     */
    @WebEndpoint(name = "GetQualityIndicatorsResponderPort")
    public GetQualityIndicatorsResponderInterface getGetQualityIndicatorsResponderPort() {
        return super.getPort(new QName("urn:riv:followup:groupoutcomes:qualityreporting:GetQualityIndicators:2:rivtabp21", "GetQualityIndicatorsResponderPort"), GetQualityIndicatorsResponderInterface.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns GetQualityIndicatorsResponderInterface
     */
    @WebEndpoint(name = "GetQualityIndicatorsResponderPort")
    public GetQualityIndicatorsResponderInterface getGetQualityIndicatorsResponderPort(WebServiceFeature... features) {
        return super.getPort(new QName("urn:riv:followup:groupoutcomes:qualityreporting:GetQualityIndicators:2:rivtabp21", "GetQualityIndicatorsResponderPort"), GetQualityIndicatorsResponderInterface.class, features);
    }

    private static URL __getWsdlLocation() {
        if (GETQUALITYINDICATORSRESPONDERSERVICE_EXCEPTION!= null) {
            throw GETQUALITYINDICATORSRESPONDERSERVICE_EXCEPTION;
        }
        return GETQUALITYINDICATORSRESPONDERSERVICE_WSDL_LOCATION;
    }

}
