
package se.inera.oppendata.mirth.soap.quality_indicators;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.w3c.dom.Element;


/**
 * De indikatorvärden som mäts som kvoter har sitt värde i klassen ProportionMeasure. Denna klass innehåller täljare, nämnare och kvoten däremellan.
 * 
 * <p>Java class for ProportionMeasureType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ProportionMeasureType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="rate" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="denominator" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="numerator" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="confidenceInterval95percent" type="{urn:riv:followup:groupoutcomes:qualityreporting:2}ConfidenceInterval95percentType" minOccurs="0"/>
 *         &lt;element name="standardDeviation" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="coverage" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="exclusions" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;element name="referenceIntervalRate" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/>
 *         &lt;any processContents='lax' namespace='##other' maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ProportionMeasureType", propOrder = {
    "rate",
    "denominator",
    "numerator",
    "confidenceInterval95Percent",
    "standardDeviation",
    "coverage",
    "exclusions",
    "referenceIntervalRate",
    "any"
})
public class ProportionMeasureType {

    protected double rate;
    protected double denominator;
    protected double numerator;
    @XmlElement(name = "confidenceInterval95percent")
    protected ConfidenceInterval95PercentType confidenceInterval95Percent;
    protected Double standardDeviation;
    protected Double coverage;
    protected Double exclusions;
    protected Double referenceIntervalRate;
    @XmlAnyElement(lax = true)
    protected List<Object> any;

    /**
     * Gets the value of the rate property.
     * 
     */
    public double getRate() {
        return rate;
    }

    /**
     * Sets the value of the rate property.
     * 
     */
    public void setRate(double value) {
        this.rate = value;
    }

    /**
     * Gets the value of the denominator property.
     * 
     */
    public double getDenominator() {
        return denominator;
    }

    /**
     * Sets the value of the denominator property.
     * 
     */
    public void setDenominator(double value) {
        this.denominator = value;
    }

    /**
     * Gets the value of the numerator property.
     * 
     */
    public double getNumerator() {
        return numerator;
    }

    /**
     * Sets the value of the numerator property.
     * 
     */
    public void setNumerator(double value) {
        this.numerator = value;
    }

    /**
     * Gets the value of the confidenceInterval95Percent property.
     * 
     * @return
     *     possible object is
     *     {@link ConfidenceInterval95PercentType }
     *     
     */
    public ConfidenceInterval95PercentType getConfidenceInterval95Percent() {
        return confidenceInterval95Percent;
    }

    /**
     * Sets the value of the confidenceInterval95Percent property.
     * 
     * @param value
     *     allowed object is
     *     {@link ConfidenceInterval95PercentType }
     *     
     */
    public void setConfidenceInterval95Percent(ConfidenceInterval95PercentType value) {
        this.confidenceInterval95Percent = value;
    }

    /**
     * Gets the value of the standardDeviation property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getStandardDeviation() {
        return standardDeviation;
    }

    /**
     * Sets the value of the standardDeviation property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setStandardDeviation(Double value) {
        this.standardDeviation = value;
    }

    /**
     * Gets the value of the coverage property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getCoverage() {
        return coverage;
    }

    /**
     * Sets the value of the coverage property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setCoverage(Double value) {
        this.coverage = value;
    }

    /**
     * Gets the value of the exclusions property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getExclusions() {
        return exclusions;
    }

    /**
     * Sets the value of the exclusions property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setExclusions(Double value) {
        this.exclusions = value;
    }

    /**
     * Gets the value of the referenceIntervalRate property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getReferenceIntervalRate() {
        return referenceIntervalRate;
    }

    /**
     * Sets the value of the referenceIntervalRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setReferenceIntervalRate(Double value) {
        this.referenceIntervalRate = value;
    }

    /**
     * Gets the value of the any property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the any property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAny().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Element }
     * {@link Object }
     * 
     * 
     */
    public List<Object> getAny() {
        if (any == null) {
            any = new ArrayList<Object>();
        }
        return this.any;
    }

}
