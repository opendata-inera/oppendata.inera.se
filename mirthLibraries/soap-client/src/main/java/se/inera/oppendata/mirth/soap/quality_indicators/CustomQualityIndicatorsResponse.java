package se.inera.oppendata.mirth.soap.quality_indicators;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by davsan on 2017-09-12.
 */
public class CustomQualityIndicatorsResponse {
    private List<MeasurementType> measurements;
    private CustomAggregatedQualityReportDetails reportDetails;

    public CustomQualityIndicatorsResponse(){}

    public CustomQualityIndicatorsResponse(GetQualityIndicatorsResponseType getQualityIndicatorsResponseType) {
        if(getQualityIndicatorsResponseType != null && getQualityIndicatorsResponseType.getAggregatedQualityReport() != null){
            setupPrivateVariables(getQualityIndicatorsResponseType.getAggregatedQualityReport());
        }
    }

    public void setup(GetQualityIndicatorsResponseType getQualityIndicatorsResponseType){
        if(getQualityIndicatorsResponseType != null && getQualityIndicatorsResponseType.getAggregatedQualityReport() != null){
            setupPrivateVariables(getQualityIndicatorsResponseType.getAggregatedQualityReport());
        }
    }

    private void setupPrivateVariables(AggregatedQualityReportType aggregatedQualityReportType){
        if(aggregatedQualityReportType != null){
            reportDetails = new CustomAggregatedQualityReportDetails(aggregatedQualityReportType.getReportingPeriod(),
                                                                        aggregatedQualityReportType.getReportingSystem(),
                                                                        aggregatedQualityReportType.getReportingOrganization(),
                                                                        aggregatedQualityReportType.getMeasurementChecksum());
            measurements = aggregatedQualityReportType.getMeasurement();
        }
    }

    public CustomAggregatedQualityReportDetails getReportDetails(){
        return reportDetails;
    }

    public List<MeasurementType> getMeasurements(){
        if(measurements == null){
            return new ArrayList<MeasurementType>();
        }
        return measurements;
    }

}
