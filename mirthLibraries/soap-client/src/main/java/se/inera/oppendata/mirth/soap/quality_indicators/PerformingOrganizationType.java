
package se.inera.oppendata.mirth.soap.quality_indicators;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import org.w3c.dom.Element;


/**
 * En resultatenhet är den enhet för vilken ett indikatorVärde uppmätts. En resultatEnhet ingår i en hierarkisk struktur och kan knytas till en högre nivå i hierarkin genom att referera till en enhet på högre nivå. Ett par nivåer har specifika koder, OrganisationsTypKod, som anger Riket - Används vid aggregering på riksnivå samt fungerar som snittvärde för riket vid rapportering på en lägre nivå Region - landsting eller region samt fungerar som snittvärde för regionen vid rapportering på en lägre nivå Kommun - Används ej Sjukhus - identifieras genom sjukhuskod eller HSAId Vårdenhet - den lägsta rapporterande enheten om typ av verksamhet är okänd. Primärvårdsenhet - typ av verksamhet och en underkategori till vårdenhet Medicinklinik - typ av verksamhet och en underkategori till vårdenhet
 * 
 * <p>Java class for PerformingOrganizationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PerformingOrganizationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="hsaId" type="{urn:riv:followup:groupoutcomes:qualityreporting:2}HSAIdType" minOccurs="0"/>
 *         &lt;element name="organizationName" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="organizationType" type="{urn:riv:followup:groupoutcomes:qualityreporting:2}OrganizationTypeCodeEnum"/>
 *         &lt;element name="organizationId" type="{urn:riv:followup:groupoutcomes:qualityreporting:2}IIType" minOccurs="0"/>
 *         &lt;element name="asOrganizationPartOf" type="{urn:riv:followup:groupoutcomes:qualityreporting:2}PerformingOrganizationType" minOccurs="0"/>
 *         &lt;any processContents='lax' namespace='##other' maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PerformingOrganizationType", propOrder = {
    "hsaId",
    "organizationName",
    "organizationType",
    "organizationId",
    "asOrganizationPartOf",
    "any"
})
public class PerformingOrganizationType {

    protected String hsaId;
    @XmlElement(required = true)
    protected String organizationName;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected OrganizationTypeCodeEnum organizationType;
    protected IIType organizationId;
    protected PerformingOrganizationType asOrganizationPartOf;
    @XmlAnyElement(lax = true)
    protected List<Object> any;

    /**
     * Gets the value of the hsaId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHsaId() {
        return hsaId;
    }

    /**
     * Sets the value of the hsaId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHsaId(String value) {
        this.hsaId = value;
    }

    /**
     * Gets the value of the organizationName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrganizationName() {
        return organizationName;
    }

    /**
     * Sets the value of the organizationName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrganizationName(String value) {
        this.organizationName = value;
    }

    /**
     * Gets the value of the organizationType property.
     * 
     * @return
     *     possible object is
     *     {@link OrganizationTypeCodeEnum }
     *     
     */
    public OrganizationTypeCodeEnum getOrganizationType() {
        return organizationType;
    }

    /**
     * Sets the value of the organizationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link OrganizationTypeCodeEnum }
     *     
     */
    public void setOrganizationType(OrganizationTypeCodeEnum value) {
        this.organizationType = value;
    }

    /**
     * Gets the value of the organizationId property.
     * 
     * @return
     *     possible object is
     *     {@link IIType }
     *     
     */
    public IIType getOrganizationId() {
        return organizationId;
    }

    /**
     * Sets the value of the organizationId property.
     * 
     * @param value
     *     allowed object is
     *     {@link IIType }
     *     
     */
    public void setOrganizationId(IIType value) {
        this.organizationId = value;
    }

    /**
     * Gets the value of the asOrganizationPartOf property.
     * 
     * @return
     *     possible object is
     *     {@link PerformingOrganizationType }
     *     
     */
    public PerformingOrganizationType getAsOrganizationPartOf() {
        return asOrganizationPartOf;
    }

    /**
     * Sets the value of the asOrganizationPartOf property.
     * 
     * @param value
     *     allowed object is
     *     {@link PerformingOrganizationType }
     *     
     */
    public void setAsOrganizationPartOf(PerformingOrganizationType value) {
        this.asOrganizationPartOf = value;
    }

    /**
     * Gets the value of the any property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the any property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAny().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Element }
     * {@link Object }
     * 
     * 
     */
    public List<Object> getAny() {
        if (any == null) {
            any = new ArrayList<Object>();
        }
        return this.any;
    }

}
