package se.inera.oppendata.mirth.soap.quality_indicators;

import javax.net.ssl.SSLContext;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.WebServiceException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;
import java.security.InvalidParameterException;
import java.util.Map;


/**
 * Created by davsan on 2017-08-31.
 */
public class QualityIndicatorsSoapClient {
    private GetQualityIndicatorsResponderInterface port;
    private GetQualityIndicatorsType params;
    private SSLContext sslContext;

    public QualityIndicatorsSoapClient(SSLContext sslContext, String wsdlFilePath) throws Exception {
        if(wsdlFilePath == null){
            throw new InvalidParameterException("Parameter 'wsdlFilePath' was null.");
        }
        params = new GetQualityIndicatorsType();
        this.sslContext = sslContext;
        initPort(wsdlFilePath);
        addSocketFactoryToPort();
    }

    private void initPort(String wsdlFilePath) throws MalformedURLException, WebServiceException {
        URL wsdlUrl = Paths.get(wsdlFilePath).toUri().toURL();
        GetQualityIndicatorsResponderService webService = new GetQualityIndicatorsResponderService(wsdlUrl);
        port = webService.getGetQualityIndicatorsResponderPort();
    }

    private void addSocketFactoryToPort(){
        if(sslContext != null){
            BindingProvider bindingProvider = (BindingProvider) port;
            bindingProvider.getRequestContext().put(CustomJAXWSProperties.SSL_SOCKET_FACTORY, sslContext.getSocketFactory());
        }
    }

    public void setTimeouts(int requestTimeoutInMillis, int connectTimeoutInMillis){
        BindingProvider bindingProvider = (BindingProvider)port;
        Map<String, Object> requestContext = bindingProvider.getRequestContext();
        if(requestContext != null){
            requestContext.put(CustomJAXWSProperties.REQUEST_TIMEOUT, requestTimeoutInMillis);
            requestContext.put(CustomJAXWSProperties.CONNECT_TIMEOUT, connectTimeoutInMillis);
        }
    }

    public void setReportingPeriod(String from, String to){
        DatePeriodType dpt = new DatePeriodType(from, to);
        params.setReportingPeriod(dpt);
    }

    public void setIncludeMeasurements(Boolean includeMeasurements){
        params.setIncludeMeasurements(includeMeasurements);
    }

    public void addMeasureId(String root, String extension){
        IIType measureId = new IIType(root, extension);
        params.getMeasureId().add(measureId);
    }

    public String getParamsAsString(){
        StringBuilder sb = new StringBuilder();
        String lineSeparator = System.getProperty("line.separator");
        for(int i = 0; i < params.getMeasureId().size(); i++){
            sb.append("MEASURE: ["+i+"]. Root: "+params.getMeasureId().get(i).getRoot() +". Extension: "+params.getMeasureId().get(i).getExtension()+", ");
        }
        if(params.getReportingPeriod() == null){
            sb.append("REPORTING PERIOD: Null, ");
        }else{
            sb.append("REPORTING PERIOD: Start: "+params.getReportingPeriod().getStart()+". End: "+params.getReportingPeriod().getEnd()+", ");
        }

        sb.append("INCLUDE MEASUREMENTS: "+ params.isIncludeMeasurements()+".");
        return sb.toString();
    }

    public void clearMeasureIds(){
        params.getMeasureId().clear();
    }

    public CustomQualityIndicatorsResponse sendMessage(String logicalAddress) throws Exception{
        CustomQualityIndicatorsResponse response = new CustomQualityIndicatorsResponse();
        GetQualityIndicatorsResponseType getQualityIndicatorsResponseType = port.getQualityIndicators(logicalAddress, params);
        response.setup(getQualityIndicatorsResponseType);
        return response;
    }
}
