
package se.inera.oppendata.mirth.soap.quality_indicators;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.w3c.dom.Element;


/**
 * Avser starttid och sluttid för rapporterade värden. Start specas till år, månad, dag End Specas till år, månad, dag
 * 
 * <p>Java class for DatePeriodType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="DatePeriodType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="start" type="{urn:riv:followup:groupoutcomes:qualityreporting:2}DateType"/>
 *         &lt;element name="end" type="{urn:riv:followup:groupoutcomes:qualityreporting:2}DateType"/>
 *         &lt;any processContents='lax' namespace='##other' maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DatePeriodType", propOrder = {
    "start",
    "end",
    "any"
})
public class DatePeriodType {

    @XmlElement(required = true)
    protected String start;
    @XmlElement(required = true)
    protected String end;
    @XmlAnyElement(lax = true)
    protected List<Object> any;

    public DatePeriodType(){}

    public DatePeriodType(String start, String end){
        this.start = start;
        this.end = end;
    }
    /**
     * Gets the value of the start property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStart() {
        return start;
    }

    /**
     * Gets the value of the start property in a custom format.
     * @param dateTimeFormatter
     * @return
     */
    public String getStart(DateTimeFormatter dateTimeFormatter){
        if(start == null){
            return null;
        }
        return LocalDate.parse(start, dateTimeFormatter).toString();
    }


    /**
     * Sets the value of the start property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStart(String value) {
        this.start = value;
    }

    /**
     * Gets the value of the end property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnd() {
        return end;
    }

    /**
     * Gets the value of the end property in a custom format.
     * @param dateTimeFormatter
     * @return
     */
    public String getEnd(DateTimeFormatter dateTimeFormatter){
        if(end == null){
            return null;
        }
        return LocalDate.parse(end, dateTimeFormatter).toString();
    }

    /**
     * Sets the value of the end property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnd(String value) {
        this.end = value;
    }


    /**
     * Gets the value of the any property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the any property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAny().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Element }
     * {@link Object }
     * 
     * 
     */
    public List<Object> getAny() {
        if (any == null) {
            any = new ArrayList<Object>();
        }
        return this.any;
    }

}
