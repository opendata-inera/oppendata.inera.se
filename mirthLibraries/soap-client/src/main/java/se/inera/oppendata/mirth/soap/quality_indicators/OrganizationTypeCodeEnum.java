
package se.inera.oppendata.mirth.soap.quality_indicators;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for OrganizationTypeCodeEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="OrganizationTypeCodeEnum">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="Land"/>
 *     &lt;enumeration value="Lan"/>
 *     &lt;enumeration value="Kommun"/>
 *     &lt;enumeration value="Sjukhus"/>
 *     &lt;enumeration value="Vardenhet"/>
 *     &lt;enumeration value="Sjukvardsregion"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "OrganizationTypeCodeEnum")
@XmlEnum
public enum OrganizationTypeCodeEnum {

    @XmlEnumValue("Land")
    LAND("Land"),
    @XmlEnumValue("Lan")
    LAN("Lan"),
    @XmlEnumValue("Kommun")
    KOMMUN("Kommun"),
    @XmlEnumValue("Sjukhus")
    SJUKHUS("Sjukhus"),
    @XmlEnumValue("Vardenhet")
    VARDENHET("Vardenhet"),
    @XmlEnumValue("Sjukvardsregion")
    SJUKVARDSREGION("Sjukvardsregion");
    private final String value;

    OrganizationTypeCodeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static OrganizationTypeCodeEnum fromValue(String v) {
        for (OrganizationTypeCodeEnum c: OrganizationTypeCodeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
