
package se.inera.oppendata.mirth.soap.quality_indicators;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import org.w3c.dom.Element;


/**
 * IndikatorVärde är det rapporterade resultatet för en indikator för en viss resultatEnhet och för en viss mätperiod. En indikatorRapport innehåller flera IndikatorV�rden. F�r att visa vilket underlag som ligger till grund f�r v�rdet �r det m�jligt att ange ett datumintervall f�r vilket m�tv�rden �r ber�knat. Detta intervall kommer att rymmas inom intervallet som anges f�r rapporten. Antingen angivet som 1. en kvot, d� med v�rden angivna som t�ljare och n�mnare 2. som en procentsats d� med m�tv�rde angivet i procent och enhet = % 3. som ett m�tv�rde d� med godtycklig enhet angiven som m�tenhet. Ett resultat kan ha ett 95% konfidensintervall angivet som ett intervall med ett l�gt och ett h�gt v�rde f�r att definiera spannet. Om en resultatEnhet saknar (tillr�ckliga) v�rden f�r en viss m�tperiod anges �nd� ett indikatorv�rde f�r avsedd m�tperiod och resultatEnhet men med NullFlavor="NI" angivet f�r m�tv�rde/kvot.
 * 
 * <p>Java class for MeasurementType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MeasurementType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="measureId" type="{urn:riv:followup:groupoutcomes:qualityreporting:2}IIType"/>
 *         &lt;element name="measureIdVersionNumber" type="{http://www.w3.org/2001/XMLSchema}integer"/>
 *         &lt;element name="measurePeriod" type="{urn:riv:followup:groupoutcomes:qualityreporting:2}DatePeriodType"/>
 *         &lt;element name="firstServiceEncounter" type="{urn:riv:followup:groupoutcomes:qualityreporting:2}DateType" minOccurs="0"/>
 *         &lt;element name="lastServiceEncounter" type="{urn:riv:followup:groupoutcomes:qualityreporting:2}DateType" minOccurs="0"/>
 *         &lt;element name="proportionMeasure" type="{urn:riv:followup:groupoutcomes:qualityreporting:2}ProportionMeasureType" minOccurs="0"/>
 *         &lt;element name="continuousVariableMeasure" type="{urn:riv:followup:groupoutcomes:qualityreporting:2}ContinuousVariableMeasureType" minOccurs="0"/>
 *         &lt;element name="cohortMeasure" type="{urn:riv:followup:groupoutcomes:qualityreporting:2}CohortMeasureType" minOccurs="0"/>
 *         &lt;element name="missingMeasure" type="{urn:riv:followup:groupoutcomes:qualityreporting:2}MissingMeasureType" minOccurs="0"/>
 *         &lt;element name="performingOrganization" type="{urn:riv:followup:groupoutcomes:qualityreporting:2}PerformingOrganizationType"/>
 *         &lt;element name="sourceSystem" type="{urn:riv:followup:groupoutcomes:qualityreporting:2}SourceSystemType"/>
 *         &lt;any processContents='lax' namespace='##other' maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MeasurementType", propOrder = {
    "measureId",
    "measureIdVersionNumber",
    "measurePeriod",
    "firstServiceEncounter",
    "lastServiceEncounter",
    "proportionMeasure",
    "continuousVariableMeasure",
    "cohortMeasure",
    "missingMeasure",
    "performingOrganization",
    "sourceSystem",
    "any"
})
public class MeasurementType {

    @XmlElement(required = true)
    protected IIType measureId;
    @XmlElement(required = true)
    protected BigInteger measureIdVersionNumber;
    @XmlElement(required = true)
    protected DatePeriodType measurePeriod;
    protected String firstServiceEncounter;
    protected String lastServiceEncounter;
    protected ProportionMeasureType proportionMeasure;
    protected ContinuousVariableMeasureType continuousVariableMeasure;
    protected CohortMeasureType cohortMeasure;
    protected MissingMeasureType missingMeasure;
    @XmlElement(required = true)
    protected PerformingOrganizationType performingOrganization;
    @XmlElement(required = true)
    protected SourceSystemType sourceSystem;
    @XmlAnyElement(lax = true)
    protected List<Object> any;

    /**
     * Gets the value of the measureId property.
     * 
     * @return
     *     possible object is
     *     {@link IIType }
     *     
     */
    public IIType getMeasureId() {
        return measureId;
    }

    /**
     * Sets the value of the measureId property.
     * 
     * @param value
     *     allowed object is
     *     {@link IIType }
     *     
     */
    public void setMeasureId(IIType value) {
        this.measureId = value;
    }

    /**
     * Gets the value of the measureIdVersionNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMeasureIdVersionNumber() {
        return measureIdVersionNumber;
    }

    /**
     * Sets the value of the measureIdVersionNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMeasureIdVersionNumber(BigInteger value) {
        this.measureIdVersionNumber = value;
    }

    /**
     * Gets the value of the measurePeriod property.
     * 
     * @return
     *     possible object is
     *     {@link DatePeriodType }
     *     
     */
    public DatePeriodType getMeasurePeriod() {
        return measurePeriod;
    }

    /**
     * Sets the value of the measurePeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link DatePeriodType }
     *     
     */
    public void setMeasurePeriod(DatePeriodType value) {
        this.measurePeriod = value;
    }

    /**
     * Gets the value of the firstServiceEncounter property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstServiceEncounter() {
        return firstServiceEncounter;
    }

    /**
     * Sets the value of the firstServiceEncounter property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstServiceEncounter(String value) {
        this.firstServiceEncounter = value;
    }

    /**
     * Gets the value of the lastServiceEncounter property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLastServiceEncounter() {
        return lastServiceEncounter;
    }

    /**
     * Sets the value of the lastServiceEncounter property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLastServiceEncounter(String value) {
        this.lastServiceEncounter = value;
    }

    /**
     * Gets the value of the proportionMeasure property.
     * 
     * @return
     *     possible object is
     *     {@link ProportionMeasureType }
     *     
     */
    public ProportionMeasureType getProportionMeasure() {
        return proportionMeasure;
    }

    /**
     * Sets the value of the proportionMeasure property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProportionMeasureType }
     *     
     */
    public void setProportionMeasure(ProportionMeasureType value) {
        this.proportionMeasure = value;
    }

    /**
     * Gets the value of the continuousVariableMeasure property.
     * 
     * @return
     *     possible object is
     *     {@link ContinuousVariableMeasureType }
     *     
     */
    public ContinuousVariableMeasureType getContinuousVariableMeasure() {
        return continuousVariableMeasure;
    }

    /**
     * Sets the value of the continuousVariableMeasure property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContinuousVariableMeasureType }
     *     
     */
    public void setContinuousVariableMeasure(ContinuousVariableMeasureType value) {
        this.continuousVariableMeasure = value;
    }

    /**
     * Gets the value of the cohortMeasure property.
     * 
     * @return
     *     possible object is
     *     {@link CohortMeasureType }
     *     
     */
    public CohortMeasureType getCohortMeasure() {
        return cohortMeasure;
    }

    /**
     * Sets the value of the cohortMeasure property.
     * 
     * @param value
     *     allowed object is
     *     {@link CohortMeasureType }
     *     
     */
    public void setCohortMeasure(CohortMeasureType value) {
        this.cohortMeasure = value;
    }

    /**
     * Gets the value of the missingMeasure property.
     * 
     * @return
     *     possible object is
     *     {@link MissingMeasureType }
     *     
     */
    public MissingMeasureType getMissingMeasure() {
        return missingMeasure;
    }

    /**
     * Sets the value of the missingMeasure property.
     * 
     * @param value
     *     allowed object is
     *     {@link MissingMeasureType }
     *     
     */
    public void setMissingMeasure(MissingMeasureType value) {
        this.missingMeasure = value;
    }

    /**
     * Gets the value of the performingOrganization property.
     * 
     * @return
     *     possible object is
     *     {@link PerformingOrganizationType }
     *     
     */
    public PerformingOrganizationType getPerformingOrganization() {
        return performingOrganization;
    }

    /**
     * Sets the value of the performingOrganization property.
     * 
     * @param value
     *     allowed object is
     *     {@link PerformingOrganizationType }
     *     
     */
    public void setPerformingOrganization(PerformingOrganizationType value) {
        this.performingOrganization = value;
    }

    /**
     * Gets the value of the sourceSystem property.
     * 
     * @return
     *     possible object is
     *     {@link SourceSystemType }
     *     
     */
    public SourceSystemType getSourceSystem() {
        return sourceSystem;
    }

    /**
     * Sets the value of the sourceSystem property.
     * 
     * @param value
     *     allowed object is
     *     {@link SourceSystemType }
     *     
     */
    public void setSourceSystem(SourceSystemType value) {
        this.sourceSystem = value;
    }

    /**
     * Gets the value of the any property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the any property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAny().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Element }
     * {@link Object }
     * 
     * 
     */
    public List<Object> getAny() {
        if (any == null) {
            any = new ArrayList<Object>();
        }
        return this.any;
    }

}
