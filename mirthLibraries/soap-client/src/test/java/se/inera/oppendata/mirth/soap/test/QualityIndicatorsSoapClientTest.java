package se.inera.oppendata.mirth.soap.test;

import org.testng.Assert;
import org.testng.annotations.Test;
import se.inera.oppendata.mirth.soap.quality_indicators.*;
import se.inera.oppendata.mirth.ssl.*;

import javax.net.ssl.SSLContext;
import javax.xml.ws.WebServiceException;
import java.net.URI;
import java.nio.file.Paths;
import java.security.InvalidParameterException;
import java.util.List;


/**
 * Created by davsan on 2017-09-12.
 */
public class QualityIndicatorsSoapClientTest {
    private static final String CERTS_FOLDER = "certs";
    private static final String VALID_PEM_FILENAME = "test.oppendata.inera.se.pem";
    private static final String VALID_PFX_FILENAME = "test.oppendata.inera.se.pfx";
    private static final String INVALID_PEM_FILENAME = "test.integration.oppendata.inera.se.pem";
    private static final String VALID_PFX_PASSWORD = "inera2017";
    private static final String VALID_SERVER_CERT_FILENAME = "qa.esb.ntjp.se.cer";
    private static final String INVALID_SERVER_CERT_FILENAME = "selfsigned.badssl.com.cer";
    private static final String HTTP_TRANSPORT_ERROR = "HTTP transport error";
    private static final String SSL_HANDSHAKE_EXCEPTION = "SSLHandshakeException";
    private static final String LOGICAL_ADDRESS_1 = "SE2321000131-S000000013394";
    private static final String INVALID_LOGICAL_ADDRESS = "ABCDEFGHIJKLMNOPQRSTUVXYZ";
    private static final String REPORTING_PERIOD_FROM_2016 = "20160101";
    private static final String REPORTING_PERIOD_TO_2016 = "20161231";
    private static final String MEASURE_ID_ROOT_1 = "1.2.826.0.1.3680043.9.4672.7";
    private static final String MEASURE_ID_EXTENSION_1 = "99011";
    private static final String WSDL_FILENAME = "qa.esb.ntjp.se.wsdl.xml";

    @Test
    public static void shouldThrowWebServiceExceptionWhenWsdlFileNotFound(){
        System.out.println("[shouldThrowWebServiceExceptionWhenWsdlFileNotFound]: STARTED");
        Throwable exception = null;
        try {
            new QualityIndicatorsSoapClient(null,"Invalid file path");
        } catch (Exception e) {
            exception = e;
        }
        Assert.assertNotNull(exception);
        Assert.assertSame(exception.getClass(), WebServiceException.class);
        System.out.println("[shouldThrowWebServiceExceptionWhenWsdlFileNotFound]: FINISHED");
    }

    @Test
    public static void shouldThrowInvalidParameterExceptionWhenWsdlFilePathIsNull(){
        System.out.println("[shouldThrowInvalidParameterExceptionWhenWsdlFilePathIsNull]: STARTED");
        Throwable exception = null;
        try {
            new QualityIndicatorsSoapClient(null,null);
        } catch (Exception e) {
            exception = e;
        }
        Assert.assertNotNull(exception);
        Assert.assertSame(exception.getClass(), InvalidParameterException.class);
        System.out.println("[shouldThrowInvalidParameterExceptionWhenWsdlFilePathIsNull]: FINISHED");
    }

    @Test
    public static void shouldGetMeasurementChecksumWithValidPemCert(){
        System.out.println("[shouldGetMeasurementChecksumWithValidPemCert]: STARTED");
        SSLContext sslContext = null;
        try {
            sslContext = SSLContextCreator.createSSLContext(getPemPath(), getValidTrustCertPath());
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        QualityIndicatorsSoapClient client = null;
        try {
            URI uri = QualityIndicatorsSoapClient.class.getResource(WSDL_FILENAME).toURI();
            String wsdlFilePath = Paths.get(uri).toString();
            client = new QualityIndicatorsSoapClient(sslContext,wsdlFilePath);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        setupSoapClientParameters(client, MEASURE_ID_ROOT_1, MEASURE_ID_EXTENSION_1, false,REPORTING_PERIOD_FROM_2016, REPORTING_PERIOD_TO_2016);
        CustomQualityIndicatorsResponse response = null;
        try {
            response = client.sendMessage(LOGICAL_ADDRESS_1);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        assertMeasurementChecksumExists(response);
        System.out.println("[shouldGetMeasurementChecksumWithValidPemCert]: FINISHED");
    }

    @Test
    public static void shouldGetMeasurementChecksumWithValidPfxCertAndPassword(){
        System.out.println("[shouldGetMeasurementChecksumWithValidPfxCertAndPassword]: STARTED");
        SSLContext sslContext = null;
        try {
            sslContext = SSLContextCreator.createSSLContext(getPfxPath(), VALID_PFX_PASSWORD, getValidTrustCertPath());
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        QualityIndicatorsSoapClient client = null;
        try {
            URI uri = QualityIndicatorsSoapClient.class.getResource(WSDL_FILENAME).toURI();
            String wsdlFilePath = Paths.get(uri).toString();
            client = new QualityIndicatorsSoapClient(sslContext,wsdlFilePath);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        setupSoapClientParameters(client,MEASURE_ID_ROOT_1, MEASURE_ID_EXTENSION_1, false,REPORTING_PERIOD_FROM_2016, REPORTING_PERIOD_TO_2016);
        CustomQualityIndicatorsResponse response = null;
        try {
            response = client.sendMessage(LOGICAL_ADDRESS_1);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        assertMeasurementChecksumExists(response);
        System.out.println("[shouldGetMeasurementChecksumWithValidPfxCertAndPassword]: FINISHED");
    }

    @Test
    public static void responseShouldContainMeasurementList(){
        System.out.println("[responseShouldContainMeasurementList]: STARTED");
        SSLContext sslContext = null;
        try {
            sslContext = SSLContextCreator.createSSLContext(getPemPath(), getValidTrustCertPath());
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        QualityIndicatorsSoapClient client = null;
        try {
            URI uri = QualityIndicatorsSoapClient.class.getResource(WSDL_FILENAME).toURI();
            String wsdlFilePath = Paths.get(uri).toString();
            client = new QualityIndicatorsSoapClient(sslContext,wsdlFilePath);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        setupSoapClientParameters(client, MEASURE_ID_ROOT_1, MEASURE_ID_EXTENSION_1, true,REPORTING_PERIOD_FROM_2016, REPORTING_PERIOD_TO_2016);
        CustomQualityIndicatorsResponse response = null;
        try {
            response = client.sendMessage(LOGICAL_ADDRESS_1);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        assertMeasurementsExist(response);
        System.out.println("[responseShouldContainMeasurementList]: FINISHED");
    }

    @Test
    public static void responseShouldContainReportingPeriod(){
        System.out.println("[responseShouldContainReportingPeriod]: STARTED");
        SSLContext sslContext = null;
        try {
            sslContext = SSLContextCreator.createSSLContext(getPemPath(), getValidTrustCertPath());
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        QualityIndicatorsSoapClient client = null;
        try {
            URI uri = QualityIndicatorsSoapClient.class.getResource(WSDL_FILENAME).toURI();
            String wsdlFilePath = Paths.get(uri).toString();
            client = new QualityIndicatorsSoapClient(sslContext,wsdlFilePath);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        setupSoapClientParameters(client, MEASURE_ID_ROOT_1, MEASURE_ID_EXTENSION_1, false,REPORTING_PERIOD_FROM_2016, REPORTING_PERIOD_TO_2016);
        CustomQualityIndicatorsResponse response = null;
        try {
            response = client.sendMessage(LOGICAL_ADDRESS_1);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        assertResponseContainsReportingPeriod(response, REPORTING_PERIOD_FROM_2016, REPORTING_PERIOD_TO_2016);
        System.out.println("[responseShouldContainReportingPeriod]: FINISHED");
    }

    @Test
    public static void responseShouldNotContainMeasurementList(){
        System.out.println("[responseShouldNotContainMeasurementList]: STARTED");
        SSLContext sslContext = null;
        try {
            sslContext = SSLContextCreator.createSSLContext(getPemPath(), getValidTrustCertPath());
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        QualityIndicatorsSoapClient client = null;
        try {
            URI uri = QualityIndicatorsSoapClient.class.getResource(WSDL_FILENAME).toURI();
            String wsdlFilePath = Paths.get(uri).toString();
            client = new QualityIndicatorsSoapClient(sslContext,wsdlFilePath);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        setupSoapClientParameters(client, MEASURE_ID_ROOT_1, MEASURE_ID_EXTENSION_1, false,REPORTING_PERIOD_FROM_2016, REPORTING_PERIOD_TO_2016);
        CustomQualityIndicatorsResponse response = null;
        try {
            response = client.sendMessage(LOGICAL_ADDRESS_1);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        assertMeasurementsDoNotExist(response);
        System.out.println("[responseShouldNotContainMeasurementList]: FINISHED");
    }


    @Test
    public static void shouldGetInvalidLogicalAddressError(){
        System.out.println("[shouldGetInvalidLogicalAddressError]: STARTED");
        SSLContext sslContext = null;
        try {
            sslContext = SSLContextCreator.createSSLContext(getPemPath(), getValidTrustCertPath());
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        QualityIndicatorsSoapClient client = null;
        try {
            URI uri = QualityIndicatorsSoapClient.class.getResource(WSDL_FILENAME).toURI();
            String wsdlFilePath = Paths.get(uri).toString();
            client = new QualityIndicatorsSoapClient(sslContext,wsdlFilePath);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        setupSoapClientParameters(client, MEASURE_ID_ROOT_1, MEASURE_ID_EXTENSION_1, false,REPORTING_PERIOD_FROM_2016, REPORTING_PERIOD_TO_2016);
        String exception = "";
        try {
            client.sendMessage(INVALID_LOGICAL_ADDRESS);
        } catch (Exception e) {
            exception = e.getMessage();
        }
        Assert.assertTrue(exception.contains(FaultCodes.INVALID_LOGICAL_ADDRESS));
        System.out.println("[shouldGetInvalidLogicalAddressError]: FINISHED");
    }

    @Test
    public static void shouldNotGetErrorWithValidServerCert(){
        System.out.println("[shouldNotGetErrorWithValidServerCert]: STARTED");
        SSLContext sslContext = null;
        try {
            sslContext = SSLContextCreator.createSSLContext(getPemPath(), getValidTrustCertPath());
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        QualityIndicatorsSoapClient client = null;
        try {
            URI uri = QualityIndicatorsSoapClient.class.getResource(WSDL_FILENAME).toURI();
            String wsdlFilePath = Paths.get(uri).toString();
            client = new QualityIndicatorsSoapClient(sslContext,wsdlFilePath);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        setupSoapClientParameters(client, MEASURE_ID_ROOT_1, MEASURE_ID_EXTENSION_1, false,REPORTING_PERIOD_FROM_2016, REPORTING_PERIOD_TO_2016);
        CustomQualityIndicatorsResponse response = null;
        try {
            response = client.sendMessage(LOGICAL_ADDRESS_1);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        assertReportDetailsExist(response);
        System.out.println("[shouldNotGetErrorWithValidServerCert]: FINISHED");
    }

    @Test
    public static void shouldGetErrorWithWrongTrustCert(){
        System.out.println("[shouldGetErrorWithWrongServerCert]: STARTED");
        SSLContext sslContext = null;
        try {
            sslContext = SSLContextCreator.createSSLContext(getPemPath(), getInvalidTrustCertPath());
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        QualityIndicatorsSoapClient client = null;
        try {
            URI uri = QualityIndicatorsSoapClient.class.getResource(WSDL_FILENAME).toURI();
            String wsdlFilePath = Paths.get(uri).toString();
            client = new QualityIndicatorsSoapClient(sslContext,wsdlFilePath);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        setupSoapClientParameters(client, MEASURE_ID_ROOT_1, MEASURE_ID_EXTENSION_1, false,REPORTING_PERIOD_FROM_2016, REPORTING_PERIOD_TO_2016);
        String exception = "";
        try {
            client.sendMessage(LOGICAL_ADDRESS_1);
        } catch (Exception e) {
            exception = e.getMessage();
        }
        Assert.assertTrue(exception.contains(SSL_HANDSHAKE_EXCEPTION));
        System.out.println("[shouldGetErrorWithWrongServerCert]: FINISHED");
    }

    @Test
    public static void shouldGetAuthorizationMissingErrorWithInvalidPemCert(){
        System.out.println("[shouldGetAuthorizationMissingErrorWithInvalidPemCert]: STARTED");
        SSLContext sslContext = null;
        try {
            sslContext = SSLContextCreator.createSSLContext(getInvalidPemPath(), getValidTrustCertPath());
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        QualityIndicatorsSoapClient client = null;
        try {
            URI uri = QualityIndicatorsSoapClient.class.getResource(WSDL_FILENAME).toURI();
            String wsdlFilePath = Paths.get(uri).toString();
            client = new QualityIndicatorsSoapClient(sslContext,wsdlFilePath);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        setupSoapClientParameters(client, MEASURE_ID_ROOT_1, MEASURE_ID_EXTENSION_1, false,REPORTING_PERIOD_FROM_2016, REPORTING_PERIOD_TO_2016);
        String exception = "";
        try {
            client.sendMessage(LOGICAL_ADDRESS_1);
        } catch (Exception e) {
            exception = e.getMessage();
        }
        Assert.assertTrue(exception.contains(FaultCodes.AUTHORIZATION_MISSING));
        System.out.println("[shouldGetAuthorizationMissingErrorWithInvalidPemCert]: FINISHED");
    }

    @Test
    public static void shouldCauseHttpTransportErrorWithoutValidKey(){
        System.out.println("[shouldCauseHttpTransportErrorWithoutValidKey]: STARTED");
        SSLContext sslContext = null;
        try {
            sslContext = SSLContextCreator.createSSLContext(getValidTrustCertPath());
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        QualityIndicatorsSoapClient client = null;
        try {
            URI uri = QualityIndicatorsSoapClient.class.getResource(WSDL_FILENAME).toURI();
            String wsdlFilePath = Paths.get(uri).toString();
            client = new QualityIndicatorsSoapClient(sslContext,wsdlFilePath);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        setupSoapClientParameters(client, MEASURE_ID_ROOT_1, MEASURE_ID_EXTENSION_1, false,REPORTING_PERIOD_FROM_2016, REPORTING_PERIOD_TO_2016);
        String exception = "";
        try {
            client.sendMessage(LOGICAL_ADDRESS_1);
        } catch (Exception e) {
            exception = e.getMessage();
        }
        Assert.assertTrue(exception.contains(HTTP_TRANSPORT_ERROR));
        System.out.println("[shouldCauseHttpTransportErrorWithoutValidKey]: FINISHED");
    }

    @Test
    public static void shouldGetParamsAsString(){
        System.out.println("[shouldGetParamsAsString]: STARTED");
        SSLContext sslContext = null;
        try {
            sslContext = SSLContextCreator.createSSLContext(getPemPath(), getValidTrustCertPath());
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        QualityIndicatorsSoapClient client = null;
        try {
            URI uri = QualityIndicatorsSoapClient.class.getResource(WSDL_FILENAME).toURI();
            String wsdlFilePath = Paths.get(uri).toString();
            client = new QualityIndicatorsSoapClient(sslContext,wsdlFilePath);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        setupSoapClientParameters(client, MEASURE_ID_ROOT_1, MEASURE_ID_EXTENSION_1, true,REPORTING_PERIOD_FROM_2016, REPORTING_PERIOD_TO_2016);
        String paramsString = client.getParamsAsString();
        Assert.assertTrue(paramsString.contains("INCLUDE MEASUREMENTS"));
        Assert.assertTrue(paramsString.contains(MEASURE_ID_ROOT_1) && paramsString.contains(MEASURE_ID_EXTENSION_1));
        Assert.assertTrue(paramsString.contains(REPORTING_PERIOD_FROM_2016) && paramsString.contains(REPORTING_PERIOD_TO_2016));
        System.out.println("[shouldGetParamsAsString]: FINISHED");
    }

    private static void setupSoapClientParameters(QualityIndicatorsSoapClient client, String measureIdRoot, String measureIdExtension, boolean includeMeasurements, String reportingPeriodFrom, String reportingPeriodTo){
        client.addMeasureId(measureIdRoot, measureIdExtension);
        client.setIncludeMeasurements(includeMeasurements);
        client.setReportingPeriod(reportingPeriodFrom, reportingPeriodTo);
    }

    private static void assertMeasurementsExist(CustomQualityIndicatorsResponse response){
        assertReportDetailsExist(response);
        List<MeasurementType> measurements = response.getMeasurements();
        Assert.assertNotNull(measurements);
        Assert.assertFalse(measurements.isEmpty());
    }

    private static void assertMeasurementsDoNotExist(CustomQualityIndicatorsResponse response){
        assertReportDetailsExist(response);
        List<MeasurementType> measurements = response.getMeasurements();
        Assert.assertNotNull(measurements);
        Assert.assertTrue(measurements.isEmpty());
    }

    private static void assertMeasurementChecksumExists(CustomQualityIndicatorsResponse response){
        assertReportDetailsExist(response);
        String measurementChecksum = response.getReportDetails().getMeasurementChecksum();
        Assert.assertNotNull(measurementChecksum);
        Assert.assertNotEquals(measurementChecksum, "");
    }
    private static void assertReportDetailsExist(CustomQualityIndicatorsResponse response){
        Assert.assertNotNull(response.getReportDetails());
    }

    private static void assertResponseContainsReportingPeriod(CustomQualityIndicatorsResponse response, String expectedStart, String expectedEnd){
        assertReportDetailsExist(response);
        String reportingPeriodStart = response.getReportDetails().getReportingPeriodStart();
        String reportingPeriodEnd = response.getReportDetails().getReportingPeriodEnd();
        Assert.assertEquals(reportingPeriodStart, expectedStart);
        Assert.assertEquals(reportingPeriodEnd, expectedEnd);
    }

    private static String getPfxPath(){
        return getCertPath(VALID_PFX_FILENAME);
    }

    private static String getPemPath(){
        return getCertPath(VALID_PEM_FILENAME);
    }

    private static String getInvalidPemPath(){
        return getCertPath(INVALID_PEM_FILENAME);
    }

    private static String getValidTrustCertPath(){
        return getCertPath(VALID_SERVER_CERT_FILENAME);
    }

    private static String getInvalidTrustCertPath(){
        return getCertPath(INVALID_SERVER_CERT_FILENAME);
    }

    private static String getCertPath(String filename){
        return getFilePath(CERTS_FOLDER, filename);
    }

    private static String getFilePath(String foldername, String filename){
        String filePath = QualityIndicatorsSoapClient.class.getClassLoader().getResource(foldername + "/" + filename).getPath();
        if(filePath.startsWith("/")){
            filePath = filePath.substring(1);
        }
        return filePath;
    }

}
