package se.inera.oppendata.mirth.ssl;

import org.testng.Assert;
import org.testng.annotations.Test;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by davsan on 2018-04-12.
 */
public class TrustManagerCreatorTest {

    @Test
    public static void trustManagersShouldBeNullWhenTrustCertPathIsNull(){
        try{
            TrustManager[] trustManagers = TrustManagerCreator.createTrustManagers(null);
            Assert.assertNull(trustManagers);
        }catch(Exception e){
            Assert.fail();
        }
    }

    @Test
    public static void shouldThrowFileNotFoundExceptionWhenTrustCertFileIsNotFound(){
        Throwable exception = null;
        try{
            TrustManager[] trustManagers = TrustManagerCreator.createTrustManagers("dummy/path/to/trustCert");
        }catch(Exception e){
           exception = e;
        }
        Assert.assertNotNull(exception);
        Assert.assertSame(exception.getClass(),FileNotFoundException.class);
    }

    @Test
    public static void shouldCreateX509TrustManagersWhenAcceptUntrustedIsSpecified(){
        try{
           X509TrustManager[] trustManagers = (X509TrustManager[]) TrustManagerCreator.createTrustManagersThatAcceptUntrustedCertificates();
           Assert.assertTrue(trustManagers.getClass().getCanonicalName().contains(X509TrustManager.class.getCanonicalName()));
        }catch(Exception e){
            Assert.fail();
        }
    }

    @Test
    public static void shouldCreateTrustManagersFromValidTrustCertPath(){
        String certPath = CertFileHandler.getTrustCertPath();
        try{
            TrustManager[] trustManagers = TrustManagerCreator.createTrustManagers(certPath);
            Assert.assertNotNull(trustManagers);
            Assert.assertTrue(trustManagers.length == 1);
        }catch(Exception e){
            Assert.fail();
        }
    }
}
