package se.inera.oppendata.mirth.ssl;

/**
 * Created by davsan on 2018-04-12.
 */
public class CertFileHandler {

    private static final String CERTS_FOLDER = "certs";
    private static final String TRUST_CERT_FILENAME = "selfsigned.badssl.com.cer";
    private static final String PFX_CERT_FILENAME = "pfxcert.pfx";
    private static final String PEM_CERT_FILENAME_WITH_PRIVATE_KEY_DELIMITER = "cert.and.private.key.pem";
    private static final String PEM_CERT_FILENAME_WITH_RSA_PRIVATE_KEY_DELIMITER = "cert.and.rsa.private.key.pem";
    private static final String PEM_CERT_FILENAME_MISSING_CERTIFICATE_DELIMITER = "cert.missing.certificate.delimiter.pem";
    private static final String PEM_CERT_FILENAME_MISSING_KEY_DELIMITER = "cert.missing.key.delimiter.pem";
    private static final String PFX_CERT_PASSWORD = "password";
    private static final String PFX_CERT_WRONG_PASSWORD = "wrong-password";

    public enum PemCertType {
        RSA_PRIVATE_KEY,
        PRIVATE_KEY,
        MISSING_CERTIFICATE,
        MISSING_KEY,
        NONE
    }

    public static String getCertPath(String filename){
        return getFilePath(CERTS_FOLDER, filename);
    }

    public static String getTrustCertPath(){
        return getCertPath(TRUST_CERT_FILENAME);
    }

    public static String getPfxCertPath(){
        return getCertPath(PFX_CERT_FILENAME);
    }

    public static String getPfxPassword(){
        return PFX_CERT_PASSWORD;
    }

    public static String getPfxWrongPassword(){
        return PFX_CERT_WRONG_PASSWORD;
    }

    public static String getPemCertPath(PemCertType pemCertType){
        if(pemCertType == PemCertType.PRIVATE_KEY){
            return getCertPath(PEM_CERT_FILENAME_WITH_PRIVATE_KEY_DELIMITER);
        }else if (pemCertType == PemCertType.RSA_PRIVATE_KEY){
            return getCertPath(PEM_CERT_FILENAME_WITH_RSA_PRIVATE_KEY_DELIMITER);
        }else if (pemCertType == PemCertType.MISSING_CERTIFICATE) {
            return getCertPath(PEM_CERT_FILENAME_MISSING_CERTIFICATE_DELIMITER);
        }else if(pemCertType == PemCertType.MISSING_KEY){
            return getCertPath(PEM_CERT_FILENAME_MISSING_KEY_DELIMITER);
        }else{
            return "";
        }
    }

    private static String getFilePath(String foldername, String filename){
        String filePath = SSLContextCreator.class.getClassLoader().getResource(foldername + "/" + filename).getPath();
        if(filePath.startsWith("/")){
            filePath = filePath.substring(1);
        }
        return filePath;
    }
}
