package se.inera.oppendata.mirth.ssl;

import org.testng.Assert;
import org.testng.annotations.Test;

import javax.net.ssl.KeyManager;
import java.nio.file.InvalidPathException;
import java.nio.file.NoSuchFileException;
import java.security.cert.CertificateException;

/**
 * Created by davsan on 2018-04-12.
 */
public class PemKeyManagerCreatorTest {

    @Test
    public static void shouldThrowInvalidPathExceptionWhenCertPathIsNotAValidPath(){
        Throwable exception = null;
        try{
            PemKeyManagerCreator.createKeyManagers("///");
        }catch(Exception e){
            exception = e;
        }
        Assert.assertNotNull(exception);
        Assert.assertSame(exception.getClass(), InvalidPathException.class);
    }

    @Test
    public static void shouldThrowNoSuchFileExceptionWhenCertFileIsNotFound(){
        Throwable exception = null;
        try{
            PemKeyManagerCreator.createKeyManagers("dummy/path/to/pemCert");
        }catch(Exception e){
            exception = e;
        }
        Assert.assertNotNull(exception);
        Assert.assertSame(exception.getClass(),NoSuchFileException.class);
    }

    @Test
    public static void shouldCreateKeyManagersFromCertWithPrivateKey(){
        String certPath = CertFileHandler.getPemCertPath(CertFileHandler.PemCertType.PRIVATE_KEY);
        try{
            KeyManager[] keyManagers = PemKeyManagerCreator.createKeyManagers(certPath);
            Assert.assertNotNull(keyManagers);
            Assert.assertTrue(keyManagers.length == 1);
        }catch(Exception e){
            Assert.fail();
        }
    }

    @Test
    public static void shouldCreateKeyManagersFromCertWithRsaPrivateKey(){
        String certPath = CertFileHandler.getPemCertPath(CertFileHandler.PemCertType.RSA_PRIVATE_KEY);
        try{
            KeyManager[] keyManagers = PemKeyManagerCreator.createKeyManagers(certPath);
            Assert.assertNotNull(keyManagers);
            Assert.assertTrue(keyManagers.length == 1);
        }catch(Exception e){
            Assert.fail();
        }
    }

    @Test
    public static void shouldThrowCertificateExceptionIfCertificateDelimiterNotFoundInCert(){
        Throwable exception = null;
        try{
            String certPath = CertFileHandler.getPemCertPath(CertFileHandler.PemCertType.MISSING_CERTIFICATE);
            PemKeyManagerCreator.createKeyManagers(certPath);
        }catch(Exception e){
            exception = e;
        }
        Assert.assertNotNull(exception);
        Assert.assertSame(exception.getClass(), CertificateException.class);
    }

    @Test
    public static void shouldThrowCertificateExceptionIfKeyDelimiterNotFoundInCert(){
        Throwable exception = null;
        try{
            String certPath = CertFileHandler.getPemCertPath(CertFileHandler.PemCertType.MISSING_KEY);
            PemKeyManagerCreator.createKeyManagers(certPath);
        }catch(Exception e){
            exception = e;
        }
        Assert.assertNotNull(exception);
        Assert.assertSame(exception.getClass(), CertificateException.class);
    }
}
