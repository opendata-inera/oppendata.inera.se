package se.inera.oppendata.mirth.ssl;

import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;

/**
 * Created by davsan on 2017-09-19.
 */
public abstract class KeyManagerCreator {
    protected String certPath;
    protected String password;

    public KeyManagerCreator(String certPath, String password){
        this.certPath = certPath;
        this.password = password;
    }

    protected String getCertPath(){
        return certPath;
    }

    protected String getPassword(){
        return password;
    }

    public KeyManager[] createKeyManagers() throws Exception {
        char[] pw = password.toCharArray();
        KeyManagerFactory factory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        KeyStore keyStore = createKeyStore();
        if (keyStore == null) {
            return null;
        }

        factory.init(keyStore, pw);
        return factory.getKeyManagers();
    }

    protected abstract KeyStore createKeyStore() throws Exception;
    protected abstract String getKeyStoreType();
}
