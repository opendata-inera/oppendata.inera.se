package se.inera.oppendata.mirth.ssl;

import javax.net.ssl.KeyManager;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

/**
 * Created by davsan on 2017-09-19.
 */
public class PfxKeyManagerCreator extends KeyManagerCreator {
    private static final String KEYSTORE_TYPE = "PKCS12";

    public static KeyManager[] createKeyManagers(String certPath, String password) throws Exception {
        PfxKeyManagerCreator keyManagerCreator = new PfxKeyManagerCreator(certPath, password);
        return keyManagerCreator.createKeyManagers();
    }

    public PfxKeyManagerCreator(String certPath, String password){
        super(certPath, password);
    }

    @Override
    protected KeyStore createKeyStore() throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException {
            KeyStore keyStore = KeyStore.getInstance(KEYSTORE_TYPE);
            keyStore.load(new FileInputStream(certPath), password.toCharArray());
            return keyStore;
    }

    @Override
    protected String getKeyStoreType() {
        return KEYSTORE_TYPE;
    }
}
