package se.inera.oppendata.mirth.ssl;

import javax.net.ssl.KeyManager;
import javax.xml.bind.DatatypeConverter;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.interfaces.RSAPrivateKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.text.ParseException;
import java.util.IllegalFormatException;

/**
 * Created by davsan on 2017-09-19.
 */
public class PemKeyManagerCreator extends KeyManagerCreator {
    private static final String CERTIFICATE_BEGIN_DELIMITER = "-----BEGIN CERTIFICATE-----";
    private static final String CERTIFICATE_END_DELIMITER = "-----END CERTIFICATE-----";
    private static final String KEY_BEGIN_DELIMITER = "-----BEGIN PRIVATE KEY-----";
    private static final String KEY_END_DELIMITER = "-----END PRIVATE KEY-----";
    private static final String RSA_KEY_BEGIN_DELIMITER = "-----BEGIN RSA PRIVATE KEY-----";
    private static final String RSA_KEY_END_DELIMITER = "-----END RSA PRIVATE KEY-----";
    private static final String KEY_ALIAS = "dummy-key-alias";
    private static final String CERT_ALIAS = "dummy-cert-alias";
    private static final String KEY_PASSWORD = "dummy-key-password";
    private static final String KEYSTORE_TYPE = "JKS";

    public static KeyManager[] createKeyManagers(String certPath) throws Exception {
        PemKeyManagerCreator keyManagerCreator = new PemKeyManagerCreator(certPath);
        return keyManagerCreator.createKeyManagers();
    }

    public PemKeyManagerCreator(String certPath){
        super(certPath, KEY_PASSWORD);
    }

    public String getKeyStoreType(){
        return KEYSTORE_TYPE;
    }

    @Override
    protected KeyStore createKeyStore() throws CertificateException, InvalidKeySpecException, NoSuchAlgorithmException, KeyStoreException, IOException {
        Path path = Paths.get(certPath);

        byte[] certAndKey = Files.readAllBytes(path);

        byte[] certBytes = parseDERFromPEM(certAndKey, CERTIFICATE_BEGIN_DELIMITER, CERTIFICATE_END_DELIMITER);
        if(certBytes == null){
            throw new CertificateException("No certificate found in pem file.");
        }
        byte[] keyBytes = parseDERFromPEM(certAndKey, KEY_BEGIN_DELIMITER, KEY_END_DELIMITER);
        if(keyBytes == null){
            keyBytes = parseDERFromPEM(certAndKey, RSA_KEY_BEGIN_DELIMITER, RSA_KEY_END_DELIMITER);
            if(keyBytes == null){
                throw new CertificateException("No private key found in pem file.");
            }
        }

        X509Certificate cert = generateCertificateFromDER(certBytes);
        RSAPrivateKey key = generatePrivateKeyFromDER(keyBytes);

        KeyStore keystore = KeyStore.getInstance(KEYSTORE_TYPE);
        keystore.load(null);
        keystore.setCertificateEntry(CERT_ALIAS, cert);
        keystore.setKeyEntry(KEY_ALIAS, key, password.toCharArray(), new Certificate[] {cert});
        return keystore;
    }

    private static byte[] parseDERFromPEM(byte[] pem, String beginDelimiter, String endDelimiter) {
        if(pem == null){
            return null;
        }
        String data = new String(pem);
        String[] tokens = data.split(beginDelimiter);

        if(tokens.length < 2){
            return null;
        }

        tokens = tokens[1].split(endDelimiter);

        if(tokens.length == 0){
            return null;
        }
        return DatatypeConverter.parseBase64Binary(tokens[0]);
    }

    private static RSAPrivateKey generatePrivateKeyFromDER(byte[] keyBytes) throws InvalidKeySpecException, NoSuchAlgorithmException {
        if(keyBytes == null){
            return null;
        }
        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(keyBytes);

        KeyFactory factory = KeyFactory.getInstance("RSA");

        return (RSAPrivateKey)factory.generatePrivate(spec);
    }

    private static X509Certificate generateCertificateFromDER(byte[] certBytes) throws CertificateException {
        if(certBytes == null){
            return null;
        }
        CertificateFactory factory = CertificateFactory.getInstance("X.509");

        return (X509Certificate)factory.generateCertificate(new ByteArrayInputStream(certBytes));
    }

}
