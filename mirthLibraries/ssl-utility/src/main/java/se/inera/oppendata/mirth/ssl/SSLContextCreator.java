package se.inera.oppendata.mirth.ssl;

import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

/**
 * Created by davsan on 2017-09-14.
 */
public class SSLContextCreator {

    public static SSLContext createSSLContext(String trustCertPath) throws Exception {
        TrustManager[] trustManagers = TrustManagerCreator.createTrustManagers(trustCertPath);
        return createSSLContext("TLS", null, trustManagers);
    }

    public static SSLContext createSSLContext(String pfxCertPath, String password, String trustCertPath) throws Exception {
        TrustManager[] trustManagers = TrustManagerCreator.createTrustManagers(trustCertPath);
        KeyManager[] keyManagers = PfxKeyManagerCreator.createKeyManagers(pfxCertPath, password);
        return createSSLContext("TLS", keyManagers, trustManagers);
    }

    public static SSLContext createSSLContext(String pemCertPath, String trustCertPath) throws Exception {
        TrustManager[] trustManagers = TrustManagerCreator.createTrustManagers(trustCertPath);
        KeyManager[] keyManagers = PemKeyManagerCreator.createKeyManagers(pemCertPath);
        return createSSLContext("TLS", keyManagers, trustManagers);
    }

    public static SSLContext createSSLContext(String pemCertPath, boolean acceptUntrusted) throws Exception {
        TrustManager[] trustManagers = acceptUntrusted ? TrustManagerCreator.createTrustManagersThatAcceptUntrustedCertificates() : null;
        KeyManager[] keyManagers = PemKeyManagerCreator.createKeyManagers(pemCertPath);
        return createSSLContext("TLS", keyManagers, trustManagers);
    }

    public static SSLContext createSSLContext(String pfxCertPath, String password, boolean acceptUntrusted) throws Exception {
        TrustManager[] trustManagers = acceptUntrusted ? TrustManagerCreator.createTrustManagersThatAcceptUntrustedCertificates() : null;
        KeyManager[] keyManagers = PfxKeyManagerCreator.createKeyManagers(pfxCertPath, password);
        return createSSLContext("TLS", keyManagers, trustManagers);
    }

    public static SSLContext createSSLContext(boolean acceptUntrusted) throws Exception {
        TrustManager[] trustManagers = acceptUntrusted ? TrustManagerCreator.createTrustManagersThatAcceptUntrustedCertificates() : null;
        return createSSLContext("TLS", null, trustManagers);
    }

    /**
     * Initializes and returns an SLLContext. If the trustManagers parameter is null, the default trustManagers will be used.
     *
     */
     public static SSLContext createSSLContext(String protocol, KeyManager[] keyManagers, TrustManager[] trustManagers) throws KeyManagementException, NoSuchAlgorithmException, NullPointerException {
        if(protocol == null){
            throw new NullPointerException("The protocol parameter was null.");
        }

        SSLContext sslContext = SSLContext.getInstance(protocol);
        sslContext.init(keyManagers, trustManagers, null);
        return sslContext;
    }

}
