package se.inera.oppendata.mirth.ssl;

import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;

/**
 * Created by davsan on 2017-09-22.
 */
public class TrustManagerCreator {
    private static final String TRUST_STORE_ALIAS = "truststore-alias";


    public static TrustManager[] createTrustManagersThatAcceptUntrustedCertificates(){
        return new X509TrustManager[]{new X509TrustManager(){
            public void checkClientTrusted(X509Certificate[] chain,
                                           String authType) throws CertificateException {}
            public void checkServerTrusted(X509Certificate[] chain,
                                           String authType) throws CertificateException {}
            public X509Certificate[] getAcceptedIssuers() {
                return new X509Certificate[0];
            }}};
    }

    public static TrustManager[] createTrustManagers(String trustCertPath) throws CertificateException, NoSuchAlgorithmException, KeyStoreException, IOException {
        if (trustCertPath == null) {
            return null;
        }
        KeyStore trustStore = createTrustStore(trustCertPath);
        if (trustStore == null) {
            return null;
        }
        TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        tmf.init(trustStore);
        return tmf.getTrustManagers();
    }

    private static KeyStore createTrustStore(String filePath) throws CertificateException, NoSuchAlgorithmException, IOException, KeyStoreException {
        KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
        trustStore.load(null);
        FileInputStream fis = new FileInputStream(filePath);
        BufferedInputStream bis = new BufferedInputStream(fis);
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
        while (bis.available() > 0) {
            Certificate cert = cf.generateCertificate(bis);
            trustStore.setCertificateEntry(TRUST_STORE_ALIAS + "-" + bis.available(), cert);
        }
        return trustStore;
    }

}
