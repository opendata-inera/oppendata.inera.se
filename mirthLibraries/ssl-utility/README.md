## SSL utility library for Mirth Connect
This is an SSL utility library to be used in mirth connect. It offers the functionality to create SSLContext objects from client certificates and trust certificates.
This functionality provides a means of handling client and server authentication. Use this library to create an SSLContext to be used with the RestClient or Soap clients.

## Installation
1. Install Java.
2. Install Maven.
3. Package the library using Maven from the root folder of this repository: mvn clean package
4. Grap the resulting jar and copy it to the mirth subfolder custom-lib, eg /opt/mirth-connect/custom-lib: cp target/ssl-utility-1.0.jar <INSERT_custom-lib_PATH_HERE>

## Usage within mirth
1. Connect and login to mirth connect using the administrator client.
2. Update resources (no need to restart mirth), click: SETTINGS - RESOURCES - RELOAD RESOURCE
3. Insert a javascript component in a channel (no need to add dependencies)

## Custom external libraries
None

## Packages depending on this library
1. se.inera.oppendata.mirth.soap.quality_indicators (soap-client project).
2. se.inera.oppendata.mirth.rest (rest-client project). 
Note: When creating a new version of the ssl-utility jar file, add the jar file to the packages above, to keep them up to date and for testing the functionality.

## Example code 1
var   pemCertPath = "/opt/mirthconnect/ca/test.oppendata.inera.se.pem";
      trustCertPath = "/opt/mirthconnect/ca/ntjp_certs.cer";
      sslContext = Packages.se.inera.oppendata.mirth.ssl.SSLContextCreator.createSSLContext(pemCertPath, trustCertPath);
      soapClient = Packages.se.inera.oppendata.mirth.soap.quality_indicators.QualityIndicatorsSoapClient.createSoapClient(sslContext);

## Example Code 2
var url = "https://hsatest.inera.se/hsafileservice/informationlist/psiPublicUnits.zip";
    fileOutPath = "/opt/mirthconnect/data/1_waiting/output.zip";
    key = "/opt/mirthconnect/ca/test.integration.oppendata.inera.se.pem";
    sslContext = Packages.se.inera.oppendata.mirth.ssl.SSLContextCreator.createSSLContext(key, true); //Initialize the sslContext with the key and accept untrusted certificates.
    restClient = new Packages.se.inera.oppendata.mirth.rest.RestClient(sslContext);
    res = restClient.getFile(url, fileOutPath);
logger.debug('[HSA import]: FINISHED. Status Code: '+res.status);


## Reference

* SSLContextCreator - Contains various static methods used to create and initialize SSLContext objects with client and server certificates.
  
  * SSLContext createSSLContext(String trustCertPath)
  * SSLContext createSSLContext(String pfxCertPath, String password, String trustCertPath)
  * SSLContext createSSLContext(String pemCertPath, String trustCertPath)
  * SSLContext createSSLContext(String pemCertPath, boolean acceptUntrusted)
  * SSLContext createSSLContext(String pfxCertPath, String password, boolean acceptUntrusted)
  * SSLContext createSSLContext(boolean acceptUntrusted)
  * SSLContext createSSLContext(String protocol, KeyManager[] keyManagers, TrustManager[] trustManagers)

  * NOTE: The default ssl protocol is set to "TLS". Use the method with the protocol parameter if you need to use another protocol.  

* TrustManagerCreator - Contains static methods used to create trust managers for use in ssl contexts, containing details of what server certificates to trust.
  
  * TrustManager[] createTrustManagersThatAcceptUntrustedCertificates() - Creates a X509TrustManager[] object that accepts untrusted server certificates.
  * TrustManager[] createTrustManagers(String trustCertPath) - Creates a TrustManager[] object that will accept requests to a server with the certificate in the trustCertPath.   

* KeyManagerCreator - Abstract class extended by PemKeyManagerCreator and PfxKeyManagerCreator (see below)
  
  * KeyManagerCreator(String certPath, String password) - Constructor
  * KeyManager[] createKeyManagers() - Creates keyManagers from the certPath and password provided in the constructor.

* PemKeyManagerCreator - Used to create keyManagers from a .pem file. Extends KeyManagerCreator.
  
  * static KeyManager[] createKeyManagers(String certPath) - Creates keyManagers from a .pem file
  * PemKeyManagerCreator(String certPath) - Calls the KeyManagerCreator constructor with the certPath and a dummy password

* PfxKeyManagerCreator -  Used to create keyManagers from a .pfx file and a password. Extends KeyManagerCreator

  * static KeyManager[] createKeyManagers(String certPath, String password) - Creates keyManagers from a .pfx file and password
  * PfxKeyManagerCreator(String certPath, String password) - Calls the KeyManagerCreator constructor with the certPath and password

## Explanation of parameters:
* pemCertPath - The path to a .pem file containing the client certificate and private key. The certificate needs to be delimited by the strings below:
  -----BEGIN CERTIFICATE-----
  {ENCRYPTED CERTIFICATE HERE}
  -----END CERTIFICATE-----

  And the private key must be delimited by either:

  -----BEGIN PRIVATE KEY-----
  {ENCRYPTED KEY HERE}
  -----END PRIVATE KEY-----

  or:

  -----BEGIN RSA PRIVATE KEY-----
  {ENCRYPTED RSA KEY HERE}
  -----END RSA PRIVATE KEY-----

* pfxCertPath - The path to a .pfx file containing the client certificate and private key (encrypted). A password is needed to decrypt it.
* trustCertPath - The path to a .cer file containing the certificate of the server that should be trusted.
* acceptUntrusted -  If true, all certificates will be trusted. If false, the default truststore of the client will be used. If the certificate of the server is not found in the truststore then the request will fail.