# Rest client library for Mirth Connect
This is a REST client library to be used in mirth connect. It offers get/post over http/https within a javascript reader.

## Installation
1. Install Java.
2. Install Maven.
3. Package the library using Maven from the root folder of this repository: mvn clean package
4. Grap the resulting jar and copy it to the mirth subfolder custom-lib, eg /opt/mirth-connect/custom-lib: cp target/rest-client-1.0.jar <INSERT_custom-lib_PATH_HERE>

## Usage within mirth
1. Connect and login to mirth connect using the administrator client.
2. Update resources (no need to restart mirth), click: SETTINGS - RESOURCES - RELOAD RESOURCE
3. Insert a javascript component in a channel (no need to add dependencies)

## Custom external libraries
ssl-utility (used by unit test cases).

### Example code
  var sslContext = Packages.se.inera.oppendata.mirth.ssl.SSLContextCreator.createSSLContext(true);  //Initialize the sslContext to accept untrusted certificates.
  var restClient = new Packages.se.inera.oppendata.mirth.rest.RestClient(sslContext);
  var headers = {};
  var params = {};
  headers.Authorization = 'AUTH_TOKEN';
  params.query = 'QUERY_STRING';

  logger.info(restClient.get('http://httpbin.org/ip', headers, params).status);
  logger.info(restClient.post('http://httpbin.org/post', 'content', headers, params).status);


## Reference
* RestClient(SSLContext sslContext) - The sslContext to use when sending requests. The sslContext should have been initialized prior to creating the RestClient.
                                      Use the SSLContextCreator utility class (in the ssl-utility project) to create and initialize an sslContext that suits your needs.
                                      If the sslContext is null, the RestClient will be created with a HTTPClient that will not accept untrusted server certificates.

    * CustomResponse uploadFile(String tempFilePath, String url, FileDetails fileDetails, String packageId, Map headers) - Upload a file to CKAN
    
    * CustomResponse getFileAsBase64String(String url, Map headers) - Returns a file as a base64 encoded string.
    * CustomResponse getFile(String url, String savePath, Map headers, Map params) - Download file from URL
    * CustomResponse getFile(String url, String savePath) - Download file from URL
    
    * CustomResponse getUsingCurl(String [] args) - Get request using a curl command (currently not in use). 
    
    * CustomResponse get(String url) - Get URL
    * CustomResponse get(String url, Map<String, String> headers, Map<String, String> params) - Get URL

    * CustomResponse post(String url, String content) - Post content to URL
    * CustomResponse post(String url, String content, Map headers, Map params) - Post content to URL

* CustomResponse  - The response returned by all methods, including an HTTP status code and the response body.
    * int status
    * String body
    * String contentType - optional

* FileDetails   - The file details used when uploading a file. 
    * String fileName
    * String content
    * boolean isBase64Encoded

* StatusCodes - Utility class containing status codes.
