package se.inera.oppendata.mirth.rest;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;

import javax.net.ssl.*;
import java.io.*;
import java.net.URI;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Map;

public class RestClient {

    private SSLContext sslContext;
    private final static String TEXT_PLAIN_CONTENT_TYPE = "text/plain";

    public RestClient(SSLContext sslContext) {
        this.sslContext = sslContext;
    }

    public CustomResponse uploadFile(String tempFilePath, String url, FileDetails fileDetails, String packageId, Map headers){
        CloseableHttpClient httpClient = getHttpClient();
        URI uri = null;
        try {
            uri = getUriBuilder(url, null).build();
        } catch (Exception e) {
            return getExceptionResponse(e);
        }
        HttpPost httpPost = new HttpPost(uri);
        httpPost = (HttpPost) setHeaders(httpPost, headers);

        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        byte[] binaryContent = fileDetails.content.getBytes();
        if(fileDetails.isBase64Encoded){
            Base64.Decoder decoder = Base64.getDecoder();
            try{
                binaryContent = decoder.decode(binaryContent);
            }catch( IllegalArgumentException iae){
                return getExceptionResponse(iae);
            }
        }
        String fullPathToFile = tempFilePath+"/"+fileDetails.fileName;

        try (FileOutputStream fos = new FileOutputStream(fullPathToFile)) {
            fos.write(binaryContent);
        } catch (IOException ioe) {
            return getExceptionResponse(ioe);
        }
        File fileToUpload = new File(fullPathToFile);
        builder.addBinaryBody("upload",fileToUpload);
        builder.addTextBody("name", fileDetails.fileName);
        builder.addTextBody("package_id", packageId);

        HttpEntity multipart = builder.build();
        httpPost.setEntity(multipart);
        try {
            CloseableHttpResponse response = httpClient.execute(httpPost);
            System.out.println(String.format("POST %s: %s", url, response.getStatusLine()));
            String body = getBody(response);
            String contentType = getContentType(response);
            response.close();
            if(fileToUpload.exists()){
                fileToUpload.delete();
            }
            int statusCode = response.getStatusLine().getStatusCode();
            return new CustomResponse(statusCode, body, contentType);
        } catch (Exception e) {
            return getExceptionResponse(e);
        }
    }

    public CustomResponse getFileAsBase64String(String url, Map headers){
        CloseableHttpClient httpClient = getHttpClient();
        URI uri = null;
        try {
            uri = getUriBuilder(url, null).build();
        } catch (Exception e) {
            return getExceptionResponse(e);
        }
        HttpGet httpGet = new HttpGet(uri);
        httpGet = (HttpGet) setHeaders(httpGet, headers);

        try {
            CloseableHttpResponse response = httpClient.execute(httpGet);
            System.out.println(String.format("DOWNLOAD %s: %s", url, response.getStatusLine()));
            System.out.println(response.getEntity().getContentType().getValue());
            int statusCode = response.getStatusLine().getStatusCode();
            String contentType = getContentType(response);
            if(statusCode != StatusCodes.OK){
                String body = getBody(response);
                return new CustomResponse(statusCode, getBody(response), contentType);
            }
            HttpEntity entity = response.getEntity();
            String encodedContent = Base64.getEncoder().encodeToString(EntityUtils.toByteArray(entity));
            CustomResponse customResponse = new CustomResponse(statusCode, encodedContent, contentType);
            return customResponse;
        } catch (Exception e) {
            return getExceptionResponse(e);
        }
    }

    public CustomResponse getFile(String url, String savePath, Map headers, Map params) {
        CloseableHttpClient httpClient = getHttpClient();
        URI uri = null;
        try {
            uri = getUriBuilder(url, params).build();
        } catch (Exception e) {
            return getExceptionResponse(e);
        }
        HttpGet httpGet = new HttpGet(uri);
        httpGet = (HttpGet) setHeaders(httpGet, headers);

        try {
            CloseableHttpResponse response = httpClient.execute(httpGet);
            System.out.println(String.format("DOWNLOAD %s: %s", url, response.getStatusLine()));
            HttpEntity entity = response.getEntity();
            int statusCode = response.getStatusLine().getStatusCode();
            String body = "";
            if (statusCode == StatusCodes.OK) {
                FileOutputStream outstream = new FileOutputStream(savePath);
                entity.writeTo(outstream);
            }else{
                body = getBody(response);
            }
            String contentType = getContentType(response);
            response.close();
            return new CustomResponse(statusCode, body, contentType);
        } catch (Exception e) {
            return getExceptionResponse(e);
        }
    }

    public CustomResponse getFile(String url, String savePath){
        return getFile(url, savePath,null, null);
    }

    public static CustomResponse getUsingCurl(String [] args) {
        String command;
        int statusCode = 500;

        command = "curl -w \\n\\rSTATUSCODE_%{http_code}";

        for (int i = 0; i < args.length; i++) {
            command += " " + args[i];
        }

        try {
            Process p = Runtime.getRuntime().exec(command);
            BufferedReader reader = new BufferedReader(new 	InputStreamReader(p.getInputStream()));

            String output = "";
            String line;

            while ((line = reader.readLine()) != null) {
                output += line;
                if (line.contains("STATUSCODE")) {
                    statusCode = Integer.parseInt(line.split("_")[1]);
                }
            }
            int exitCode = p.waitFor();
            if (exitCode != 0) {
                return getExceptionResponse("Failed to complete curl command");
            }

            return new CustomResponse(statusCode, output, TEXT_PLAIN_CONTENT_TYPE);
        } catch (Exception e) {
            return getExceptionResponse(e.getMessage());
        }
    }

    public CustomResponse get(String url){
        return get(url, null, null);
    }

    public CustomResponse get(String url, Map<String, String> headers, Map<String, String> params){
        CloseableHttpClient httpClient = getHttpClient();
        URI uri = null;

        try {
            uri = getUriBuilder(url, params).build();
        } catch (Exception e) {
            return getExceptionResponse(e.getMessage());
        }
        HttpGet httpGet = new HttpGet(uri);
        httpGet = (HttpGet) setHeaders(httpGet, headers);

        try {
            CloseableHttpResponse response = httpClient.execute(httpGet);
            System.out.println(String.format("GET %s: %s", url, response.getStatusLine()));
            String body = getBody(response);
            String contentType = getContentType(response);
            response.close();
            return new CustomResponse(response.getStatusLine().getStatusCode(), body, contentType);
        } catch (Exception e) {
            return getExceptionResponse(e.getMessage());
        }
    }

    public CustomResponse post(String url, String content){
        return post(url, content, null, null);
    }

    public CustomResponse post(String url, String content, Map headers, Map params) {
        CloseableHttpClient httpClient = getHttpClient();
        URI uri = null;
        try {
            uri = getUriBuilder(url, params).build();
        } catch (Exception e) {
            return getExceptionResponse(e);
        }
        HttpPost httpPost = new HttpPost(uri);
        httpPost = (HttpPost) setHeaders(httpPost, headers);

        try {
            httpPost.setEntity(new ByteArrayEntity(content.getBytes("UTF-8")));
            CloseableHttpResponse response = httpClient.execute(httpPost);
            System.out.println(String.format("POST %s: %s", url, response.getStatusLine()));
            String body = getBody(response);
            String contentType = getContentType(response);
            int statusCode = response.getStatusLine().getStatusCode();
            response.close();
            return new CustomResponse(statusCode, body, contentType);
        } catch (Exception e) {
            return getExceptionResponse(e);
        }
    }

    private static URIBuilder getUriBuilder(String urlString, Map<String, String> params) {
        URIBuilder builder = null;
        try {
            builder = new URIBuilder(urlString);
        } catch (Exception e) {
            return null;
        }

        if (params == null) {
            return builder;
        }
        for (Map.Entry<String, String> entry: params.entrySet()) {
            builder.setParameter(entry.getKey(), entry.getValue());
        }
        return builder;
    }

    private static HttpRequestBase setHeaders(HttpRequestBase request, Map<String, String> headers) {
        if (headers == null) {
            return request;
        }
        List<Header> headersList =new ArrayList<Header>();
        for (Map.Entry<String, String> entry: headers.entrySet()) {
            Header header = new BasicHeader(entry.getKey(), entry.getValue());
            headersList.add(header);
        }
        Header[] headersArray = headersList.toArray(new Header[0]);
        request.setHeaders(headersArray);
        return request;
    }

    private static String getBody(HttpResponse response) {
        HttpEntity entity = response.getEntity();
        StringBuilder sb = new StringBuilder();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(entity.getContent()), 65728);
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return sb.toString();
    }

    private static String getContentType(HttpResponse response){
        try{
            HttpEntity entity = response.getEntity();
            String contentType = entity.getContentType().getValue();
            return contentType;
        }catch(Exception e){
            e.printStackTrace();
            return "";
        }
    }

    private static CustomResponse getExceptionResponse(Exception exception){
        return getExceptionResponse(exception.getMessage());
    }

    private static CustomResponse getExceptionResponse(String exceptionMessage){
        return new CustomResponse(StatusCodes.EXCEPTION_CAUGHT, exceptionMessage, TEXT_PLAIN_CONTENT_TYPE);
    }

    /**
     * Returns a default CloseableHttpClient (not accepting untrusted certificates) if the RestClient has not been initialized with an sslContext.
     * If the sslContext has been set, it will be used to create the HTTPClient.
     */
    private CloseableHttpClient getHttpClient(){
        if (sslContext == null) {
            return HttpClients.createDefault();
        }
        HttpClientBuilder builder = HttpClientBuilder.create();

        builder.setSSLContext(sslContext);
        SSLConnectionSocketFactory sslSocketFactory = new SSLConnectionSocketFactory(sslContext);
        Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                .register("http", PlainConnectionSocketFactory.getSocketFactory())
                .register("https", sslSocketFactory)
                .build();

        PoolingHttpClientConnectionManager connMgr = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
        builder.setConnectionManager(connMgr);
        CloseableHttpClient client = builder.build();
        return client;
    }
}


