package se.inera.oppendata.mirth.rest;

/**
 * Created by davsan on 2017-07-07.
 */
public class StatusCodes {
    public static final int OK = 200;
    public static final int ERROR = 500;
    public static final int EXCEPTION_CAUGHT = 0;
}
