package se.inera.oppendata.mirth.rest;

/**
 * Created by staffannase on 2017-05-16.
 */
public class CustomResponse {
    public int status;
    public String body;
    public String contentType;

    public CustomResponse(int status, String body) {
        this.status = status;
        this.body = body;
        contentType = "";
    }

    public CustomResponse(int status, String body, String contentType){
        this.status = status;
        this.body = body;
        this.contentType = contentType;
    }


}
