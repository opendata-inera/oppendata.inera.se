package se.inera.oppendata.mirth.rest;

/**
 * Created by davsan on 2017-07-03.
 */
public class FileDetails {
    public String fileName;
    public String content;
    public boolean isBase64Encoded;

    public FileDetails(String fileName, String content, boolean isBase64Encoded){
        this.fileName = fileName;
        this.content = content;
        this.isBase64Encoded = isBase64Encoded;
    }

}
