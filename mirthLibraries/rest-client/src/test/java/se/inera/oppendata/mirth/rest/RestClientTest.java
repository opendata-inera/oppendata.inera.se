package se.inera.oppendata.mirth.rest;


import org.testng.Assert;
import org.testng.annotations.Test;
import se.inera.oppendata.mirth.ssl.SSLContextCreator;

import javax.net.ssl.SSLContext;
import java.util.HashMap;
import java.util.Map;


public class RestClientTest {

    private static String CERTIFICATE_EXCEPTION = "SunCertPathBuilderException";
    private static final String VALID_PEM_FILENAME = "test.integration.oppendata.inera.se.pem";
    private static final String HSA_FILE_URL = "https://hsatest.inera.se/hsafileservice/informationlist/psiPublicUnits.zip";
    private static final String HSA_DL_FILE_PATH = "target/psiPublicUnits.zip";
    private static final String HSA_TEST_TRUSTCERT = "siths_type3_ca_v1_pp.cer";
    private static final String SSL_HANDSHAKE_EXCEPTION = "handshake_failure";

    @Test
    public static void shouldGetFileAsJson(){
        String path = "https://test.oppendata.inera.se/dataset/filestore/resource/eee7f2ee-0318-49f1-8c5c-2fd8beaca4c5/download";
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Authorization", "8c85b8c7-0f60-4f73-9288-580b2f8dc89e");
        SSLContext sslContext = null;
        try {
            sslContext = SSLContextCreator.createSSLContext(true);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        RestClient client = new RestClient(sslContext);
        CustomResponse res = client.getFileAsBase64String(path, headers);
        Assert.assertEquals(StatusCodes.OK, res.status);
    }

    @Test
    public static void shouldUploadFile(){
        String url = "https://test.oppendata.inera.se//api/action/resource_create";
        String tempFilePath = "target/";
        String packageId = "filestore";
        FileDetails fileDetails = new FileDetails("restClientTest.txt", "testtesttest", false);
        HashMap<String, String> headers = new HashMap<>();
        headers.put("Authorization", "8c85b8c7-0f60-4f73-9288-580b2f8dc89e");
        SSLContext sslContext = null;
        try {
            sslContext = SSLContextCreator.createSSLContext(true);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        RestClient client = new RestClient(sslContext);
        CustomResponse res = client.uploadFile(tempFilePath, url, fileDetails, packageId, headers);
        Assert.assertEquals(StatusCodes.OK, res.status);
    }

    @Test
    public static void shouldGetHttpbinUsingCurl() {
        String [] args = {"http://httpbin.org"};
        CustomResponse customResponse = RestClient.getUsingCurl(args);
        Assert.assertEquals(StatusCodes.OK, customResponse.status);
    }

    @Test
    public static void shouldNotGetInvalidUrlUsingCurl() {
        String [] args = {"thisisnotworking"};
        CustomResponse customResponse = RestClient.getUsingCurl(args);
        Assert.assertNotEquals(StatusCodes.OK, customResponse.status);
    }

    @Test
    public void shouldGetFromHttp() {
        SSLContext sslContext = null;
        try {
            sslContext = SSLContextCreator.createSSLContext(true);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        RestClient restClient = new RestClient(sslContext);
        CustomResponse response = restClient.get("http://httpbin.org/ip");
        System.out.println(String.format("\nResponse: %s", response.body));
        Assert.assertEquals(response.status, StatusCodes.OK);
    }

    @Test
    public void shouldGetFromHttpWithHeadersAndParams() {
        Map<String, String> headers = new HashMap<>();
        Map<String, String> params = new HashMap<>();
        headers.put("Authorization", "DummyAuth");
        params.put("Query", "DummyQuery");
        SSLContext sslContext = null;
        try {
            sslContext = SSLContextCreator.createSSLContext(true);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }        RestClient restClient = new RestClient(sslContext);
        CustomResponse response = restClient.get("http://httpbin.org/ip", headers, params);
        System.out.println(String.format("\nResponse: %s", response.body));
        Assert.assertEquals(response.status, StatusCodes.OK);
    }

    @Test
    public void shouldGetToHttpWithUntrustingClient() {
        SSLContext sslContext = null;
        try {
            sslContext = SSLContextCreator.createSSLContext(false);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        RestClient restClient = new RestClient(sslContext);
        CustomResponse response = restClient.get("http://httpbin.org/ip");
        System.out.println(String.format("\nResponse: %s", response.body));
        Assert.assertEquals(response.status, StatusCodes.OK);
    }

    @Test
    public void shouldGetFromHttps() {
        SSLContext sslContext = null;
        try {
            sslContext = SSLContextCreator.createSSLContext(true);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        RestClient restClient = new RestClient(sslContext);
        CustomResponse response = restClient.get("https://httpbin.org/ip");
        System.out.println(String.format("\nResponse: %s", response.body));
        Assert.assertEquals(response.status, StatusCodes.OK);
    }

    @Test
    public void shouldGetFromHttpsWithTrustCert() {
        String trustCertPath = getFilePath("selfsigned.badssl.com.crt");
        SSLContext sslContext = null;
        try {
            sslContext = SSLContextCreator.createSSLContext(trustCertPath);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        RestClient restClient = new RestClient(sslContext);
        CustomResponse response = restClient.get("https://self-signed.badssl.com");
        System.out.println(String.format("\nResponse: %s", response.body));
        Assert.assertEquals(response.status, StatusCodes.OK);
    }

    @Test
    public void shouldNotGetFromHttpsWithWrongCert() {
        String trustCertPath = getFilePath("googlese.crt");
        SSLContext sslContext = null;
        try {
            sslContext = SSLContextCreator.createSSLContext(trustCertPath);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        RestClient restClient = new RestClient(sslContext);
        CustomResponse response = restClient.get("https://self-signed.badssl.com");
        System.out.println(String.format("\nResponse: %s", response.body));
        Assert.assertEquals(response.status, StatusCodes.EXCEPTION_CAUGHT);
        Assert.assertEquals(response.body.contains(CERTIFICATE_EXCEPTION), true);
    }

    @Test
    public void shouldNotGetFromHttpsWithMissingCert() {
        SSLContext sslContext = null;
        try {
            sslContext = SSLContextCreator.createSSLContext(false);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        RestClient restClient = new RestClient(sslContext);
        CustomResponse response = restClient.get("https://self-signed.badssl.com");
        System.out.println(String.format("\nResponse: %s", response.body));
        Assert.assertEquals(response.status, StatusCodes.EXCEPTION_CAUGHT);
        Assert.assertEquals(response.body.contains(CERTIFICATE_EXCEPTION), true);
    }

    @Test
    public void shouldGetFromHttpsWithHeadersAndParams() {
        Map<String, String> headers = new HashMap<>();
        Map<String, String> params = new HashMap<>();
        headers.put("Authorization", "DummyAuth");
        params.put("Query", "DummyQuery");
        SSLContext sslContext = null;
        try {
            sslContext = SSLContextCreator.createSSLContext(true);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        RestClient restClient = new RestClient(sslContext);
        CustomResponse response = restClient.get("https://httpbin.org/ip", headers, params);
        System.out.println(String.format("\nResponse: %s", response.body));
        Assert.assertEquals(response.status, StatusCodes.OK);
    }

    @Test
    public void shouldPostToHttp() {
        SSLContext sslContext = null;
        try {
            sslContext = SSLContextCreator.createSSLContext(true);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        RestClient restClient = new RestClient(sslContext);
        CustomResponse response = restClient.post("http://httpbin.org/post", "Valid content");
        System.out.println(String.format("\nResponse: %s", response.body));
        Assert.assertEquals(response.status, StatusCodes.OK);
    }

    @Test
    public void shouldPostToHttpWithHeadersAndParams() {
        Map<String, String> headers = new HashMap<>();
        Map<String, String> params = new HashMap<>();
        headers.put("Authorization", "DummyAuth");
        params.put("Query", "DummyQuery");
        SSLContext sslContext = null;
        try {
            sslContext = SSLContextCreator.createSSLContext(true);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        RestClient restClient = new RestClient(sslContext);
        CustomResponse response = restClient.post("http://httpbin.org/post", "Valid content", headers, params);
        System.out.println(String.format("\nResponse: %s", response.body));
        Assert.assertEquals(response.status, StatusCodes.OK);
    }

    @Test
    public void shouldPostToHttpWithUntrustingClient() {
        SSLContext sslContext = null;
        try {
            sslContext = SSLContextCreator.createSSLContext(false);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        RestClient restClient = new RestClient(sslContext);
        CustomResponse response = restClient.post("http://httpbin.org/post", "Valid content");
        System.out.println(String.format("\nResponse: %s", response.body));
        Assert.assertEquals(response.status, StatusCodes.OK);
    }

    @Test
    public void shouldPostToHttps() {
        SSLContext sslContext = null;
        try {
            sslContext = SSLContextCreator.createSSLContext(true);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        RestClient restClient = new RestClient(sslContext);
        CustomResponse response = restClient.post("https://httpbin.org/post", "Valid content");
        System.out.println(String.format("\nResponse: %s", response.body));
        Assert.assertEquals(response.status, StatusCodes.OK);
    }

    @Test
    public void shouldPostToHttpsWithHeadersAndParams() {
        SSLContext sslContext = null;
        try {
            sslContext = SSLContextCreator.createSSLContext(true);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        RestClient restClient = new RestClient(sslContext);
        Map<String, String> headers = new HashMap<>();
        Map<String, String> params = new HashMap<>();
        headers.put("Authorization", "DummyAuth");
        params.put("Query", "DummyQuery");
        CustomResponse response = restClient.post("https://httpbin.org/post", "Valid content", headers, params);
        System.out.println(String.format("\nResponse: %s", response.body));
        Assert.assertEquals(response.status, StatusCodes.OK);
    }

    @Test
    public void shouldNotPostToHttpsWithWrongTrustCert() {
        String trustCertPath = getFilePath("googlese.crt");
        SSLContext sslContext = null;
        try {
            sslContext = SSLContextCreator.createSSLContext(trustCertPath);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        RestClient restClient = new RestClient(sslContext);
        CustomResponse response = restClient.post("https://httpbin.org/post", "Valid content");
        System.out.println(String.format("\nResponse: %s", response.body));
        Assert.assertEquals(response.status, StatusCodes.EXCEPTION_CAUGHT);
        Assert.assertTrue(response.body.contains(CERTIFICATE_EXCEPTION));
    }

    @Test
    public void shouldGetFileOverHttp() {
        SSLContext sslContext = null;
        try {
            sslContext = SSLContextCreator.createSSLContext(true);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        RestClient restClient = new RestClient(sslContext);
        CustomResponse response = restClient.getFile("http://httpbin.org/image/png", "target/sample1.png");
        Assert.assertEquals(response.status, StatusCodes.OK);
    }

    @Test
    public void shouldGetFileOverHttpWithHeadersAndParams() {
        SSLContext sslContext = null;
        try {
            sslContext = SSLContextCreator.createSSLContext(true);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        RestClient restClient = new RestClient(sslContext);
        Map<String, String> headers = new HashMap<>();
        Map<String, String> params = new HashMap<>();
        headers.put("Authorization", "DummyAuth");
        params.put("Query", "DummyQuery");
        CustomResponse response = restClient.getFile("http://httpbin.org/image/png", "target/sample1.png", headers, params);
        Assert.assertEquals(response.status, StatusCodes.OK);
    }

    @Test
    public void shouldGetFileOverHttpWithUntrustingClient() {
        SSLContext sslContext = null;
        try {
            sslContext = SSLContextCreator.createSSLContext(false);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        RestClient restClient = new RestClient(sslContext);
        CustomResponse response = restClient.getFile("http://httpbin.org/image/png", "target/sample1.png");
        Assert.assertEquals(response.status, StatusCodes.OK);
    }

    @Test
    public void shouldGetFileOverHttps() {
        SSLContext sslContext = null;
        try {
            sslContext = SSLContextCreator.createSSLContext(true);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        RestClient restClient = new RestClient(sslContext);
        CustomResponse response = restClient.getFile("https://httpbin.org/image/png", "target/sample1.png");
        Assert.assertEquals(response.status, StatusCodes.OK);
    }

    @Test
    public void shouldGetFileOverHttpsWithHeadersAndParams() {
        SSLContext sslContext = null;
        try {
            sslContext = SSLContextCreator.createSSLContext(true);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        RestClient restClient = new RestClient(sslContext);
        Map<String, String> headers = new HashMap<>();
        Map<String, String> params = new HashMap<>();
        headers.put("Authorization", "DummyAuth");
        params.put("Query", "DummyQuery");
        CustomResponse response = restClient.getFile("https://httpbin.org/image/png", "target/sample1.png", headers, params);
        Assert.assertEquals(response.status, StatusCodes.OK);
    }

    @Test
    public void shouldGetHsaFileWithValidClientCert(){
        String clientCertPath = getFilePath(VALID_PEM_FILENAME);
        SSLContext sslContext = null;
        try {
            sslContext = SSLContextCreator.createSSLContext(clientCertPath,true);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        RestClient restClient = new RestClient(sslContext);
        CustomResponse res = restClient.getFile(HSA_FILE_URL, HSA_DL_FILE_PATH);
        Assert.assertTrue(res.status == StatusCodes.OK);
    }

    @Test
    public void shouldNotGetHsaFileWithMissingClientCert(){
        SSLContext sslContext = null;
        try {
            sslContext = SSLContextCreator.createSSLContext(true);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        RestClient restClient = new RestClient(sslContext);
        CustomResponse res = restClient.getFile(HSA_FILE_URL, HSA_DL_FILE_PATH);
        Assert.assertTrue(res.status == StatusCodes.EXCEPTION_CAUGHT);
        Assert.assertTrue(res.body.contains(SSL_HANDSHAKE_EXCEPTION));
    }

    @Test
    public void shouldGetHsaFileWithClientAndTrustCerts(){
        String trustCertPath = getFilePath(HSA_TEST_TRUSTCERT);
        String clientCertPath = getFilePath(VALID_PEM_FILENAME);
        SSLContext sslContext = null;
        try {
            sslContext = SSLContextCreator.createSSLContext(clientCertPath, trustCertPath);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail();
        }
        RestClient restClient = new RestClient(sslContext);
        CustomResponse res = restClient.getFile(HSA_FILE_URL, HSA_DL_FILE_PATH);
        Assert.assertEquals(res.status, StatusCodes.OK);
    }

    private static String getFilePath(String filename){
        String filePath = RestClient.class.getClassLoader().getResource(filename).getPath();
        if(filePath.startsWith("/")){
            filePath = filePath.substring(1);
        }
        return filePath;
    }



}
