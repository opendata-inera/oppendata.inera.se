# Inera swagger
This is a node application that hosts a Swagger UI for Ineras opendata platform. This swagger works as documentation as well as for testing Ineras APIs provided by Mirth Connect. The swagger file is generated automatically from Mirth using the mirth APIs. 

**NOTE 1** There are some strong dependencies to mirth for this platform to work

**NOTE 2** Before Mirth version 3.5, channel tags were stored in each channel's XML description file under <tags>. Since 3.5 this field has been removed. 
The channel tags now have to be requested separately by calling the "server/channelTags" Mirth endpoint. The mirth-to-swagger.js file has been updated to accomodate this change.
Should there be a need to revert back to a Mirth version before 3.5, the old version can be found in mirth-to-swagger-legacy.js.   

## Directory structure

| path  | description   |
|---|---|
| api-docs/  | Swagger UI. The only changes made here is the path is edited within index.html  |
| node_modules | NodeJS dependencies for the project |
| samples | Some sample requests |
| templates  | Templates used to generate swagger.json used by Swagge UI |
| app.js | The main app. Can be run using _node app.js_ or _npm start_ |
| config.js | The main configuration file. Needs to be updated to reflect your environment |
| mirth-to-swagger.js | Fetches all the data needed to generate swagger.json |
| mirth-to-swagger-legacy.js | Legacy version of mirth-to-swagger.js, used prior to Mirth version 3.5 |
| package.json | NPM file to manage dependencies and project metadata |
| README.md | Well, this file |
| swagger-generator.js | Generates the actual file from data provided by mirth-to-swagger.js |

## Getting started
This NodeJS project uses NPM, a package manager for node, which makes setting this project up a breeze. Steps follow:
1. Install GIT
2. Install node
3. Checkout this codebase
4. In the basefolder (the folder where this README is located) run:
_npm install_
5. Update config.js to match your environment. For more documentation on configuration refer to the config.js file comments.
6. Start the application from the basefolder with the command: _npm start_

You should now be able to access the portal on the port you specified in the config.js 


## Mapping Mirth API channels to Swagger 
Mirth API channels are mapped to a swagger file by this NodeJS application using Mirths client APIs. For a channel to be mapped it needs to:

1. Be tagged with the tag: SWAGGER
2. Have a valid JSON object in its description field, containing swagger specific attributes, and a _full_channel_name_ attribute containing the name/URL to the channel using the following naming convention (METHOD service version endpoint), example: _GET nkk v1 documents_. For more information regarding the definition, visit the swagger website.

NOTE: The _full_channel_name_ attribute is not a standard swagger attribute, but is added due to the restriction on the Mirth channel names to contain a maximum of 40 characters. Using this attributes allows for longer channel names/URLs. The _full_channel_name_ attribute should also be present in the CHANNEL_IDS field of the configurationMap (see Settings/Configuration Map).  

There is an image(example_mapping_in_mirth.png) in this folder that portrays how a channel should be setup for it to be mapped properly.

## Deploying to production
When deploying to production it is warmly recommened to use PM2 (http://pm2.keymetrics.io/) to run server. PM2 ensures that the application stays up, restarting it when errors occur. PM2 also offers resource management and is a convenient way to run any node application as a daemon. Steps follow:

1. Install pm2: _npm install pm2 -g_
2. Run the app from this folder: _pm2 start app_ 

# NOTE #
If the server has been restarted or any environment variables have been changed, then pm2 will fail to recognize paths starting with './'. 
To solve this problem, start/restart the pm2 process using the flag '--update-env'. Example command: _pm2 start app --update-env_.

# Reading pm2 logs #
To read the output from 'console.log' use the command _pm2 logs_. By default, only the last 15 lines are displayed. Use the flag '--lines' plus the number of lines you would like to display. Example: _pm2 logs --lines 100_ to display the last 100 lines.    

## Adding an example in Swagger UI
In order to display an example response or body in Swagger, a JSON object of the same format as below can be added to a "response" or "parameters" field (in the description in a Mirth channel). 
		
"schema": 
{
		"type": "object",
		"properties" :{ "id":{"type": "integer"} },
		"example": {"id": 30}
}		
