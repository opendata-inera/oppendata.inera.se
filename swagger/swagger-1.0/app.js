var express = require('express'),
    app = express(),
    fs = require('fs'),
    config = require('./config'),
    mirthToSwag  = require('./mirth-to-swagger');

var port = process.env.PORT || config.server.port;
var swaggerFolderPath = './' + config.swagger.swaggerfolder;
var swaggerIndexFile = swaggerFolderPath + '/'+ config.swagger.swaggerIndexFile;
var swaggerIndexTemplateFile = swaggerFolderPath + '/'+ config.swagger.swaggerIndexFileTemplate;

// Perform untrusted requests
if (config.app.performUntrustedRequests) {
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
}

// Replace URL in index.html to the one specified in config.js
fs.readFile(swaggerIndexTemplateFile, 'utf8', function (err,data) {
    if (err) {
        console.log(err);
        return;
    }

    if(!data){
        console.log("[app.js]: The data parameter is undefined.");
        return;
    }
    var path = config.resource_server.scheme + '://' + config.resource_server.hostname + ':' + config.resource_server.port + '/' + config.swagger.outputfile;
    console.log('[app.js] PATH: '+ path);
    var result = data.replace(/url = "http.*";/g, 'url = "' + path + '";');
    fs.writeFile(swaggerIndexFile, result, 'utf8', function (err) {
        if (err) {
            console.log(err);
        }
    });
});

// Generate swagger.json used by Swagger UI
mirthToSwag.generateSwaggerJson();

// Start http server that hosts Swagger UI
app.use(express.static(__dirname + '/' + config.swagger.swaggerfolder));
app.listen(port, function(err) {
    console.log('Swagger documentation served on: %d', port);
});

