var request = require('request'),
    config = require('./config'),
    swaggerGenerator = require('./swagger-generator'),
    parseString = require('xml2js').parseString;

/**
 * 
 * The flow (you can basically read the code from top to bottom): 
 * generateSwaggerJson -> loginToMirth -> getMirthChannels -> parseChannels -> swagger-generater.js
 * 
 */

var generateSwaggerJson = function () {
    loginToMirth(config.mirth.username, config.mirth.password);
};

var loginToMirth = function (username, password) {
    var apiPath = getApiPath();
    request({url: apiPath + '/users/_login', 
        method: 'POST', jar: true, form: {username: username, password: password}}, 
        function (error, response, body) {
            if (error || response.statusCode != '200') {
                console.error('Could not login, halting execution. Status: ' + response.statusCode);
                process.exit(1);
            }
            console.log('Login status:', response && response.statusCode);
            getMirthChannels();
        }
    );
};

function getApiPath(){
    return config.mirth.scheme + '://' + config.mirth.hostname + ':' + config.mirth.port + config.mirth.basepath;
}

var getMirthChannels = function (username, password) {
    var apiPath = getApiPath();
    request({url:  apiPath + '/channels', 
        method: 'GET', jar: true}, function (error, response, body) {
            if (error || response.statusCode != '200') {
                console.error('Could not fetch channels, halting execution. Status: ' + response.statusCode);
                process.exit(1);
            }
            console.log('Channels status:', response && response.statusCode);
            parseChannels(response.body);
        }
    );
};
 
var parseChannels = function (xml) {
    parseString(xml, function (err, json) {
        var channelsToSwaggerize = [];
        for (var i = 0; i < json.list.channel.length; i++) {
            var currentChannel = json.list.channel[i];
            var currentChannelTags = currentChannel.properties[0].tags[0].string;

            if (currentChannelTags === undefined || currentChannelTags.indexOf('SWAGGER') == -1) {
                continue;
            }
            var parsedJson;

            try {
                parsedJson = {
                    id: currentChannel.id[0],
                    enabled: currentChannel.enabled[0],
                    name: currentChannel.name[0],
                    parameters: currentChannel.parameters,
                    description: currentChannel.description[0],
                    tags: currentChannel.properties[0].tags[0].string
                };
            } catch (error) {
                console.error('Could not parse channel for swagger, skipping item');
                continue;
            }

            channelsToSwaggerize.push(parsedJson);
        }
        console.log('Found the following %d channels to swaggerize', channelsToSwaggerize.length);
        for (var i = 0; i < channelsToSwaggerize.length; i++) {
            console.log('   %s - %s', channelsToSwaggerize[i].name, channelsToSwaggerize[i].description);
        }

        // Now let's unparse this thing and get that swagger.json done!
        swaggerGenerator.createJsonFile(channelsToSwaggerize);
    });
};

module.exports = {
    generateSwaggerJson: generateSwaggerJson
};