var request = require('request'),
    config = require('./config'),
    swaggerGenerator = require('./swagger-generator'),
    parseString = require('xml2js').parseString;

/**
 * 
 * The flow (you can basically read the code from top to bottom): 
 * generateSwaggerJson -> loginToMirth -> getChannelIdsContainingSwaggerTag -> parseChannelTags -> getMirthChannels -> parseChannels -> swagger-generator.js
 * 
 */

var channelIdsWithCorrectTag = [];

var generateSwaggerJson = function () {
    loginToMirth(config.mirth.username, config.mirth.password);
};

var loginToMirth = function (username, password) {
    var apiPath = getApiPath();
    request({url: apiPath + '/users/_login', 
        method: 'POST', jar: true, form: {username: username, password: password}}, 
        function (error, response, body) {
            if (error || response.statusCode != '200') {
                console.error('Could not login, halting execution. Status: ' + response.statusCode);
                process.exit(1);
            }
            console.log('Login status:', response && response.statusCode);
            getChannelIdsContainingSwaggerTag();
        }
    );
};

function getApiPath(){
    return config.mirth.scheme + '://' + config.mirth.hostname + ':' + config.mirth.port + config.mirth.basepath;
}

var getChannelIdsContainingSwaggerTag = function (username, password){
    var apiPath = getApiPath();
    request({url:  apiPath + '/server/channelTags', 
        method: 'GET', jar: true}, function (error, response, body) {
            if (error || response.statusCode != '200') {
                console.error('Could not fetch channels, halting execution. Status: ' + response.statusCode);
                process.exit(1);
            }
            console.log('Channels status:', response && response.statusCode);
            parseChannelTags(response.body, 'SWAGGER');
            if(channelIdsWithCorrectTag.length == 0){
                console.error('No channels found with SWAGGER tag.');
                process.exit(1);
            }
            getMirthChannels();
        }
    );
};

var parseChannelTags = function(xml, tagToLookFor){
    parseString(xml, tagToLookFor, function (err, json) {
        var channelTags = json.set.channelTag;
        for (var i = 0; i < channelTags.length; i++) {
            var currentChannelTagName = channelTags[i].name;
            
            if (currentChannelTagName === undefined || currentChannelTagName != tagToLookFor) {
                continue;
            }
            channelIdsWithCorrectTag = channelTags[i].channelIds[0].string;
            break;
        }
        console.log('Found the following %d channelIds with correct tag', channelIdsWithCorrectTag.length);
        for (var i = 0; i < channelIdsWithCorrectTag.length; i++) {
            console.log('   %s', channelIdsWithCorrectTag[i]);
        }
    });
    
};

var getMirthChannels = function (username, password) {
    var apiPath = getApiPath();
    request({url:  apiPath + '/channels', 
        method: 'GET', jar: true}, function (error, response, body) {
            if (error || response.statusCode != '200') {
                console.error('Could not fetch channels, halting execution. Status: ' + response.statusCode);
                process.exit(1);
            }
            console.log('Channels status:', response && response.statusCode);
            parseChannels(response.body);
        }
    );
};
 
var parseChannels = function (xml) {
    parseString(xml, function (err, json) {
        var channelsToSwaggerize = [];
        for (var i = 0; i < json.list.channel.length; i++) {
            var currentChannel = json.list.channel[i];
            if(!containsValue(channelIdsWithCorrectTag, currentChannel.id[0])){
                continue;
            }
            var parsedJson;

            try {
                parsedJson = {
                    id: currentChannel.id[0],
                    name: currentChannel.name[0],
                    description: currentChannel.description[0]
                };
            } catch (error) {
                console.error('Could not parse channel for swagger, skipping item '+error);
                continue;
            }

            channelsToSwaggerize.push(parsedJson);
        }
        console.log('Found the following %d channels to swaggerize', channelsToSwaggerize.length);
        for (var i = 0; i < channelsToSwaggerize.length; i++) {
            console.log('   %s - %s', channelsToSwaggerize[i].name, channelsToSwaggerize[i].description);
        }

        // Now let's unparse this thing and get that swagger.json done!
        swaggerGenerator.createJsonFile(channelsToSwaggerize);
    });
};

function containsValue(array, value){
    for(var i = 0; i < array.length; i++){
        if(array[i] == value){
            return true;
        }
    }
    return false;
}

module.exports = {
    generateSwaggerJson: generateSwaggerJson
};


//Below code may be used if the XML parsing needs to be handled differently depending on Mirth version 

//Version numbers are converted to integer values for conveniency.
//Example:  version 3.5.0 is parsed to 350.
//var MIN_MIRTH_VERSION_WITH_NEW_CHANNEL_TAG_HANDLING_AS_INT = 350;
//var currentMirthVersion = 0;
/*
function getMirthVersionAsInteger (username, password){
    console.log("getVersionAsInteger");
    var apiPath = getApiPath();
    request({url:  apiPath + '/server/version', 
        method: 'GET', jar: true}, function (error, response, body) {
            if (error || response.statusCode != '200') {
                console.error('Could not get version, halting execution. Status: ' + response.statusCode);
                process.exit(1);
            }
            console.log('Response status:', response && response.statusCode);
            currentMirthVersion = parseMirthVersionToInteger(response.body);
            console.log("Current Mirth version as int: "currentMirthVersion);
        }
    );
}

function parseMirthVersionToInteger(str){
    var onlyDigits = str.split('.').join('');
    if(onlyDigits.length < 2){
        return onlyDigits;
    }
    console.log(onlyDigits);
    return onlyDigits.substring(0,2);
}
*/