module.exports = {
    app: {
        performUntrustedRequests: true
    },

    // These server settings are where the swagger api would be hosted
    server: {
        scheme: 'https',
        hostname: 'localhost',
        port: 8002,
        basepath: '/api/v1'
    },

    //These settings specify where swagger should fetch its resources from.
    resource_server: {
        scheme:'https',
        hostname: 'testapi.oppendata.inera.se',
        port: 443,
        basepath: '/api/v1'
    },

 //   These settings specify where the base Mirth APIs are located
    mirth: {
       scheme: 'https',
       hostname: 'localhost',
       port: 8443,
       basepath: '/api',
       username: 'admin',
       password: 'PASSWORD'
   },

 //   These settings specify where the Inera Mirth APIs are located.
 //   This would be the API Router channel in Ineras Mirth installation.
    inera: {
       scheme: 'https',
       hostname: 'test.integration.oppendata.inera.se',
       port: 443,
       basepath: '/api'
    },


    // This is information used to generate the swagger file
    swagger: {
        basefile: './templates/swagger-base.json',
        swaggerfolder: 'api-docs',
        swaggerIndexFile: 'index.html',
        swaggerIndexFileTemplate: 'index-template.html',
        outputfile: 'swagger-out.json',
        title: 'API - Inera Öppendataplattform',
        description: 'Genom vårt öppna API kan du hämta våra datakällor för användning på din webbplats eller i din mobilapp. Detta ger en möjlighet att återanvända den data som skapas inom den offenliga förvaltningen. Vi vill öka kännedomen om våra tjänster och sprida vårt kvalitetssäkrade innehåll för att skapa en bättre hälsa för alla. \n\nAPI är ett tekniskt gränssnitt som kan användas för att utveckla egna tillämpningar med innehåll från Inera. Genom ett standardiserat gränssnitt får du tillgång till bland annat kvalitetsindikatorer och mätvärden från vården, öppen organisationsinformation etc. Med hjälp av en teknisk nyckel ansluter du ditt system till API-lösningen och kan i egna kanaler spegla den data som Inera publicerar tillsammans med SKL. Det ger dina användare relevant och aktuell kunskap inom hälsa och vård. \n\nFör att använda Ineras öppna API behöver du en API-nyckel. Denna får du genom att registrera dig som användare på Ineras Öppendataplattorm: test.oppendata.inera.se \n\n Hur testar jag tjänsterna på denna sida? \n 1. Klicka på den tjänst du önskar testa.\n 2. Fyll i din tekniska nyckel i fältet "authorization".\n 3. Ange eventuella parametrar du vill skicka med anropet. \n 4. Tryck på "Try it out" för att skicka anropet.\n',
        email: 'kundservice@inera.se',
        version: '1.0',
        externalDocDescription: 'EXTERNAL DOCUMENT DESCRIPTION',
        externalDocUrl: 'EXTERNAL DOCUMENT URL',
        apiKeyName: 'Authorization'
    }
};