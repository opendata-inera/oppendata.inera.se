var request = require('request'),
    config = require('./config'),
    swaggerGenerator = require('./swagger-generator'),
    parseString = require('xml2js').parseString,
    channelIdsWithCorrectTag = [],
    channelsToSwaggerize = [],
    statusCodes = {
        OK: '200'
    },
    configurationPropertyAttribute = 'com.mirth.connect.util.ConfigurationProperty';


/**
 * 
 * The flow (you can basically read the code from top to bottom): 
 * generateSwaggerJson -> loginToMirth -> getChannelIdsContainingSwaggerTag -> parseChannelTags -> getMirthChannels -> parseChannels -> swagger-generator.js
 * 
 */

var generateSwaggerJson = function () {
    loginToMirth(config.mirth.username, config.mirth.password);
};

var loginToMirth = function (username, password) {
    var apiPath = getApiPath();
    request({url: apiPath + '/users/_login', 
        method: 'POST', jar: true, form: {username: username, password: password}}, 
        function (error, response, body) {
            if (!wasRequestSuccessful(error, response, statusCodes.OK)) {
                console.error('Could not login, halting execution.');
                process.exit(1);
            }
            console.log('Login status:', response.statusCode);
            getChannelIdsContainingSwaggerTag();
        }
    );
};

function getApiPath(){
    return config.mirth.scheme + '://' + config.mirth.hostname + ':' + config.mirth.port + config.mirth.basepath;
}

var getChannelIdsContainingSwaggerTag = function (username, password){
    var apiPath = getApiPath();
    request({url:  apiPath + '/server/channelTags', 
        method: 'GET', jar: true}, function (error, response, body) {
            if (!wasRequestSuccessful(error, response, statusCodes.OK)) {
                console.error('Could not fetch channels, halting execution.');
                process.exit(1);
            }
            console.log('Channels status:', response.statusCode);
            parseChannelTags(response.body, 'SWAGGER');
            if(channelIdsWithCorrectTag.length == 0){
                console.error('No channels found with SWAGGER tag.');
                process.exit(1);
            }
            getMirthChannels();
        }
    );
};

var parseChannelTags = function(xml, tagToLookFor){
    parseString(xml, tagToLookFor, function (err, json) {
        var channelTags = json.set.channelTag;
        for (var i = 0; i < channelTags.length; i++) {
            var currentChannelTagName = channelTags[i].name;
            
            if (currentChannelTagName === undefined || currentChannelTagName != tagToLookFor) {
                continue;
            }
            channelIdsWithCorrectTag = channelTags[i].channelIds[0].string;
            break;
        }
        console.log('Found %d channels with correct tag', channelIdsWithCorrectTag.length);
    });
    
};

var getMirthChannels = function () {
    var apiPath = getApiPath();
    request({url:  apiPath + '/channels', 
        method: 'GET', jar: true}, function (error, response, body) {
            if (!wasRequestSuccessful(error, response, statusCodes.OK)) {
                console.error('Could not fetch channels, halting execution.');
                process.exit(1);
            }
            console.log('Channels status:', response.statusCode);
            parseChannels(response.body);
        }
    );
};
 
var parseChannels = function (xml) {
    parseString(xml, function (err, json) {
        for (var i = 0; i < json.list.channel.length; i++) {
            var currentChannel = json.list.channel[i];
            if(!containsValue(channelIdsWithCorrectTag, currentChannel.id[0])){
                continue;
            }
            var parsedJson;

            try {
                parsedJson = {
                    id: currentChannel.id[0],
                    name: currentChannel.name[0],
                    description: currentChannel.description[0]
                };
            } catch (error) {
                console.error('Could not parse channel for swagger, skipping item '+error);
                continue;
            }

            channelsToSwaggerize.push(parsedJson);
        }
        
        console.log('Found the following %d channels to swaggerize', channelsToSwaggerize.length);
        for (var i = 0; i < channelsToSwaggerize.length; i++) {
            console.log('   %s - %s', channelsToSwaggerize[i].name, channelsToSwaggerize[i].description);
        }


        if(config.mirth.configurationMap && config.mirth.configurationMap.swaggerKey){
            getConfigurationMap();
        }else{
            console.log('No swagger key found in config file. Skipping search for swagger components in the configurationMap');
            // Now let's unparse this thing and get that swagger.json done!
            swaggerGenerator.createJsonFile(channelsToSwaggerize, {});
        }

    });
};

var getConfigurationMap = function(){
    var apiPath = getApiPath();
    request({url:  apiPath + '/server/configurationMap', 
        method: 'GET', jar: true}, function (error, response, body) {
            if(!wasRequestSuccessful(error, response, statusCodes.OK)){
                console.error('Could not fetch configurationMap, Skipping this step. Status: ' + response.statusCode);
                process.exit(1);
            }
            console.log('Configuration map status:', response.statusCode);
            parseSwaggerComponentsFromConfigurationMap(response.body);
        }
    );
}

var parseSwaggerComponentsFromConfigurationMap = function(xml){
    parseString(xml, function (err, json) {
        var swaggerComponents = {};
        var swaggerKeyFound = false;

        for (var i = 0; i < json.map.entry.length; i++) {
            var currentEntry = json.map.entry[i];
            if(currentEntry.string[0] != config.mirth.configurationMap.swaggerKey){
                continue;
            }

            try {
                swaggerComponents = JSON.parse(currentEntry[configurationPropertyAttribute][0].value[0]);
                if(Object.keys(swaggerComponents).length === 0){
                    console.log('Swagger components key found in configurationMap, but entry was empty.');
                }
                else{
                    console.log('Swagger components found.');
                }
                swaggerKeyFound = true;
                break;
            } catch (error) {
                console.error('Could not parse swagger components from %s entry in configurationMap. Halting execution', currentEntry.string);
                process.exit(1);
            }

        }

        if(!swaggerKeyFound){
            console.log('No Swagger components key found in configurationMap.');
        }
        // Now let's unparse this thing and get that swagger.json done!
        swaggerGenerator.createJsonFile(channelsToSwaggerize, swaggerComponents);
    });
}

function containsValue(array, value){
    for(var i = 0; i < array.length; i++){
        if(array[i] == value){
            return true;
        }
    }
    return false;
}

function wasRequestSuccessful(error, response, expectedStatusCode){
    if(error){
        console.error('Error: '+error);
        return false;
    }else if(response.statusCode != expectedStatusCode){
        console.error('StatusCode: '+response.statusCode + '. Body: \n'+response.body);
        return false;
    }
    return true;
}

module.exports = {
    generateSwaggerJson: generateSwaggerJson
};