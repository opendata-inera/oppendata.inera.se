module.exports = {
    app: {
        performUntrustedRequests: true
    },

    // These server settings are where the swagger api would be hosted. Currently, only the port is used.
    server: {
        scheme: 'http',
        hostname: 'localhost',
        port: 8002
    },

    //These settings specify where the file used to generate the swagger page should be stored.
    resource_server: {
        scheme:'http',
        hostname: 'localhost',
        port: 8002
    },

 //   These settings specify where the base Mirth APIs are located
    mirth: {
       scheme: 'https',
       hostname: 'localhost',
       port: 8443,
       basepath: '/api',
       username: 'admin',
       password: 'admin',
       // Swagger components can be stored in the configurationMap in Mirth, and refered to from the the Mirth channels. If no components are present, remove this attribute or set the 'swaggerKey' to null.
       configurationMap:{
          swaggerKey: 'SWAGGER_COMPONENTS'
       }
   },

 //   These settings specify where the Inera Mirth APIs are located.
 //   This would be the API Router channel in Ineras Mirth installation.
    inera: {
       scheme: 'http',
       hostname: 'localhost',
       port: 8001,
       basepath: '/api',
       //The string in the description file template that should be replaced by the URL to the Mirth APIs. 
       urlPlaceHolder: '_MIRTH_URL_'
    },

    //The link to CKAN where users can register. Used in the swagger description.
    ckan: {
      scheme: 'https',
      hostname: 'test.oppendata.inera.se',
      //The string in the description file template that should be replaced by the URL to CKAN. 
      urlPlaceHolder: '_CKAN_URL_'
    },

    // This is information used to generate the swagger file
    swagger: {
        header:{
          title: "Inera opendata API documentation"
        },
        topbar:{
          url: 'https://www.inera.se',
          logo: 'images/inera-logo.png',
          text: 'Inera' 
        },
        basefile: './templates/swagger-base.json',
        descriptionFileTemplate: './templates/description-template.md',
        descriptionFile: './description-out.md',
        swaggerFolder: 'api-docs',
        swaggerIndexFile: './api-docs/index.html',
        swaggerIndexFileTemplate: './api-docs/index-template.html',
        outputfile: 'swagger-out.json',
        title: 'API - Inera Öppendataplattform',
        contact:{
            email: 'kundservice@inera.se',
            name: 'Inera Kundservice'
        },
        version: '1.0',
        externalDocDescription: 'EXTERNAL DOCUMENT DESCRIPTION',
        externalDocUrl: 'EXTERNAL DOCUMENT URL',
        presetFileTemplate: './api-docs/swagger-ui-standalone-preset-template.js',
        presetFile: './api-docs/swagger-ui-standalone-preset.js'
    }
};