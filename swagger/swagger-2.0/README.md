# Inera Swagger 2.0
This is a node application that hosts a Swagger UI for Ineras opendata platform. This swagger works as documentation as well as for testing Ineras APIs provided by Mirth Connect. The swagger file is generated automatically from Mirth using the mirth APIs. 

**NOTE** There are some strong dependencies to Mirth for this platform to work

## Directory structure

| path  | description   |
|---|---|
| api-docs/  | Contains files used to generate the swagger UI. |
| api-docs/index-template.html | The template used to generate the index.html file. The header title and the url are changed through the swagger-generator.js script.|
| api-docs/swagger-ui-standalone-preset-template.js | The template used to generate the swagger-ui-standalone-preset.js file (which creates elements in the index.html file). The topbar settings are changed through the swagger-generator.js script.|
| node_modules | NodeJS dependencies for the project |
| templates/swagger-base.json  | A template used to generate swagger-out.json used by Swagger UI |
| templates/description-template.md | The template used for creating the description of the Opendata platform in Swagger |
| app.js | The main app. Can be run using _node app.js_ or _npm start_ |
| config.js | The main configuration file. Needs to be updated to reflect your environment |
| mirth-to-swagger.js | Fetches all the data needed to generate swagger-out.json |
| package.json | NPM file to manage dependencies and project metadata |
| README.md | Well, this file |
| swagger-generator.js | Generates the actual file from data provided by mirth-to-swagger.js |
| Dockerfile | The file used to generate a docker image |
| .dockerignore | Specifies what files should be ignored when creating a docker image |

## Getting started
This NodeJS project uses NPM, a package manager for node, which makes setting this project up a breeze. Steps follow:
1. Install GIT
2. Install node
3. Checkout this codebase
4. In the basefolder (the folder where this README is located) run:
`npm install`
5. Update config.js to match your environment. For more documentation on configuration refer to the config.js file comments.
6. Start the application from the basefolder with the command: `npm start`

You should now be able to access the portal on the port you specified in the config.js

## Using a proxy ## 
If you are confronted with the `ETIMEDOUT` error message when running `npm install`, you may have problems with your firewall or you need to go through a proxy.
If you have a proxy available, run the following commands, then try `npm install` again.
1. `npm config set proxy http://path.to.proxy`
2. `npm config set https-proxy http://path.to.proxy`

## Mapping Mirth API channels to Swagger 
Mirth API channels are mapped to a swagger file by this NodeJS application using Mirths client APIs. For a channel to be mapped it needs to:

1. Be tagged with the tag: SWAGGER
2. Have a valid JSON object in its description field, containing swagger specific attributes, and a _full_channel_name_ attribute containing the name/URL to the channel using the following naming convention (METHOD service version endpoint), example: _GET nkk v1 documents_. For more information regarding the definition, visit the swagger website.

**NOTE** 
The _full_channel_name_ attribute is not a standard swagger attribute, but is added due to the restriction on the Mirth channel names to contain a maximum of 40 characters. Using this attributes allows for longer channel names/URLs. The _full_channel_name_ attribute should also be present in the CHANNEL_IDS field of the configurationMap (see Settings/Configuration Map).  

There is an image(example_mapping_in_mirth.png) in this folder that portrays how a channel should be setup for it to be mapped properly.

## Deploying to production
When deploying to production it is warmly recommened to use PM2 (http://pm2.keymetrics.io/) to run server. PM2 ensures that the application stays up, restarting it when errors occur. PM2 also offers resource management and is a convenient way to run any node application as a daemon. Steps follow:

1. Install pm2: `npm install pm2 -g`
2. Run the app from this folder: `pm2 start app` 

**NOTE**
If the server has been restarted or any environment variables have been changed, then pm2 will fail to recognize paths starting with `./`. 
To solve this problem, start/restart the pm2 process using the flag `--update-env`. Example command: `pm2 start app --update-env`.

## Reading pm2 logs 
To read the output from `console.log` use the command `pm2 logs`. By default, only the last 15 lines are displayed. Use the flag '--lines' plus the number of lines you would like to display. Example: `pm2 logs --lines 100` to display the last 100 lines.   

## Basefarm specific deployment
The following steps were provided by Basefarm, and should be used to reboot Swagger after any functionality has been updated. The 'swagger.zip' file mentioned is a zipped version of the folder where this README file is located.

1. Stop Swagger.
`systemctl stop pm2-ine-swagger.service;`

2. Change the name of the old swagger folder to 'swagger.old' and unpack the new 'swagger.zip' file.
`cd /opt/; mv swagger swagger.old; cp /tmp/swagger.zip /opt/; unzip swagger.zip`

3. Set up the new swagger functionality (install node modules etc.).
`chown -R ine-swagger:ine-swagger /opt/swagger; cd /opt/swagger; npm install ; npm install pm2 -g` 

4. Change to the 'ine-swagger' user and start the swagger application.
`su - ine-swagger -c "pm2 start app.js; pm2 save; pm2 delete 0"`

5. Run systemctl to start the service.
`systemctl start pm2-ine-swagger.service`

## Dockerized Swagger

### Setup
 * Since the Swagger instance is based on the Mirth Connect functionality, it is important to first start the Mirth docker container (see separate README). 
 * The Swagger configuration can be found in the ``config.js`` file. Change the following settings as follows:
    * `mirth.hostname`: Should be the name of the mirth container, usually `mirth`.
    * `mirth.port`: The container port specified when starting the mirth container, used to contact the Mirth User API.
    * `inera.hostname`: Needs to be set to the host that can be reached from outside the container scope. Usually `localhost`.
    * `inera.port`: The host port specified when starting the mirth container, used to call the custom Mirth APIs. 

### Commands to start
Execute the commands in order from this directory. The `<NETWORK_NAME>` should be set to whatever name you have chosen when creating a docker network (see the README in the main Docker folder for details).

1. Create a docker image from the Dockerfile: 
	* `docker build -t swagger-image .`
2. Create and run a Swagger container on port 8002, based on the Docker image created in step 1.
	* `docker run --name swagger -d -p 8002:8002 --network <NETWORK_NAME> swagger-image`