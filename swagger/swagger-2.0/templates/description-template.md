Genom vårt öppna API kan du hämta våra datakällor för användning på din webbplats eller i din mobilapp. Detta ger en möjlighet att återanvända den data som skapas inom den offenliga förvaltningen. Vi vill öka kännedomen om våra tjänster och sprida vårt kvalitetssäkrade innehåll för att skapa en bättre hälsa för alla. 

API är ett tekniskt gränssnitt som kan användas för att utveckla egna tillämpningar med innehåll från Inera. Genom ett standardiserat gränssnitt får du tillgång till bland annat kvalitetsindikatorer och mätvärden från vården, öppen organisationsinformation etc. Med hjälp av en teknisk nyckel ansluter du ditt system till API-lösningen och kan i egna kanaler spegla den data som Inera publicerar tillsammans med SKL. Det ger dina användare relevant och aktuell kunskap inom hälsa och vård.
     
#### Hur testar jag tjänsterna på denna sida? ####
1. Klicka på "Authorize" för att ange din tekniska nyckel, antingen som en header eller som en parameter. 
2. Klicka på den tjänst du önskar testa.
3. Tryck på "Try it out". 
4. Ange eventuella parametrar du vill skicka med anropet. 
5. Tryck på "Execute" för att skicka anropet.

#### Felsökning ####
- Microsoft Edge stöder för närvarande inte funktionaliteten på denna sida fullt ut. Vänligen använd en annan webbläsare för att testa API:erna.
- Om du som statuskod får meddelandet "Undocumented", prova följande steg:
	1. Kopiera adressen i "Request URL"-fältet.
	2. Öppna en ny flik i webbläsaren.
	3. Klistra in adressen och anropa den.
	4. Vid varning om certifikatets giltighet: Acceptera certifikatet.
	5. Nu kan du fortsätta testa API:erna. 
- Om du använder en Java-klient för att direkt anropa _MIRTH_URL_ och stöter på SSL-/certifikatproblem: _MIRTH_URL_ identifierar sig med SITHS funktionscertifikat vars certifikatkedja består av SITHS Type 3 CA v1 och SITHS Root CA v1. SITHS ingår i Windows truststore men inte i Java varför dessa måste läggas in i java truststore. Se mer på [https://www.inera.se/kundservice/dokument-och-lankar/tjanster/identifieringstjanst-siths/ca-certifikat-siths/](https://www.inera.se/kundservice/dokument-och-lankar/tjanster/identifieringstjanst-siths/ca-certifikat-siths/). Ineras plattform för öppna data kommer att gå över till EFOS certifikat under hösten. Detta bör lösa trust-problematiken.