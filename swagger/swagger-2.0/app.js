var express = require('express'),
    app = express(),
    fs = require('fs'),
    config = require('./config'),
    mirthToSwag  = require('./mirth-to-swagger');

var port = process.env.PORT || config.server.port;

// Perform untrusted requests
if (config.app.performUntrustedRequests) {
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
}

// Replace environment specific variables index.html to the ones specified in config.js
fs.readFile(config.swagger.swaggerIndexFileTemplate, 'utf8', function (err,data) {
    if (err) {
        console.error(err);
        process.exit(1);
    }

    if(!data){
        console.error("[app.js]: The data parameter is undefined.");
        process.exit(1);
    }
    var path = config.resource_server.scheme + '://' + config.resource_server.hostname + ':' + config.resource_server.port + '/' + config.swagger.outputfile;
    console.log('[app.js] PATH: '+ path);

    //Change the url to where the swagger-out file is found.
    var result = data.replace("_URL_", path);

    //Change the header title 
    result = result.replace("_TITLE_", config.swagger.header.title);

    fs.writeFile(config.swagger.swaggerIndexFile, result, 'utf8', function (err) {
        if (err) {
            console.error(err);
            process.exit(1);
        }
    });
});

// Replace the topbar settings used by index.html to the ones specified in config.js
fs.readFile(config.swagger.presetFileTemplate, 'utf8', function (err,data) {
    if (err) {
        console.error(err);
        process.exit(1);
    }

    if(!data){
        console.error("[app.js]: The data parameter is undefined.");
        process.exit(1);
    }
    var topbar = config.swagger.topbar;
    var json = {};
    json.url = topbar.url;
    json.logo = topbar.logo;
    json.text = topbar.text;
    json.visible = topbar.visible;
    var str = JSON.stringify(json);
    result = data.replace(/topbarSettings = .*;/g, 'topbarSettings = '+str+';');
    fs.writeFile(config.swagger.presetFile, result, 'utf8', function (err) {
        if (err) {
            console.error(err);
            process.exit(1);
        }
    });
});

// Replace the CKAN URL and Mirth URL placeholders in the description file with the urls specified in config.js
fs.readFile(config.swagger.descriptionFileTemplate, 'utf8', function (err,data) {
    if (err) {
        console.error(err);
        process.exit(1);
    }

    if(!data){
        console.error("[app.js]: The data parameter is undefined.");
        process.exit(1);
    }
    var ckanUrl = config.ckan.scheme + '://' + config.ckan.hostname;
    var ckanUrlPlaceHolder = config.ckan.urlPlaceHolder;
    result = data.replace(new RegExp(ckanUrlPlaceHolder,'g'), ckanUrl);

    var mirthUrl = config.inera.hostname;
    var mirthUrlPlaceholder = config.inera.urlPlaceHolder;
    result = result.replace(new RegExp(mirthUrlPlaceholder, 'g'), mirthUrl);
    
    fs.writeFile(config.swagger.descriptionFile, result, 'utf8', function (err) {
        if (err) {
            console.error(err);
            process.exit(1);
        }
    });
});

// Generate swagger.json used by Swagger UI
mirthToSwag.generateSwaggerJson();

// Start http server that hosts Swagger UI
app.use(express.static(__dirname + '/' + config.swagger.swaggerFolder));
app.listen(port, function(err) {
    console.log('Swagger documentation served on: %d', port);
});

