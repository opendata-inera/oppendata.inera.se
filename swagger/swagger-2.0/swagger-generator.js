/**
 * 
 * Generates the actual swagger.json file used by the web-gui. Swagger-generator.js has no
 * dependencies and accepts the an array of JSON channels into createJsonFile(channels). 
 * 
 * All configuration is picked up from config.js.
 * 
 */
var fs = require('fs'),
    _ = require('lodash'),
    config = require('./config');

/**
 * 
 * The flow (you can basically read from top to bottom): 
 * createJsonFile -> setBaseConfig -> addAllChannels -> writeFile
 * 
 */
var createJsonFile = function (channels, swaggerComponents) {
    var swagger = JSON.parse(fs.readFileSync(config.swagger.basefile, 'utf8'));
    setBaseConfig(swagger, function () 
    {
            addAllChannels(swagger, channels, function () 
            {
                addSwaggerComponents(swagger, swaggerComponents, function()
                {
                        writeFile(swagger);
                });
            });
    });
};

var setBaseConfig = function (swagger, callback) {
    var port = config.inera.port;
    if(port){
        port = ':' + port;
    }else{
        port = ''; 
    }
    var server = {};
    server.url = config.inera.scheme + '://' + config.inera.hostname + port + config.inera.basepath;
    swagger.servers.push(server);
    swagger.info.title = config.swagger.title;

    var description = fs.readFileSync(config.swagger.descriptionFile, 'utf8');
    swagger.info.description = description;
    swagger.info.version = config.swagger.version;
    swagger.info.contact.email = config.swagger.contact.email;
    swagger.info.contact.name = config.swagger.contact.name;

    callback();
};

var addAllChannels = function (swagger, channels, callback) {
    for (var j = 0; j < channels.length; j++) {
        var channel = channels[j];

        var definition = JSON.parse(channel.description);

        if(!definition.full_channel_name){
            console.error('No full_channel_name attribute found in "%s" channel description, needed to create a URL. Halting execution...', channel.name);
            process.exit(1);
        }
        
        var method = definition.full_channel_name.split(' ')[0].toLowerCase();
        var path = definition.full_channel_name.substring(method.length).replace(new RegExp(' ', 'g'), '/');
        delete definition.full_channel_name;
        path = path.replace(new RegExp('/--', 'g'), '/{').replace(new RegExp('--/', 'g'), '}/').replace(new RegExp('--$', 'g'), '}');

        if(swagger.paths[path] == null){
            swagger.paths[path] = {};
        }

        swagger.paths[path][method] = definition;


        for (var i = 0; i < definition.tags.length; i++) {
            var tagIndex = _.findIndex(swagger.tags, {'name': definition.tags[i]});
            if (tagIndex == -1) {
                swagger.tags.push({
                    name: definition.tags[i],
                    description: ''
                });
            }
        }
    }
    callback();
};

var addSwaggerComponents = function (swagger, components, callback) {
    if(!swagger.components){
        swagger.components = components;
    }else{
        for (var key in components) {
            if (components.hasOwnProperty(key)) {
                swagger.components[key] = components[key];
            }
        }
    }
    callback();
};

var writeFile = function (swagger) {

    var outputPath =  './' + config.swagger.swaggerFolder + '/' + config.swagger.outputfile;
    fs.writeFile(outputPath, JSON.stringify(swagger, null, 2), function (err) {
        if (err) {
            return console.log(err);
        }
        console.log('Writing Swagger JSON to ' + outputPath);
    });
};

module.exports = {
    createJsonFile: createJsonFile
};
