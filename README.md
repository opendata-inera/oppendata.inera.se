# README #

This README would normally document whatever steps are necessary to get your application up and running.

## What is this repository for? ##

This repository is for creating an open data platform using the open source product CKAN and integrating it to different sources
using the open source product Mirth Connect.
0.1 (Not officially released)
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

## Topics ##

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions


## Setup ##
### Docker
There is a docker configuration under the docker subfolder, see that folder for more information on installing the platform using Docker. 


### A. CKAN
####  A.1 Installing CKAN
Prerequisite for this step is an installed CKAN with all of its dependencies (Redis, Solr and Postgres). Instructions on installing CKAN can be found here: http://docs.ckan.org/en/ckan-2.7.0/maintaining/installing/install-from-source.html.

Steps to setup the configuration specific to us follows:
1. Create a user for CKAN using the web interface.
2. Login with that user.
3. Manually create the dataset that should contain the resource that you want to upload, in CKAN.
4. Navigate to your profile, note the API KEY, you will need this later.
5. Using curl create a resource definition and upload some data into it using curl (you will find example definition files under ckan folder): _curl -X POST https://INSERT-CKAN-URL>/api/3/action/datastore_create -H "Authorization: API KEY" -H "Content-Type: application/json" -d @INSERT-DEFINITION-FILE-PATH_

You should now be ready to start using CKAN. There is more info on setting up Mirth Connect to work with CKAN later in this README.

####  A.2 Installing CKAN on Windows 10 for development

To install on your Windows-10 machine you should use following guide with some changes:

https://github.com/ckan/ckan/wiki/How-to-Install-CKAN-2.5.2-on-Windows-7

* Instead of use CKAN version 2.5.2 you should use version 2.6.2.
* Install extension Datastore - see http://docs.ckan.org/en/latest/maintaining/datastore.html.
* After installation check out this getting started guide and create a admin user:
http://docs.ckan.org/en/latest/maintaining/getting-started.html#create-admin-user

When installing the Datastore extension make sure to create a new database in your Postgres server and create one user with admin rights and another with read rights (That's described in the datastore extension documentation above).

Check that your development.ini file contains following settings:

* ckan.plugins = stats text_view image_view recline_view datastore
* ckan.views.default_views = image_view text_view recline_view
* ckan.preview.direct = png jpg gif txt json xml
* ckan.preview.loadable = html htm rdf+xml owl+xml xml n3 n-triples turtle plain atom csv tsv rss txt json
* ckan.site_url = http://localhost:PORT (where PORT is the port specified in the [server:main] section).

The /ckan/ckanext/datastore/db.py file fails to parse any nested JSON data stored in CKAN datastore by default.
To fix this bug, make the following change:
1. Find the `convert(data, type_name)` function
2. Replace the line `return json.loads(data[0])` with `return json.loads(json.dumps(data[0]))`. 
Reference: https://stackoverflow.com/questions/36262753/python-json-typeerror-expected-string-or-buffer/36263033  

**Note:** There are two problems with this installation. 
1. The dataview in ckan web is not working.**TODO:** Test change the ckan.site_url setting in development.ini.
2. There is an error to read data that contains nested json format from the ckan datastore.
**TODO:** Test with SOLR 6.5

#### A.3 Workaround to use _pip install_ with a proxy 
In the guide for installing CKAN on Windows, the _pip install_ command needs to be used to install a virtual environment.
To allow this install when behind a proxy, use the following steps: 

1. Click the Windows search button.
2. Search for _Edit the system environment variables_.
3. Click _Environment variables_.
4. Under _System variables_, click _New_.
5. Add a variable named HTTP_PROXY, with the value http://path.to.proxy.server.
6. Add a variable named HTTPS_PROXY, with the value https://path.to.proxy.server.
7. Restart the computer. You should now be able to run _pip install_.
8. After finishing installing, remove the enivronment variables that were added.   

### B. Mirth Connect
The Mirth Connect User Guide contains information regarding config files and other functionality. The 3.5 version is located in the _./doc_ subfolder.   

After installation follow the following steps to get started (note that ports/urls/protocol might differ):
1. Download JNLP app(Mirth Connect Administrator) from http://localhost:9080/webadmin/Index.action
2. Start the app and connect to https://localhost:9443 using credentials admin/admin

**Note:** In case you dont have trusted certificates on your server (often the case for DEV servers) you will probably have some issues with HTTPS senders not working in mirth. To fix this issue install the sites certificate (pem file) into the Java key store (jks). This is a generic procedure and you can find resources online on how to do it.
**Note:** Version 3.5 of Mirth Connect is very picky regarding what Java version it accepts. The latest compatible Java version is Java 8, update 151 (1.8.0_151).

#### B.1. Importing 3rd party libraries
We use our own custom 3rd party libraries to provide custom functionality. These libraries are written in Java and need to be compiled and imported into Mirth before importing any of the created scripts. There are four _.jar_ files in total that should be imported into Mirth: _rest-client-*.jar_, _soap-client-*.jar_, _ssl-utility-*.jar_ and _parser-*-jar-with-dependencies.jar_. Follow the steps below to import the jar files to Mirth.  
1. Copy the all jar files from the _mirthLibraries_ subfolder into the _custom-lib_ subfolder in the Mirth base directory.
2. Verify that the user running mirth is the owner of the file and has sufficient rights to execute it.
3. Open mirth and navigate the menus to _settings -> resources -> reload resources_ , the lib should now be ready for use.

For more information and installation instructions please refer to the README in the base folder of each library.

#### B.2 Setting up Mirth
The Mirth Connect channels are the basis of all integrations done with mirth. Exports of these channels are in the folder _./mirthChannels/groups_. For more information about these channels please refer to the README in the _./mirthChannels_ subfolder.

The channels are exported and imported into Mirth Connect manually using the Mirth Connect Administrator.

1. For each exported channel group XML file under _./mirthChannels/groups_ go into mirth connect administrator and choose "channels" --> "import groups" and select the XML file. Once imported the channel groups should be visible on the first page.
2. We also need to import global scripts. Navigate to "channels" --> "Edit Global Scripts" --> "Import Scripts" and choose the file _./mirthChannels/other/global-scripts.xml_
3. Next, the code templates shared between scripts in Mirth should be imported. Navigate to "Channels" --> "Edit Code Templates" --> "Import Libraries" and choose the file _./mirthChannels/other/codeTemplates.xml_
4. We also need to set our global variables used within mirth under the global scripts. Navigate to globalscripts using the steps above and update the variables to reflect your environment.

Once this is done we should be able to enable and deploy the scripts and the platform should be ready to start accepting incoming traffic provided the environment is setup correctly along with firewall openings and the like.

##### Changing charset #####
If you are setting up Mirth Connect on a server where the default charset is _not_ UTF-8, the custom Java libraries (see section B.1) may not encode special characters (such as Swedish å, ä and ö) correctly. The charset used by Mirth Connect can be observed in the log files or in the Mirth Connect Administrator interface after starting the Mirth service. Example:

``[2018-07-10 16:21:31,646]  INFO  (com.mirth.connect.server.Mirth:448): Running Java HotSpot(TM) 64-Bit Server VM 1.8.0_151 on Windows 10 (10.0, amd64), derby, with charset UTF-8.``

To change the charset, add the line ``-Dfile.encoding=UTF8`` to the ``mcserver.vmoptions`` and the ``mcservice.vmoptions`` files, and restart the Mirth Connect service.


### C. Swagger
Swagger is a tool that can be accessed through a web browser to test APIs. Our Swagger implementation runs a script (mirth-to-swagger.js) on start that uses the Mirth API to fetch information about the Mirth channels that should be included in the Swagger instance. In order for a Mirth channel to be included in the Swagger instance, it needs to have a tag set to SWAGGER, and its channel description needs to be in a valid JSON format. 
For more information regarding the Swagger implementation, please see the Swagger README file in the _./swagger_ subfolder.   

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact