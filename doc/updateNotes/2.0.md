# 2018-06-04, Production Update 2.0 #

| component | description | reason | responsible |
|---|---|---|---|
| Logstash | Installed Logstash on Mirth import node. | Send the logs from the import APIs to Kibana. | Basefarm |
| Logstash | Updated the Mirth logstash-filter to allow parsing of JSON | Parse the logs from Mirth correctly. | Sopra Steria |
| Logstash | Updated the CKAN logstash-filter to fit the CKAN log format. | Reduce the number of parsing errors when logstash reads the CKAN logs. | Sopra Steria |
| Logstash | Set the proper read/write/execute-access on the directory where CKAN's log file is stored. | Allow for logstash to read the CKAN log and send it to Kibana. | Basefarm| 
| Mirth | Changed log format to JSON. | Allow for more flexible parsing of logs in Logstash and Kibana.  | Sopra Steria |
| Mirth | Moved the CKAN resource_search API functionality from 'CKAN get resource' channel to its own 'CKAN resource_search' channel. Renamed the former channel 'CKAN datastore_search'. | Make the channels more modular and easy to understand. | Sopra Steria |
| Mirth | Moved the responsibility of parsing a CKAN response from 'CKAN datastore_search' channel, to the respective APIs. 'CKAN datastore_search' now returning a raw CKAN response. | Give each API the possibility to customize the response.  | Sopra Steria |
| Mirth | Added a new response data format when the optional 'limit' parameter is present in an incoming API call. Returns a link to the next page of data. | Allow for better usability when requesting a subset of data. | Sopra Steria |
| Mirth | Added the Indikatorvarden APIs.  | Add ability to retrieve measurements, performingOrganizations, aggregatedQualityReports, reportingSystems and reportingOrganizations. | Sopra Steria |
| Mirth | Added the 'NTJP import' and 'NTJP load data from old platform' channels to Import channel group. | Add and update the resources in CKAN, used by Indikatorvarden APIs | Sopra Steria |
| Swagger | Added optional offset parameters to APIs that have optional limit parameters. Updated response data models to fit the new responses returned from Mirth when the limit parameter is present in the API call. | Conform to the new format of response data returned from Mirth when the limit parameter is present. | Sopra Steria | 