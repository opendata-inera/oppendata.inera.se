# 2018-04-23, Production Update 1.7 #

All functionality committed up to this point added to production environment with the following exceptions: 
1. (Mirth) NTJP import channels removed from Import group. 
2. (Mirth) Indikatorvärden group ommitted since NTJP import not yet implemented.