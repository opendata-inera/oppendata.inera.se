# Mirth Channels
This folder contains script files used by Mirth Connect.

## Directory structure

| path  | description   |
|---|---|
| channelDescriptions/ | Contains channel description JSON files for all Mirth APIs (copy & paste into a Mirth channel's _Channel Description_ field) |
| ckan_resource_definitions/  | Contains JSON files for each CKAN resource, describing the fields in the DB, and other CKAN specific information. The content of these files should be copied and added to the configuration.properties file (see below), for each resource present in CKAN. See _Configuration map example_ below.|
| groups/  | Contains Mirth channel groups including channels, in XML format. Each of them contains a number of channels categorized by what function they provide Found in webadmin under _Channels_.|
| other/codeTemplates.xml | A file containing code templates, providing functionality shared among Mirth channels. Found in webadmin under _Edit Code Templates_. |
| other/global-scripts.xml | A global script used for globally available variables. The values in this file are accessed from the other channels through the globalMap. The global script fetches the definitions for the ckan resources from the configurationMap (see configuration_maps below). Found in webadmin under _Edit Global Scripts_. |
| other/configuration_maps | Contains configuration files for the different environments (TEST, QA, PROD). The configuration file can be found in webadmin under _Settings-->Configuration Map_. The global-script will read certain variables from the configuration file on startup. |
| unused/ | Contains channel groups and other Mirth functtionality that has been developed but not currently in use in any of the environments. |
| README.md | Well, this file |

### Configuration map example
Example of how a CKAN resource definition file should be defined in the configurationMap (configuration.properties file).
|Key|Value|Comment|
|---|---|---|
|CKAN_RESOURCE_DEFINITION_KEYS|["DEF_CODES"]|Array containing all resources in CKAN. Used by global-script to set up resource information. |
|DEF_CODES|{"resource":{"package_id": "indikatorbeskrivningar", "name": "codes", "hash": "codes__"}...}|The definition of the codes resource.|
 
## Channel groups

### Router.xml
This group contains the API Router channel. It routes incoming requests to the right channels, based on their method (GET/POST) and the endpoint. Each endpoint is setup as a separate channel. The naming of each channel is critical. The API router parses the incoming request and forwards it to the corresponding API based on the name.

### CKAN.xml
Contains channels used to communicate with the CKAN API.

### Import.xml
Contains channels that are used to import data from various datasources, and send it to CKAN.

### HSA.xml
Contains channels that can be called to GET data from CKAN's HSA dataset.

### NKK.xml
Contains channels that can be called to GET data from CKAN's NKK dataset.

### Kvalitetsindikatorer.xml
Contains channels that can be called to GET data from CKAN's Kvalitetsindikatorer dataset.

### Indikatorvarden.xml
Contains channels that can be called to GET data from CKAN's Indikatorvarden dataset.

### Misc.xml
Contains a channel to POST new data or update current data in CKAN. Currently only used for testing purposes. 

### XML to JSON Utilities.xml
Contains functionality to convert XML data to JSON.

### Status.xml
Contains a channel that returns a message telling the caller that the API/Mirth is up and running.

### Monitoring.xml 
An import node specific channel group that is used to monitor a Mirth server's status. Used instead of the Router group on nodes whose sole purpose is to import data. Used by QA and PROD environments only.

## Further reading
For a more detailed description of the Mirth channels and their functionality, please read the _Implementerad funktionalitet i ÖDP.docx_ file located in the _./doc_ folder.