# Dockerized CKAN files
This folder contains everything needed to start containers running CKAN, Postgres, Solr and Redis.

## Versions
| CKAN | Solr | Redis | Postgres |
|---|---|---|---|
| 2.8.0 | 6.6.2 | 4.0.10 | 10.4 |

## Before starting
Before setting up CKAN, make sure that you have created a docker network (see the README file in the main docker folder). The network name that you have chosen should be inserted into the .env file under the `EXTERNAL_NETWORK_NAME` variable.

## Using Docker-compose to setup CKAN
By using the provided `docker-compose.yml` file you can get all the necessary components up and running quickly. Any configurations, environment variables etc. should be updated in the `.env` file. 

### Commands to start
Execute these commands in the folder where this README and `docker-compose.ýml` file are located.
1. `docker-compose up`. This command starts the Postgres, Solr, Redis and CKAN docker containers.
2. `docker exec ckan /usr/local/bin/ckan-paster --plugin=ckan datastore set-permissions -c /etc/ckan/default/development.ini | docker exec -i db psql -U <POSTGRES_USER>`. This command sets the permissions for the CKAN datastore. The `<POSTGRES_USER>` variable should be exchanged for the value of the 0`POSTGRES_USER` in the .env file.
3. `docker-compose restart ckan`. This command restarts the CKAN container and applies the changes to the `development.ini` file.
4. `<CKAN_SITE_URL>/api/3/action/datastore_search?resource_id=_table_metadata`. Run this commmand to see that the datastore has been setup properly and is returning some data.
5. `docker exec -it ckan /usr/local/bin/ckan-paster --plugin=ckan sysadmin -c /etc/ckan/default/development.ini add <YOUR_USER_NAME>`. Run this command to add a CKAN sysadmin user with the name of your choice. Follow the instructions on screen to setup your user details.  


## Using separate docker commands to setup CKAN

**NOTE**
These commands have not been tested since updatíng the docker-compose method above. The commands below should be updated to reflect the functionality and environment variables from the docker-compose setup, if there is a need to use the separate commands at all.  

### Postgres
Execute the commands in order from the /postgres subdirectory.
1. `docker build -t postgres:ckan .`
2. `docker run --name postgres -d -p 5432:5432 --network opendata -v postgres-data-volume:/var/lib/postgresql/data -e POSTGRES_PASSWORD=ckan -e POSTGRES_USER=ckan_default postgres:ckan`

### Solr
Execute the commands in order from the /solr subdirectory.
1. `docker build -t solr:ckan .`
2. `docker run --name solr -d -p 8983:8983 --network opendata solr:ckan`

### Redis
Execute the commands in order from the /redis subdirectory.
1. `docker build -t redis:ckan .`
1. `docker run --name redis -d -p 6379:6379 --network opendata redis:ckan`

### Ckan
Execute the commands in order from the /ckan subdirectory.
1. `docker build -t ckan:ckan .`
2. `docker run --name ckan -d -p 5000:5000 ckan:ckan`
