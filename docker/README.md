# Inera Opendata platform Docker

## Docker version compatibility
Describes the environment, docker application and docker version that the docker functionality in this folder has been tested with.

|environment|application|version|tested|working|date tested|notes|
|---|---|---|---|---|---|---|
| Windows 10 Pro | Docker for Windows |18.03.1-ce-win64|yes|yes|2018-09-10|-|

## Description
This repository contains all the opendata platform used by Inera. The platform consists of three components. To install these components and use them together you will need to start them in the following order: CKAN --> Mirth --> Swagger.

* Datastore - CKAN, an open source data portal software for displaying and adding data.
* Integration - Mirth Connect, an open source interface engine that collects data from providers and exposes an API for 3rd party integrations.
* API testing tool - Swagger, an open-source tool used to describe and test APIs.

**NOTE** This installation should be customized when deploying to production due to exposed ports, insecure configuration and no data persistency. For data persistency, read the 'Docker volumes and data persistency' section below.

## Install using docker

1. Install docker
2. Create a network with a name of your choice and remember the name. It will be used when installing the components in the next step.
`docker network create my-network-name`
3. Follow instructions in subfolders (ckan, mirth and swagger) to install all components separately.

During installation check container logs for error messages, there should be no errors. Once installation is complete verify that the following locations are accessible. Note that the ports may differ depending on the configurations you may have made in the configuration files for each component. These are the default values.
* Solr admin interface: http://localhost:8983/solr/#/
* Ckan site: http://localhost:5000/
* Mirth site: http://localhost:8080/webadmin/Index.action
* Swagger site: http://localhost:8002

## Useful docker commands
Below are some commands that might come in handy. For more details regarding docker commands, have a look at the [Docker documentation](https://docs.docker.com/).

### Legend
The abbreviations below are used in the commands in the following paragraphs. Note that these abbreviations are only used in this document and do not represent any type of standard notation.

|abbreviation|meaning|
|---|---|
|CID|container id|
|CN|container name|
|IID|image id|
|IN|image name|
|CMD|command|

The flags in the table below can be used with the `docker run` command.
|flag|description|example|
|---|---|---|
|`-d`|Used to start a docker container 'detached' (in the background), instead of the current terminal window.|`docker run -d redis`| 
|`-p`|Used to specify what ports to publish (if no ports are specified then the container will not be reachable from outside of the Docker service). Syntax: `host_port:container_port` |`docker run -p 80:5000`|
|`--name`|Used to specify the name of the container. Can be used to avoid running multiple containers of the same image, since no two containers can have the same name.|`docker run --name my-redis-container redis`|
|`--network`|Used to specify the network where the container should run. Networks are used to allow communication between containers. |`docker run --network my-docker-network redis`|
|`-v`| Used to specify a docker volume to use for a specific directory in the container. The syntax is `path-to-some-directory-to-use-as-volume:path-to-container-contents-that-should-be-stored-in-volume` | `docker run -v /my-laptop/data/:/opt/mirthconnect/appdata mirth-image`|

### Container commands
* List all running containers: `docker ps`
* List all containers: `docker ps -a`
* Get information about a container: `docker inspect <CN or CID>`
* Execute a command in the application running in the container: `docker exec <CN or CID> <CMD>`
* Access docker container: `docker exec -it <CN or CID> /bin/bash`
* Follow container logs: `docker logs -f <CN or CID>`
* Start container: `docker start  <CN or CID>`
* Stop container: `docker stop <CN or CID>`
* Remove container: `docker rm <CN or CID>` 

### Image commands
* List images: `docker images`
* Pull/download an image from a remote host: `docker pull <IN>`
* Create a new container from an image and execute the container: `docker run <IID>`. If the image is not found locally it will pull it from a remote server. 
* Remove image: `docker rmi <IN or IID>`

## Communication between containers
Docker containers are usually run in isolated environments, oblivious to the outside world. However, oftentimes we want our containers to be able to speak to each other. To allow this container-to-container communication, we make use of docker networks. Docker supports many different types of networks, but in this application we use what is called a 'user-defined bridge network'. To create a network and use it when running a container, follow these steps:
1. Create a network with a name of your choice.
`docker network create my-network-name`
2. Run your container in the network you have created.
`docker run --network my-network-name my-docker-image`

## Tips
* When using an `IID` or `CID` as a parameter, a substring of the id can be used instead.
For example, let's assume that we have run the command `docker images` to list our images, and received the following output:

    `REPOSITORY              TAG                 IMAGE ID            CREATED             SIZE`
    `hello-world             latest              2cb0d9787c4d        7 days ago          1.85kB`

    We see that the `IMAGE ID` is `2cb0d9787c4d`. If we want to remove the image we could type
`docker rmi 2cb0d9787c4d`, but using a substring of the id will also work: `docker rmi 2c`.

* Containers can contact each other using container names, as long as they are on the same network. For example, a request from the `swagger` container to port `8443` on the `mirth` container may look like this: `https:\\mirth:8443`.  

## Docker volumes and data persistency
By default, a docker container keeps all its files isolated from the outside world. However, this also means that if a container is destroyed, so are any data in the database, files created etc. In other words, there is no data-persistency. This is where docker volumes come into play. A docker volume will let you specify a place to store data outside the container, on your local machine or elsewhere, and create a link to these files from the container. This allows you to modify any data on the fly, and the data will still be there even if the container itself is destroyed. Using volumes is the recommended approach when setting up containers in a production environment, or when you want more customization options. For more information about volumes, read the documentation on [how to use docker volumes](https://docs.docker.com/storage/volumes/).

### Windows
The tips below are applicable when running Docker on Windows natively.
* Use Windows Powershell instead of command line to run docker commands. Some commands may not work otherwise.
* Unlike the Linux version, it is not possible to reach the IP address of a Docker container directly since the container is running in Hyper V virtualization. Instead, go through `localhost:<CONTAINER_PORT>` to reach the application running in the container.

## Building a custom docker image
A docker image is created by using a dockerfile. A dockerfile is a text file containing instructions that when executed build a docker image. The instructions are executed by running the `docker build` command. 

## Further reading
* [Docker documentation](https://docs.docker.com/) 
* [The Basics of the Run Command](https://blog.codeship.com/the-basics-of-the-docker-run-command/)
* [How to use Docker volumes](https://docs.docker.com/storage/volumes/)