#!/bin/bash
docker run --name redis -d -p 6379:6379 redis
cd ckan/postgres
docker build -t postgres:ckan .
docker run --name postgres -d -p 5432:5432 -e POSTGRES_PASSWORD=ckan -e POSTGRES_USER=ckan_default postgres:ckan
cd ../solr
docker build -t solr:ckan .
docker run --name solr -d -p 8983:8983 solr:ckan
cd ../ckan
docker build -t ckan:ckan .
docker run --name ckan  -d -p 5000:5000 -t --link solr:solr --link postgres:db ckan:ckan
cd ../../mirth
cd docker/mirth
docker build -t connect:mirth .
docker run --name mirth -d -p 9080:8080 connect:mirth
cd ../..
