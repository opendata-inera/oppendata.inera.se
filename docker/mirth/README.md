# Dockerized Mirth Connect for test
This folder contains everything needed to start Mirth Connect in a Docker container. Any Mirth channels, code templates and global scripts need to be added after starting the container, either through the webadmin interface or by using the Mirth Auto Update node.js application.

## Directory structure

| path  | description   |
|---|---|
| resources/ | Contains resources that should be included in the Mirth container. |
| resources/auth/ | Put any files containing API keys for external datasources here. |
| resources/ca/ | Put any server and client certificates used by Mirth here. |
| resources/custom-lib/ | Put any Java libraries (jars) that Mirth should use here.|
| volumes/ | May be used to persist data. Contains some example folders. |
| mirth.properties | Specifies the ports and other settings used by Mirth. |
| README.md | This file |
| Dockerfile | The file used to generate a docker image |
| .dockerignore | Specifies what files should be ignored when creating a docker image |
| docker-entrypoint.sh | A shell script used by the Dockerfile to run Mirth as a non-root user.|

## Data persistency
The Dockerfile in this folder was created to allow a quick setup of the Mirth Connect application. Any files (certificates, authorizations and custom Java libraries) will therefore be uploaded from the 'resources' folder to the Mirth container upon initialization. After the container has been created, there is no easy way to add files to the Mirth container from the outside, and if the container is destroyed, so are the uploaded files. If you need to persist data, use docker volumes (more information in the README in the parent folder).

## Setup
 * 	The Dockerfile contains three exposed ports. These ports are specified in the Dockerfile to explain what host ports to use (ports that can be connected to from outside the container), but the actual exposure of these ports is done in the `docker run` command below. The ports are: 
 	* `MIRTH_USER_API_PORT`: Port used to send calls to the Mirth User API.
 	* `MIRTH_WEBADMIN_PORT`: Port where the webadmin interface can be found.
 	* `MIRTH_API_PORT`: Port used to send calls to the custom APIs.

* The `mirth.properties` file in this directory also specifies open ports on the Mirth container:
	* `http.port`: The port used for the webadmin interface.
	* `https.port`: The port used for the Mirth User API. 

* The API Router channel in Mirth specifies a port used for incoming API calls. To facilitate the description of the ports in the run command below, we here call this port `API_ROUTER_PORT`.

* You may of course change any of the ports to suit your needs, but make sure to change them in all the places necessary.

## Commands to start
Execute the commands in order from this directory. The `<NETWORK_NAME>` should be set to whatever name you have chosen when creating a docker network (see the README in the main Docker folder for details). Choose one of the following options: Quick start or Volume start.  

### Quick start (no data persistency) 
Use this if you want to quickly get started. Any data created in the container will be deleted once the container is removed.
1. Add any (optional) authorization files, client/server certificates and Java libraries (jars) in the /resources folder
2. Create a docker image from the Dockerfile: 
	* `docker build -t mirth-image .`
3. Create and run a Mirth Connect container named 'mirth', specifying what ports to use.
	* `docker run --name mirth -d -p <MIRTH_WEBADMIN_PORT>:<http.port> -p <MIRTH_USER_API_PORT>:<https.port> -p <MIRTH_API_PORT>:<API_ROUTER_PORT> --network <NETWORK_NAME> mirth-image`

### Volume start (with data persistency) 
Use this if you want to setup docker volumes for persisting data. The data in the volumes will persist even if the container is removed.
1. Add any files you want to use to a folder of your choice(can also be added after the container has been started). There is a /volumes folder containing some example subfolders that you may use. 
2. Create a docker image from the Dockerfile: 
	* `docker build -t mirth-image .`
3. Create and run a Mirth Connect container named 'mirth', specifying what ports to use. Also add the volumes you want to use by using the `-v` flag. For example, to specify that the contents in the `/opt/mirthconnect/appdata` folder in the container, should be stored in the `/volumes/appdata` folder, use the following command: `-v $(pwd)/volumes/appdata:/opt/mirthconnect/appdata`. For Windows users, use the `${pwd}\volumes\appdata` path to the volume instead. Add another command for each directory in the container that you want to link to a volume. `pwd` fetches your current working directory.  
	* `docker run --name mirth -d -p <MIRTH_WEBADMIN_PORT>:<http.port> -p <MIRTH_USER_API_PORT>:<https.port> -p <MIRTH_API_PORT>:<API_ROUTER_PORT> --network <NETWORK_NAME>  -v <VOLUME>:<CONTAINER_DIRECTORY> mirth-image`
