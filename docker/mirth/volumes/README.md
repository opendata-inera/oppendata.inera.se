# Mirth Connect volumes
This folder contains a few empty folders that can be used as docker volumes to persist data by the Mirth Connect docker container.
All folders are named to reflect the names of the folders in the container.
Feel free to add any other folders that you would like to use for persisting data here. 

## Directory structure

| path  | description   |
|---|---|
| auth/ | Put any files containing API keys for external datasources here. |
| ca/ | Put any server and client certificates used when performing HTTP requests in Mirth. |
| custom-lib/ | Put any Java libraries (jars) that Mirth should use here. |
| appdata/ | Can be used to store the Mirth Database. Do not put any files here, Mirth will create the necessary data itself.|
| README.md | This file |
