var config = require('./config'),
    mirthUpdate  = require('./scripts/mirthUpdate');

// Perform untrusted requests
if (config.app.performUntrustedRequests) {
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
}

// Update the mirth functionality
mirthUpdate.start();
