var request = require('request'),
    fileHandler = require('./fileHandler'),
    readline = require('readline'),
    xmlBuilder = require('xmlbuilder'),
    config = require('../config'),
    paths = require('./paths'),
    status = require('./statusCodes'),
    contentTypeHeader = {'content-type' : 'application/xml'};

function createChannelGroupsFile(callback){
    var directory = config.directories.channelGroups;
    var channelGroupFilepaths = getFilepaths(directory);
    var outputfilePath = config.filepaths.autoCreatedChannelGroups;
    var successfullyCreatedChannelGroupsFile = fileHandler.tryCreateSingleFileFromMultiple(outputfilePath, channelGroupFilepaths, '<set>\n', '\n</set>');
    if(!successfullyCreatedChannelGroupsFile){
        console.log('[channelGroupsFileCreation]: Failed to create file from channelGroups. Halting execution...');
        process.exit(1);
    }
    console.log('[channelGroupsFileCreation]: Successfully created a file containing all channelGroups.');
    callback();
    
}

function getFilepaths(directory){
    console.log('[channelGroupsUpdate]: Reading channelGroup files from directory %s.', directory);
    var filenames = fileHandler.getFilepaths(directory);
    if(!filenames || filenames.length == 0){
        console.log('[channelGroupsUpdate]: No channelGroup files found. Halting execution...');
        process.exit(1);
    }
    console.log('[channelGroupsUpdate]: Found %s channelGroups to update:', filenames.length);
    for (var i = 0; i < filenames.length; i++) {
        console.log('   %s', filenames[i]);
    }
    return filenames;
}

module.exports = {
    create: createChannelGroupsFile
};