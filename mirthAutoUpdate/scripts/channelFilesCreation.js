var fileHandler = require('./fileHandler'),
    readline = require('readline'),
    xml2js = require('xml2js'),
    xmlBuilder = require('xmlbuilder'),
    config = require('../config'),
    onChannelsFound = null,
    onChannelsMissing = null;

function createChannelsXml(onChannelsFoundCallback, onChannelsMissingCallback){
    onChannelsFound = onChannelsFoundCallback;
    onChannelsMissing = onChannelsMissingCallback;
    console.log('[channelsFileCreation]: Extracting channels from channelGroups file...');
    var filepath = config.filepaths.autoCreatedChannelGroups;
    var channelGroupsXmlFile = fileHandler.readFileSync(filepath);
    if(channelGroupsXmlFile == null){
        console.log('[channelsFileCreation]: Failed to read channelGroup xml file. Halting execution...');
        process.exit(1);
    }
    createJsonFromXml(channelGroupsXmlFile, extractChannelsFromChannelGoups);
}

function createJsonFromXml(xml, callback){
    xml2js.parseString(xml, function (err, json) {
        if(err){
            console.log('[channelsFileCreation]: %s', err);
            console.log('Halting execution...');
            process.exit(1);
        }

        callback(json, createXmlFromJsonAndWriteToFile);
    });
}

function extractChannelsFromChannelGoups(json, callback){
    if(json.set && json.set.channelGroup && json.set.channelGroup.length > 0){
        var channelGroups = json.set.channelGroup;
        var extractedChannels = [];
        for(var i = 0; i < channelGroups.length; i++){
            if(channelGroups[i].channels){
                for(var j = 0; j < channelGroups[i].channels.length; j++){
                    for(var k = 0; k < channelGroups[i].channels[j].channel.length; k++){
                        var channelJson = {};
                        channelJson.channel = channelGroups[i].channels[j].channel[k];
                        extractedChannels.push(channelJson);
                    }
                }
            }        
        }
        if(extractedChannels.length == 0){
            console.log('[channelsFileCreation]: No channels found in channelGroups file. Skipping channels file creation and update...');
            onChannelsMissing();
        }else{
            console.log('[channelsFileCreation]: Extracted %s channels from channelGroups file:', extractedChannels.length);
             for (var i = 0; i < extractedChannels.length; i++) {
                console.log('   %s', getChannelName(extractedChannels[i]));
            }

            callback(extractedChannels);
        }

    }else{
        console.log('[channelsFileCreation]: channelGroups file did not contain any channelGroups. Halting execution...');
        process.exit(1);
    }
}

function getChannelName(channelJson){
    if(channelJson.channel.name && channelJson.channel.name.length > 0){
        return channelJson.channel.name[0];
    }
    return 'Channel name not found.';
}

function createXmlFromJsonAndWriteToFile(channels){
    var outputDir = config.directories.autoCreatedChannels;
    var builder = new xml2js.Builder();

    for(var i = 0; i < channels.length; i++){
        var outputFilepath = outputDir + channels[i].channel.id[0] + '.xml';
        var xml = builder.buildObject(channels[i]);
        if(!fileHandler.tryWriteFileSync(outputFilepath, xml)){
            console.log('[channelsFileCreation]: Failed to write channel xml file. Halting execution...');
            process.exit(1);
         }   
    }
    
    console.log('[channelsFileCreation]: Successfully printed %s files to %s directory.', channels.length, outputDir);

    onChannelsFound();
}

module.exports = {
    create:createChannelsXml
};