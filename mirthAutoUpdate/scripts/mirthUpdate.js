var config = require('../config'),
    login = require('./login'),
    configurationMapFileCreation = require('./configurationMapFileCreation'),
    configurationMapUpdate = require('./configurationMapUpdate'),
    configurationMapFileDeletion = require('./configurationMapFileDeletion'),
    globalScriptsUpdate = require('./globalScriptsUpdate'),
    channelDeletion = require('./channelDeletion'),
    channelGroupsFileCreation = require('./channelGroupsFileCreation'),
    channelGroupsUpdate = require('./channelGroupsUpdate'),
    channelFilesCreation = require('./channelFilesCreation'),
    channelsUpdate = require('./channelsUpdate'),
    channelFilesDeletion = require('./channelFilesDeletion'),
    channelGroupsFileDeletion = require('./channelGroupsFileDeletion'),
    codeTemplateLibrariesUpdate = require('./codeTemplateLibrariesUpdate'),
    codeTemplateFilesCreation = require('./codeTemplateFilesCreation'),
    codeTemplatesUpdate = require('./codeTemplatesUpdate'),
    codeTemplateFilesDeletion = require('./codeTemplateFilesDeletion'),
    redeployAllChannels = require('./redeployAllChannels'),
    reloadResource = require('./reloadResource');


var start = function(){
    var username = config.mirth.username;
    var password = config.mirth.password;
    login.login(username, password, startGlobalScriptsUpdate);
}


//Update the global scripts

function startGlobalScriptsUpdate(){
    if(config.updates.globalScripts){
        globalScriptsUpdate.update(startConfigurationMapFileCreation);    
    }else{
        console.log('[mirthUpdate]: Skipping globalScripts update...');
        startConfigurationMapFileCreation();
    }
}

//Update the configurationMap

function startConfigurationMapFileCreation(){
    if(config.updates.configurationMap){
        configurationMapFileCreation.create(startConfigurationMapUpdate);
    }else{
        console.log('[mirthUpdate]: Skipping configurationMap update...');
        startChannelDeletion();
    }
}

function startConfigurationMapUpdate(){
    configurationMapUpdate.update(startChannelDeletion);
}


// Update the server with new channels and channelGroups. Will delete all old channels before updating with new ones.

function startChannelDeletion(){
    if(config.updates.channelGroups){
        channelDeletion.delete(startChannelGroupsFileCreation);
    }else{
        console.log('[mirthUpdate]: Skipping channel deletion and update of new channels and channelGroups...');
        startCodeTemplateLibrariesUpdate();
    }
}

function startChannelGroupsFileCreation(){
    channelGroupsFileCreation.create(startChannelGroupsUpdate);
}

function startChannelGroupsUpdate(){
    channelGroupsUpdate.update(startChannelFilesCreation);
}

function startChannelFilesCreation(){
    channelFilesCreation.create(startChannelsUpdate, startCodeTemplateLibrariesUpdate);
}

function startChannelsUpdate(){
    channelsUpdate.update(startCodeTemplateLibrariesUpdate);
}

// Update codeTemplateLibraries and the codeTemplates they contain.

function startCodeTemplateLibrariesUpdate(){
    if(config.updates.codeTemplateLibraries){
         codeTemplateLibrariesUpdate.update(startCodeTemplateFilesCreation);
    }else{
        console.log('[mirthUpdate]: Skipping codeTemplateLibraries and codeTemplates update...');
        startRedeployAllChannels();
    }
}

function startCodeTemplateFilesCreation(){
    codeTemplateFilesCreation.create(startCodeTemplatesUpdate, startRedeployAllChannels);
}

function startCodeTemplatesUpdate(){
    codeTemplatesUpdate.update(startRedeployAllChannels);
}

function startRedeployAllChannels(){
    if(config.redeployAllChannelsAfterCompletion){
        redeployAllChannels.redeploy(startReloadResource);
    }else{
        startReloadResource();
    }
}

function startReloadResource(){
    if(config.reloadResource){
        var resourceId = config.resourceId;
        reloadResource.reload(resourceId, startDeletionOfAutoCreatedFiles);
    }else{
        console.log('[mirthUpdate]: Skipping reloading of resource containing Java libraries...');
        startDeletionOfAutoCreatedFiles();
    }
}

function startDeletionOfAutoCreatedFiles(){
    if(config.deleteAutoCreatedFilesAfterCompletion){
        if(config.updates.configurationMap){
            configurationMapFileDeletion.delete();
        }
        if(config.updates.channelGroups){
            channelGroupsFileDeletion.delete();
            channelFilesDeletion.delete();
        }
        if(config.updates.codeTemplateLibraries){
            codeTemplateFilesDeletion.delete();
        }
    }else{
        console.log('[mirthUpdate]: Skipping deletion of auto created files...');
    }
    finish();
}

function finish(){
    console.log('[mirthUpdate]: Update finished successfully.');
}


module.exports = {
    start:start
};