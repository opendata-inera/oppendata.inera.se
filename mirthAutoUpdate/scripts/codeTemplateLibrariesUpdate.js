var request = require('request'),
    fileHandler = require('./fileHandler'),
    config = require('../config'),
    paths = require('./paths'),
    status = require('./statusCodes'),
    contentTypeHeader = {'content-type' : 'application/xml'};

function updateCodeTemplateLibraries(callback){
    console.log('[codeTemplateLibrariesUpdate]: Reading codeTemplateLibraries xml file...');
    var filepath = config.filepaths.codeTemplateLibraries;
    var xmlFile = fileHandler.readFileSync(filepath);
    if(xmlFile == null){
        console.log('[codeTemplateLibrariesUpdate]: Failed to read codeTemplateLibraries xml file. Halting execution...');
        process.exit(1);
    }
    sendUpdateCodeTemplateLibrariesRequest(xmlFile, callback);
}

function sendUpdateCodeTemplateLibrariesRequest(codeTemplateLibrariesXml, callback){
    if(codeTemplateLibrariesXml == null){
        console.log('[codeTemplateLibrariesUpdate]: codeTemplateLibraries xml was null. Halting execution...');
        process.exit(1);
    }
    console.log('[codeTemplateLibrariesUpdate]: Updating codeTemplateLibraries on server...');
    request({
            url:  paths.codeTemplateLibraries + '?override=true',
            headers: contentTypeHeader,
            body: codeTemplateLibrariesXml, 
            method: 'PUT', 
            jar: true}, 
        function (error, response, body) {
            if(!status.wasRequestSuccessful(error, response, status.ok)){
                console.error('[codeTemplateLibrariesUpdate]: Failed to update codeTemplateLibraries. Halting execution...');
                process.exit(1);
            }
            console.log('[codeTemplateLibrariesUpdate]: Successfully updated codeTemplateLibraries.');
            callback();
        }
    );
}

module.exports = {
    update:updateCodeTemplateLibraries
};