var fileHandler = require('./fileHandler'),
    config = require('../config');

function deleteChannelFiles(){
	var directory = config.directories.autoCreatedChannels;
	console.log('[channelFilesDeletion]: Deleting channel files from  directory %s ...', directory);
    if(!fileHandler.tryDeleteAllFilesSync(directory)){
        console.log('[channelFilesDeletion]: Failed to delete all channel files. Halting execution...');
        process.exit(1);
    }
    console.log('[channelFilesDeletion]: Success!');
}

module.exports = {
    delete: deleteChannelFiles
};