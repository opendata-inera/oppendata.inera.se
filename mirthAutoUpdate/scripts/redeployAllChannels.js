var request = require('request'),
    config = require('../config'),
    paths = require('./paths'),
    status = require('./statusCodes'),
    contentTypeHeader = {'content-type' : 'application/xml'};

function sendRedeployAllChannelsRequest(callback){
    console.log('[redeployAllChannels]: Redeploying all channels on the server...');
    request({
            url:  paths.redeployAllChannels + '?returnErrors=true',
            headers: contentTypeHeader,
            method: 'POST', 
            jar: true}, 
        function (error, response, body) {
            if(!status.wasRequestSuccessful(error, response, status.noContent)){
                console.error('[redeployAllChannels]: Failed to redeploy all channels. Halting execution...');
                process.exit(1);
            }
            console.log('[redeployAllChannels]: Successfully redeployed all channels.');
            callback();
        }
    );
}

module.exports = {
    redeploy:sendRedeployAllChannelsRequest
};