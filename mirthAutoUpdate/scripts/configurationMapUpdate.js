var request = require('request'),
    fileHandler = require('./fileHandler'),
    config = require('../config'),
    paths = require('./paths'),
    status = require('./statusCodes'),
    contentTypeHeader = {'content-type' : 'application/xml'};

function updateConfigurationMap(callback){
    console.log('[configurationMapUpdate]: Reading configurationMap xml file...');
    var filepath = config.filepaths.autoCreatedConfigurationMapXml;
    var xmlFile = fileHandler.readFileSync(filepath);
    if(xmlFile == null){
        console.log('[configurationMapUpdate]: Failed to read configuration map xml file. Halting execution...');
        process.exit(1);
    }
    sendUpdateConfigurationMapRequest(xmlFile, callback);
}

function sendUpdateConfigurationMapRequest(configurationMapXml, callback){
    if(configurationMapXml == null){
        console.log('[sendUpdateConfigurationMapRequest]: configurationMap xml was null. Halting execution...');
        process.exit(1);
    }
    console.log('[configurationMapUpdate]: Updating configurationMap on server...');
    request({
            url:  paths.configurationMap,
            headers: contentTypeHeader,
            body: configurationMapXml, 
            method: 'PUT', 
            jar: true}, 
        function (error, response, body) {
            if(!status.wasRequestSuccessful(error, response, status.noContent)){
                console.error('[configurationMapUpdate]: Failed to update configurationMap. Halting execution...');
                process.exit(1);
            }
            console.log('[configurationMapUpdate]: Successfully updated configurationMap.');
            callback();
        }
    );
}

module.exports = {
    update:updateConfigurationMap
};