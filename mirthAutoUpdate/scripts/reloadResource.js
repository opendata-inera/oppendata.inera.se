var request = require('request'),
    config = require('../config'),
    paths = require('./paths'),
    status = require('./statusCodes'),
    contentTypeHeader = {'content-type' : 'application/xml'};

function sendReloadResourceRequest(resourceId, callback){
    console.log('[reloadResource]: Reloading resource %s ...', resourceId);
    request({
            url:  paths.resources + '/' + resourceId + paths.reloadResourceEndpoint,
            headers: contentTypeHeader,
            method: 'POST', 
            jar: true}, 
        function (error, response, body) {
            if(!status.wasRequestSuccessful(error, response, status.noContent)){
                console.error('[reloadResource]: Failed to reload resource %s. Halting execution...', resourceId);
                process.exit(1);
            }
            console.log('[reloadResource]: Successfully reloaded resource.');
            callback();
        }
    );
}

module.exports = {
    reload:sendReloadResourceRequest
};