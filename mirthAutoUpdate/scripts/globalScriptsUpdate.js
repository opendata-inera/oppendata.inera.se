var request = require('request'),
    fileHandler = require('./fileHandler'),
    config = require('../config'),
    paths = require('./paths'),
    status = require('./statusCodes'),
    contentTypeHeader = {'content-type' : 'application/xml'},
    globalScriptsFilepath = config.filepaths.globalScripts;

function updateGlobalScripts(callback){
    console.log('[globalScriptsUpdate]: Reading globalScripts file...');
    var globalScriptsXml = fileHandler.readFileSync(globalScriptsFilepath);
    if(globalScriptsXml == null){
        console.log('[globalScriptsUpdate]: File error. Halting execution...');
        process.exit(1);
    }
    console.log('[globalScriptsUpdate]: globalScripts file read successfully.');
    sendUpdateGlobalScriptsRequest(globalScriptsXml, callback);
}

function sendUpdateGlobalScriptsRequest(globalScriptsXml, callback){
    console.log('[globalScriptsUpdate]: Updating globalScripts in Mirth...');
    request({
            url:  paths.globalScripts,
            headers: contentTypeHeader,
            body: globalScriptsXml, 
            method: 'PUT', 
            jar: true}, 
        function (error, response, body) {
            if(!status.wasRequestSuccessful(error, response, status.noContent)){
                console.error('[globalScriptsUpdate]: Failed to update globalScripts. Halting execution...');
                process.exit(1);
            }
            console.log('[globalScriptsUpdate]: Successfully updated globalScripts.');
            callback();
        }
    );
}

module.exports = {
    update:updateGlobalScripts
};