var request = require('request'),
    config = require('../config'),
    paths = require('./paths'),
    status = require('./statusCodes');


var loginToMirth = function (username, password, callback) {
    console.log('[loginToMirth]: Logging in to Mirth...');
    request({url: paths.login, 
        method: 'POST', jar: true, form: {username: username, password: password}}, 
        function (error, response, body) {
            if(!status.wasRequestSuccessful(error, response, status.ok)){
                console.error('[loginToMirth]: Login failed. Halting execution...');
                process.exit(1);
            }
            console.log('[loginToMirth]: Logged in!');
            callback();
        }
    );
};

module.exports = {
	login: loginToMirth
};