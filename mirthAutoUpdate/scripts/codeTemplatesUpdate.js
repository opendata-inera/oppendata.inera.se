var request = require('request'),
    fileHandler = require('./fileHandler'),
    config = require('../config'),
    paths = require('./paths'),
    status = require('./statusCodes'),
    contentTypeHeader = {'content-type' : 'application/xml'},
    codeTemplateIndex = 0,
    codeTemplateFilenames = [],
    onCompletionCallback = null,
    directory = config.directories.autoCreatedCodeTemplates;

function updateCodeTemplates(callback){
    onCompletionCallback = callback;
    codeTemplateFilenames = getFilenames(directory);
    sendRequestIfNotFinished();
}

function getFilenames(directory){
    console.log('[codeTemplatesUpdate]: Reading codeTemplate files from directory %s.', directory);
    var filenames = fileHandler.getFilenames(directory);
    if(!filenames || filenames.length == 0){
        console.log('[codeTemplatesUpdate]: No codeTemplate files found. Halting execution...');
        process.exit(1);
    }
    console.log('[codeTemplatesUpdate]: Found %s codeTemplates to update.', filenames.length);
    return filenames;
}

function sendRequestIfNotFinished(){
    if(codeTemplateIndex < codeTemplateFilenames.length){
        var filename = codeTemplateFilenames[codeTemplateIndex];
        var filepath = directory + filename;
        var codeTemplateXml = fileHandler.readFileSync(filepath);
        if(codeTemplateXml == null){
            console.log('[codeTemplatesUpdate]: codeTemplate xml file not found. Halting execution...');
            process.exit(1);
        }
        var codeTemplateId = filename.replace('.xml', '');
        codeTemplateIndex++;
        console.log('[codeTemplatesUpdate]: Adding codeTemplate %s / %s to server.', codeTemplateIndex, codeTemplateFilenames.length);
        sendUpdateCodeTemplatesRequest(codeTemplateId, codeTemplateXml, sendRequestIfNotFinished);
    }else{
        console.log('[codeTemplatesUpdate]: Successfully added all %s codeTemplates to server.', codeTemplateIndex);
        onCompletionCallback();
    }
}

function sendUpdateCodeTemplatesRequest(codeTemplateId, codeTemplateXml, callback){
    console.log('[codeTemplatesUpdate]: Adding codeTemplate to server...');
    request({
            url:  paths.codeTemplates + '/' + codeTemplateId + '?override=true',
            headers: contentTypeHeader,
            body: codeTemplateXml, 
            method: 'PUT', 
            jar: true}, 
        function (error, response, body) {
            if(!status.wasRequestSuccessful(error, response, status.ok)){
                console.error('[codeTemplatesUpdate]: Failed to add codeTemplate with id %s. Halting execution...', codeTemplateId);
                process.exit(1);
            }
            console.log('[codeTemplatesUpdate]: Success!');
            callback();
        }
    );
}

module.exports = {
    update: updateCodeTemplates
};