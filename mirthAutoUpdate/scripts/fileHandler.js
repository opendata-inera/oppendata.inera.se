var fs = require('fs');

function readFileSync(filepath){
    try{
        return fs.readFileSync(filepath);
    }catch(e){
        console.log('[readFileSync]: Failed to read file: '+e.message);
        console.log('Halting execution...');
        process.exit(1);
    }
}

function getFilepaths(directory){
    var filenames = getFilenames(directory);
    for(var i = 0; i < filenames.length; i++){
        filenames[i] = directory+filenames[i];    
    }
    return filenames;
}

function getFilenames(directory){
    try{
        return fs.readdirSync(directory);
    }catch(e){
        console.log('[getFilenames]: Failed to get files in directory: '+e.message);
        console.log('Halting execution...');
        process.exit(1);
    }
}

function tryWriteFileSync(filepath, content){
    try {
        fs.writeFileSync(filepath, content);
        return true;
    } catch (err) {
        console.log('[fileHandler]: %s', err.message);
        return false;
    }
}

function tryCreateSingleFileFromMultiple(outputFilepath, filepaths, prefix, suffix){
    if(!tryWriteFileSync(outputFilepath, prefix)){
        return false;
    }

    for(var i = 0; i < filepaths.length; i++){
        var content = readFileSync(filepaths[i]);
        if(content == null){
            return false;
        }
        if(!tryAppendFileSync(outputFilepath, content)){
            return false;
        }
    }

    if(!tryAppendFileSync(outputFilepath, suffix)){
        return false;
    }

    return true;
}

function tryDeleteAllFilesSync(directory){
    try{
        var files = getFilepaths(directory);
        if(files.length == 0){
            console.log('[tryDeleteAllFilesSync]: No files found in %s ', directory);
            return true;
        }
        for(var i = 0; i < files.length; i++){
            fs.unlinkSync(files[i]);
        }
        console.log('[tryDeleteAllFilesSync]: All files deleted successfully from %s.', directory);
        return true;
    }catch(e){
        console.log('[tryDeleteAllFilesSync]: Deletion failed: %s. ', e.message);
        return false;
    }
}

function tryDeleteFileSync(filepath){
    try{
        fs.unlinkSync(filepath);
        console.log('[tryDeleteFileSync]: Successfully deleted file %s.', filepath);
        return true;
    }catch(e){
        console.log('[tryDeleteFileSync]: Deletion failed: %s. ', e.message);
        return false;
    }
}

function tryAppendFileSync(filepath, content){
    try {
        fs.appendFileSync(filepath, content);
        return true;
    } catch (err) {
        console.log('[fileHandler]: %s', err.message);
        return false;
    }
}

module.exports = 
{
	createReadStream: fs.createReadStream,
	readFileSync: readFileSync,
    getFilepaths: getFilepaths,
    appendFileSync: fs.appendFileSync,
    tryCreateSingleFileFromMultiple: tryCreateSingleFileFromMultiple,
    tryWriteFileSync: tryWriteFileSync,
    getFilenames: getFilenames,
    tryDeleteAllFilesSync: tryDeleteAllFilesSync,
    tryDeleteFileSync: tryDeleteFileSync
};