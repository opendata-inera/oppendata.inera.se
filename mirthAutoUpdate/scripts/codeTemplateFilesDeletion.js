var fileHandler = require('./fileHandler'),
    config = require('../config');

function deleteCodeTemplateFiles(){
    var directory = config.directories.autoCreatedCodeTemplates;
    console.log('[codeTemplateFilesDeletion]: Deleting codeTemplate files from  directory %s ...', directory);
    if(!fileHandler.tryDeleteAllFilesSync(directory)){
        console.log('[codeTemplateFilesDeletion]: Failed to delete all codeTemplate files. Halting execution...');
        process.exit(1);
    }
    console.log('[codeTemplateFilesDeletion]: Success!');
}

module.exports = {
    delete: deleteCodeTemplateFiles
};