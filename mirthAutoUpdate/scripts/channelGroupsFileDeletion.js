var fileHandler = require('./fileHandler'),
    config = require('../config');

function deleteChannelGroupsFile(){
	var filepath = config.filepaths.autoCreatedChannelGroups;
	console.log('[channelGroupsFileDeletion]: Deleting channelGroups file from %s ...', filepath);
    if(!fileHandler.tryDeleteFileSync(filepath)){
        console.log('[channelGroupsFileDeletion]: Failed to delete channelGroups file. Halting execution...');
        process.exit(1);
    }
    console.log('[channelGroupsFileDeletion]: Success!');
}

module.exports = {
    delete: deleteChannelGroupsFile
};