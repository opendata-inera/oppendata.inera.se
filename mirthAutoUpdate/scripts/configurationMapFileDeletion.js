var fileHandler = require('./fileHandler'),
    config = require('../config');

function deleteConfigurationMapXmlFile(){
	var filepath = config.filepaths.autoCreatedConfigurationMapXml;
	console.log('[configurationMapFileDeletion]: Deleting configurationMap xml file from %s ...', filepath);
    if(!fileHandler.tryDeleteFileSync(filepath)){
        console.log('[configurationMapFileDeletion]: Failed to delete configurationMap xml file. Halting execution...');
        process.exit(1);
    }
    console.log('[configurationMapFileDeletion]: Success!');
}

module.exports = {
    delete: deleteConfigurationMapXmlFile
};