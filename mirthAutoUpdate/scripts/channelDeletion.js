var request = require('request'),
    StringBuilder = require('string-builder'),
    fileHandler = require('./fileHandler'),
    config = require('../config'),
    parseString = require('xml2js').parseString,
    paths = require('./paths'),
    status = require('./statusCodes'),
    formBoundary = 'WebKitFormBoundary7MA4YWxkTrZu0gW',
    contentTypeHeader = {'content-type' : 'multipart/form-data;boundary=----'+formBoundary},
    onCompletionCallback = null;

function deleteChannels(callback){
    onCompletionCallback = callback;
    sendGetChannelsRequest(assertChannelsExist);
}

function sendGetChannelsRequest(callback){
    console.log('[channelDeletion]: Fetching all channels from server...');
    request({
            url:  paths.channels, 
            method: 'GET', 
            jar: true}, 
        function (error, response, body) {
            if(!status.wasRequestSuccessful(error, response, status.ok)){
                console.error('[channelDeletion]: Failed to fetch channels. Halting execution...');
                process.exit(1);
            }
            var channelIds = parseChannelIds(response.body);
            callback(channelIds);  
        }
    );
}

function assertChannelsExist(channelIds){
    if(channelIds.length == 0){
          console.log('[channelDeletion]: No channels found on server. Skipping deletion...');
          onCompletionCallback();
    }else{
          console.log('[channelDeletion]: Found %s channels to delete:', channelIds.length);
          for (var i = 0; i < channelIds.length; i++) {
               console.log('   channelId: %s', channelIds[i]);
          }
          sendDeleteChannelsRequest(channelIds);
    }
}

function sendDeleteChannelsRequest(channelIds){
    console.log('[channelDeletion]: Deleting all channels from server...');
    var urlWithParams = createUrlWithChannelIdParameters(paths.channels, channelIds);

    request({
            url:  urlWithParams, 
            method: 'DELETE', 
            jar: true}, 
        function (error, response, body) {
            if(!status.wasRequestSuccessful(error, response, status.noContent)){
                console.error('[channelDeletion]: Failed to delete channels. Halting execution...');
                process.exit(1);
            }
            console.log('[channelDeletion]: Successfully deleted all channels from server.');
            console.log('[channelDeletion]: Checking that no channels exist on server after deletion...');
            sendGetChannelsRequest(assertNoChannelsExist);
        }
    );
}

function createUrlWithChannelIdParameters(url, channelIds){
    var sb = new StringBuilder(url);
    for(var i = 0; i < channelIds.length; i++){
        if(i == 0){
            sb.append('?channelId=');
        }else{
            sb.append('&channelId=');
        }
        sb.append(channelIds[i]);
    }
    return sb.toString();
}

function assertNoChannelsExist(channelIds){
    if(channelIds.length == 0){
        console.log('[channelDeletion]: No channels found on server after deletion. Deletion process finished.');
        onCompletionCallback();
    }else{
        console.log('[channelDeletion]: Deletion failed. Found %s channels on server after delete request sent:', channelIds.length);
        for (var i = 0; i < channelIds.length; i++) {
            console.log('   channelId: %s', channelIds[i]);
        }
        console.log('[channelDeletion]: Halting execution...');
        process.exit(1);  
    }
}


var parseChannelIds = function (xml) {
    var channelIds = [];
    parseString(xml, function (err, json) {
        if(err){
            console.log('[parseChannelIds]: %s', err);
            console.log('Halting execution...');
            process.exit(1);
        }
        if(json.list && json.list.channel){
            for (var i = 0; i < json.list.channel.length; i++) {
                var currentChannel = json.list.channel[i];
                channelIds.push(currentChannel.id);
            }
        }
    });
    return channelIds;
};

module.exports = {
    delete: deleteChannels
};