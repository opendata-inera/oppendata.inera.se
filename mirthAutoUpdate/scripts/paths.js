var config = require('../config');

//Mirth API URLs
var apiPath = config.mirth.scheme + '://' + config.mirth.hostname + ':' + config.mirth.port + config.mirth.basepath,
	loginUrl = apiPath + '/users/_login',
	channelsUrl = apiPath + '/channels',
	channelGroupsUrl = apiPath + '/channelgroups',
	channelGroupsUpdateUrl = apiPath + '/channelgroups/_bulkUpdate',
	globalScriptsUrl = apiPath + '/server/globalScripts',
	configurationMapUrl = apiPath + '/server/configurationMap',
	codeTemplateLibrariesUrl = apiPath + '/codeTemplateLibraries',
	codeTemplatesUrl = apiPath + '/codeTemplates',
	redeployAllChannelsUrl = apiPath + '/channels/_redeployAll',
	resources = apiPath + '/server/resources',
	reloadResourceEndpoint = '/_reload';



module.exports = {
        channels : channelsUrl,
        login : loginUrl,
        channelGroups : channelGroupsUrl,
        globalScripts : globalScriptsUrl,
        configurationMap : configurationMapUrl,
        channelGroupsUpdate: channelGroupsUpdateUrl,
        codeTemplateLibraries: codeTemplateLibrariesUrl,
        codeTemplates: codeTemplatesUrl,
        redeployAllChannels: redeployAllChannelsUrl,
        resources: resources,
        reloadResourceEndpoint: reloadResourceEndpoint
};