var request = require('request'),
    fileHandler = require('./fileHandler'),
    config = require('../config'),
    paths = require('./paths'),
    status = require('./statusCodes'),
    contentTypeHeader = {'content-type' : 'application/xml'},
    channelIndex = 0,
    channelFilepaths = [],
    onCompletionCallback = null;

function updateChannels(callback){
    onCompletionCallback = callback;
    var directory = config.directories.autoCreatedChannels;
    channelFilepaths = getFilepaths(directory);
    sendRequestIfNotFinished();
}

function getFilepaths(directory){
    console.log('[channelsUpdate]: Reading channel files from directory %s.', directory);
    var filenames = fileHandler.getFilepaths(directory);
    if(!filenames || filenames.length == 0){
        console.log('[channelsUpdate]: No channel files found. Halting execution...');
        process.exit(1);
    }
    console.log('[channelsUpdate]: Found %s channels to update:', filenames.length);
    for (var i = 0; i < filenames.length; i++) {
        console.log('   %s', filenames[i]);
    }
    return filenames;
}

function sendRequestIfNotFinished(){
    if(channelIndex < channelFilepaths.length){
        var filepath = channelFilepaths[channelIndex];
        var channelXml = fileHandler.readFileSync(filepath);
        if(channelXml == null){
            console.log('[channelsUpdate]: Channel xml file not found. Halting execution...');
            process.exit(1);
        }
        console.log('[channelsUpdate]: Updating channel file: %s', filepath);
        channelIndex++;
        sendUpdateChannelsRequest(channelXml, sendRequestIfNotFinished);
    }else{
        console.log('[channelsUpdate]: Successfully added all %s channels to server.', channelIndex);
        onCompletionCallback();
    }
}

function sendUpdateChannelsRequest(channelXml, callback){
    console.log('[channelsUpdate]: Adding channel to server...');
    request({
            url:  paths.channels,
            headers: contentTypeHeader,
            body: channelXml, 
            method: 'POST', 
            jar: true}, 
        function (error, response, body) {
            if(!status.wasRequestSuccessful(error, response, status.ok)){
                console.error('[channelsUpdate]: Failed to add channel. Halting execution...');
                process.exit(1);
            }
            console.log('[channelsUpdate]: Successfully added channel to server.');
            callback();
        }
    );
}

module.exports = {
    update: updateChannels
};