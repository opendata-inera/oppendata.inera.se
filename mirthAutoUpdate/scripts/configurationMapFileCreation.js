var fileHandler = require('./fileHandler'),
    readline = require('readline'),
    xmlBuilder = require('xmlbuilder'),
    config = require('../config'),
    configurationMapFilepath = config.filepaths.configurationMap;
    configurationPropertyAttribute = 'com.mirth.connect.util.ConfigurationProperty';
    onCompletionCallback = null;

function createConfigurationMapXml(callback){
    onCompletionCallback = callback;
    console.log('[configurationMapFileCreation]: Creating configurationMap xml file from properties file...');
    createXmlFromConfigurationMap(configurationMapFilepath,writeXmlToFile);
}


function createXmlFromConfigurationMap(configurationMapFilepath, callback){
    var stream = fileHandler.createReadStream(configurationMapFilepath); 
    stream.on('error', function(error){
        console.log('[configurationMapFileCreation]: '+error);
        callback(null);
    });

    const rl = readline.createInterface({
        input: stream 
    });

    var entries = [];
    var lineNumber = 1;
    var currentEntryIndex = 0;
    var previousLine = '';

    rl.on('line', function (line) {

        if(isComment(line)){
            if(isComment(previousLine)){
                console.log('Comment with missing key and value found on line %s', lineNumber-1);
                process.exit(1);
            }
            previousLine = line;

        }else if(isKeyAndValue(line)){
            var keyAndValue = getKeyAndValue(line);
            if(keyAndValue.length != 2){
                console.log('Failed to get key and value from line %s. Invalid format.', lineNumber);
                process.exit(1);
            }

            var json = createJsonObjectWithKeyAndValue(keyAndValue[0], keyAndValue[1]);
            if(isComment(previousLine)){
                previousLine = removeHashTagFromComment(previousLine);
                addCommentToEntryContainingKeyAndValue(json, previousLine);
            }
            entries.push(json);
            previousLine = '';
        }else{
            console.log('Invalid format on line %s.', lineNumber);
            process.exit(1);
        }

        lineNumber++;
    });

    rl.on('close', function(){
        console.log('[configurationMapFileCreation]: Entries found in configurationMap: %s', entries.length);
        var json = {};
        json.map = {};
        json.map.entry = entries;
        var xml = xmlBuilder.create(json);
        callback(xml.end({pretty: true}));
    });
}

function isComment(line){
    return (line.length != 0 && line[0] == '#') ? true : false;
}

function isJsonOrArray(line){
    if(line[0] == '{' && line[line.length-1] == '}' || line[0] == '[' && line[line.length-1] == ']'){
        return true;
    }
    return false;
}

function getCleanedUpJsonString(jsonString){
    var str = jsonString.replace(/\\n|\\t/g,'')
                        .replace(/\\"/g, '"');
    var json = JSON.parse(str);
    return JSON.stringify(json, null, 2);
}

function removeHashTagFromComment(line){
    var tmp = line.split('# ');
    if(tmp.length != 2){
        console.log('[configurationMapFileCreation]: Failed to split line on "# " for string %s.', line);
        process.exit(1);
    }
    line = tmp[1];
    return line;
}

function createJsonObjectWithKeyAndValue(key, value){
    if(isJsonOrArray(value)){
        value = getCleanedUpJsonString(value);
    }
    var json = {};
    json.string = key;
    json[configurationPropertyAttribute] = {};
    json[configurationPropertyAttribute].value = value;
    return json;
}

function isKeyAndValue(line){
    return (!isLineEmptyOrStartsWithBlankSpace(line) && line[0] != '#' && (line.indexOf(' = ') !== -1)) ? true : false;
}

function isLineEmptyOrStartsWithBlankSpace(line){
    if(line.length == 0 || line[0] == ' '){
        return true;
    }
    return false;
}

function getKeyAndValue(line){
    var keyAndValue = line.split(' = ');
    if(keyAndValue.length == 2){
        return keyAndValue;
    }
    console.log('[configurationMapFileCreation]: Failed to get key and value from string by splitting on " = ".');
    return [];
}

function containsKey(entry){
   return entry.string ? true : false;
}

function addCommentToEntryContainingKeyAndValue(entry, comment){
    if(!entry[configurationPropertyAttribute]){
        console.log('[configurationMapFileCreation]: Error. Expected a ConfigProperty attribute but found none: '+JSON.stringify(entry));
        process.exit(1);
    }
    entry[configurationPropertyAttribute].comment = comment;
}

function writeXmlToFile(configurationMapXml){
    if(configurationMapXml == null){
        console.log('[configurationMapFileCreation]: configurationMap xml was null. Halting execution...');
        process.exit(1);
    }

    var outputfilePath = config.filepaths.autoCreatedConfigurationMapXml;
    if(!fileHandler.tryWriteFileSync(outputfilePath, configurationMapXml)){
        console.log('[configurationMapFileCreation]: Failed to write configurationMap to file. Halting execution...');
        process.exit(1);
    }
    console.log('[configurationMapFileCreation]: Successfully created configurationMap xml file at %s.', outputfilePath);
    onCompletionCallback();
}

module.exports = {
    create:createConfigurationMapXml
};