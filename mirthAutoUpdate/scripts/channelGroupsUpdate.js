var request = require('request'),
    StringBuilder = require('string-builder'),
    fileHandler = require('./fileHandler'),
    config = require('../config'),
    paths = require('./paths'),
    status = require('./statusCodes'),
    formBoundary = 'WebKitFormBoundary7MA4YWxkTrZu0gW',
    contentTypeHeader = {'content-type' : 'multipart/form-data;boundary=----'+formBoundary};

function updateChannelGroups(callback){
    var filepath = config.filepaths.autoCreatedChannelGroups;
    var channelGroupsXml = fileHandler.readFileSync(filepath);
    if(channelGroupsXml == null){
        console.log('[channelGroupsUpdate]: File error. Halting execution...');
        process.exit(1);
    }
    console.log('[channelGroupsUpdate]: channelGroups file read successfully.');

    var sb = new StringBuilder();
    setStartOfRequestBody(sb);
    sb.appendLine(channelGroupsXml);
    setEndOfRequestBody(sb);
    //console.log(sb.toString());
    sendUpdateChannelGroupsRequest(sb.toString(), callback);
}

function setStartOfRequestBody(stringbuilder){
    stringbuilder.appendLine('------');
    stringbuilder.append(formBoundary);
    stringbuilder.appendLine('Content-Disposition: form-data; name="channelGroups"');
    stringbuilder.appendLine('Content-Type: application/xml');
    stringbuilder.appendLine();
}

function setEndOfRequestBody(stringbuilder){
    stringbuilder.appendLine();
    stringbuilder.appendLine('------');
    stringbuilder.append(formBoundary);
    stringbuilder.appendLine('Content-Disposition: form-data; name="removedChannelGroupIds"');
    stringbuilder.appendLine('Content-Type: application/xml');
    stringbuilder.appendLine();
    stringbuilder.appendLine('<set/>');
    stringbuilder.appendLine();
    stringbuilder.appendLine('------');
    stringbuilder.append(formBoundary);
    stringbuilder.append('--');

}

function sendUpdateChannelGroupsRequest(requestBody, callback){
    console.log('[channelGroupsUpdate]: Updating channelGroups in Mirth...');
    request({
            url:  paths.channelGroupsUpdate + '?override=true',
            headers: contentTypeHeader,
            body: requestBody, 
            method: 'POST', 
            jar: true}, 
        function (error, response, body) {
            if(!status.wasRequestSuccessful(error, response, status.ok)){
                console.error('[channelGroupsUpdate]: Failed to update channelGroups. Halting execution...');
                process.exit(1);
            }
            console.log('[channelGroupsUpdate]: Successfully updated channelGroups.');
            callback();
        }
    );
}

module.exports = {
    update: updateChannelGroups
};