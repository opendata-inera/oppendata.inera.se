var fileHandler = require('./fileHandler'),
    readline = require('readline'),
    xml2js = require('xml2js'),
    xmlBuilder = require('xmlbuilder'),
    config = require('../config'),
    onCodeTemplatesFound = null,
    onCodeTemplatesMissing = null;

function createCodeTemplateFiles(onCodeTemplatesFoundCallback, onCodeTemplatesMissingCallback){
    onCodeTemplatesFound = onCodeTemplatesFoundCallback;
    onCodeTemplatesMissing = onCodeTemplatesMissingCallback;
    console.log('[codeTemplateFilesCreation]: Extracting codeTemplates from codeTemplateLibraries file...');
    var filepath = config.filepaths.codeTemplateLibraries;
    var codeTemplateLibrariesXmlFile = fileHandler.readFileSync(filepath);
    if(codeTemplateLibrariesXmlFile == null){
        console.log('[codeTemplateFilesCreation]: Failed to read codeTemplateLibraries xml file. Halting execution...');
        process.exit(1);
    }
    createJsonFromXml(codeTemplateLibrariesXmlFile, extractCodeTemplatesFromCodeTemplateLibraries);
}

function createJsonFromXml(xml, callback){
    xml2js.parseString(xml, function (err, json) {
        if(err){
            console.log('[codeTemplateFilesCreation]: %s', err);
            console.log('Halting execution...');
            process.exit(1);
        }

        callback(json, createXmlFromJsonAndWriteToFile);
    });
}

function extractCodeTemplatesFromCodeTemplateLibraries(json, callback){
    if(json.list && json.list.codeTemplateLibrary && json.list.codeTemplateLibrary.length > 0){
        var codeTemplateLibraries = json.list.codeTemplateLibrary;
        var extractedCodeTemplates = [];
        for(var i = 0; i < codeTemplateLibraries.length; i++){
            if(codeTemplateLibraries[i].codeTemplates){
                for(var j = 0; j < codeTemplateLibraries[i].codeTemplates.length; j++){
                    for(var k = 0; k < codeTemplateLibraries[i].codeTemplates[j].codeTemplate.length; k++){
                        var codeTemplateJson = {};
                        codeTemplateJson.codeTemplate = codeTemplateLibraries[i].codeTemplates[j].codeTemplate[k];
                        extractedCodeTemplates.push(codeTemplateJson);
                    }
                }
            }        
        }
        if(extractedCodeTemplates.length == 0){
            console.log('[codeTemplateFilesCreation]: No codeTemplates found in codeTemplateLibraries file. Skipping codeTemplates file creation and update...');
            onCodeTemplatesMissing();
        }else{
            console.log('[codeTemplateFilesCreation]: Extracted %s codeTemplates from codeTemplateLibraries file:', extractedCodeTemplates.length);
             for (var i = 0; i < extractedCodeTemplates.length; i++) {
                console.log('   %s', getCodeTemplateName(extractedCodeTemplates[i]));
            }

            callback(extractedCodeTemplates);
        }

    }else{
        console.log('[codeTemplateFilesCreation]: codeTemplateLibraries file did not contain any codeTemplateLibraries. Halting execution...');
        process.exit(1);
    }
}

function getCodeTemplateName(codeTemplateJson){
    if(codeTemplateJson.codeTemplate.name && codeTemplateJson.codeTemplate.name.length > 0){
        return codeTemplateJson.codeTemplate.name[0];
    }
    return 'CodeTemplate name not found.';
}

function createXmlFromJsonAndWriteToFile(codeTemplates){
    var outputDir = config.directories.autoCreatedCodeTemplates;
    var builder = new xml2js.Builder();

    for(var i = 0; i < codeTemplates.length; i++){
        var outputFilepath = outputDir + codeTemplates[i].codeTemplate.id[0] + '.xml';
        var xml = builder.buildObject(codeTemplates[i]);
        if(!fileHandler.tryWriteFileSync(outputFilepath, xml)){
            console.log('[codeTemplateFilesCreation]: Failed to write codeTemplate xml file. Halting execution...');
            process.exit(1);
         }   
    }
    
    console.log('[codeTemplateFilesCreation]: Successfully printed %s files to %s directory.', codeTemplates.length, outputDir);

    onCodeTemplatesFound();
}

module.exports = {
    create:createCodeTemplateFiles
};