var config = require('../config');

module.exports = {
	ok:'200',
	noContent:'204',
	wasRequestSuccessful: wasRequestSuccessful
}

function wasRequestSuccessful(error, response, expectedStatusCode){
    if(error){
        console.error('Error: '+error);
        return false;
    }else if(response.statusCode != expectedStatusCode){
        console.error('StatusCode: '+response.statusCode + '. Body: \n'+response.body);
        return false;
    }
    return true;
}