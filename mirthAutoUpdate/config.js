
//Set the root directory where the files are stored that should be used to update the Mirth server. This directory and subfolders (see README file) need to be created manually before running the application. 
var fileRootDirectory = './update';

module.exports = {
    app: {
        performUntrustedRequests: true
    },

 //   These settings specify where the base Mirth APIs are located
    mirth: {
       scheme: 'https',
       hostname: 'localhost',
       port: 8443,
       basepath: '/api',
       username: 'admin',
       password: 'admin'
   },

   directories:{
        fileRoot : fileRootDirectory,
        channelGroups: fileRootDirectory + '/channelGroups/',
        autoCreatedChannels: fileRootDirectory + '/autoCreatedFiles/channels/',
        autoCreatedCodeTemplates: fileRootDirectory + '/autoCreatedFiles/codeTemplates/'
   },


   //Paths to the files used to update the server.
   filepaths:{
        globalScripts: fileRootDirectory + '/global-scripts.xml',
        codeTemplateLibraries: fileRootDirectory + '/codeTemplates.xml',
        configurationMap: fileRootDirectory + '/configuration.properties',
        autoCreatedChannelGroups: fileRootDirectory + '/autoCreatedFiles/channelGroups.xml',
        autoCreatedConfigurationMapXml: fileRootDirectory + '/autoCreatedFiles/configuration.xml' 
   },

   //Specify which resources should be updated
   updates:{
        configurationMap: true,
        globalScripts: true,
        channelGroups: true,
        codeTemplateLibraries: true
   },

   //should the auto created files be removed from the local directory when finished?
   deleteAutoCreatedFilesAfterCompletion: true,
   //Should the channels on the server be redeployed after the update script has run? (if true, the redeploy will run even if new channels have not been added to the server)
   redeployAllChannelsAfterCompletion: true,
   //Have the Java libraries on the server (stored in what Mirth calls a "resource") been updated and need to be reloaded?
   reloadResource: true,
   //The id of the resource to reload
   resourceId: 'Default Resource'
};