# Mirth Auto Update
This is a node application that that runs an automated update of new functionality to the specified Mirth Connect server. Its purpose is to facilitate the update of channels, code templates, global scripts and configuration maps. Instead of using the Mirth Connect webadmin, you can use this application to perform update tasks through the REST api provided by Mirth. 

## IMPORTANT
Running this application will remove the old functionality on the Mirth server before adding the new functionality. Make sure to create a backup of the scripts that you want to keep before running the update, preferably through the Mirth webadmin application. 

## Mirth Connect version compatibility
Describes what versions of Mirth Connect this application is compatible with. 

| version | tested | working | notes |
|---|---|---|---|
| 3.5.0 | yes | yes |- |
| 3.6.0 | yes | yes |- |

## Directory structure

| path  | description   |
|---|---|
| node_modules | NodeJS dependencies for the project |
| scripts/  | This is where all the scripts that contact Mirth's REST API reside, together with some utility scripts  |
| update/  | This is where the configuration properties file, code template libraries XML file, and global scripts XML file should be placed (if you want to update them on the server)  |
| update/channelGroups | Place your channel group XML files (with channels included) here |
| update/autoCreatedFiles | Used to store the automatically created configuration XML file and channel groups file (do not add files here) |
| update/autoCreatedFiles/channels | Used to store the automatically created channel files (do not add files here) |
| update/autoCreatedFiles/codeTemplates | Used to store the automatically created code template files (do not add files here) |
| app.js | The main app. Can be run using _node app.js_ or _npm start_ |
| config.js | The main configuration file. Needs to be updated to reflect your environment and the resources in Mirth that you wish to update |
| package.json | NPM file to manage dependencies and project metadata |
| README.md | Well, this file |

### Note
The "update" directory and subfolders are not included in the repository and need to be created manually before running the application.

## Getting started
This NodeJS project uses NPM, a package manager for node, which makes setting this project up a breeze. Steps follow:
1. Install GIT
2. Install node
3. Checkout this codebase
4. Add the "update" directory in the basefolder and subfolders as explained above. Put the files you wish to update here.
5. If you want to keep any of your files currently on the server, make sure to back them up first. This update will remove the functionality currently on the server. 
   Add a "backup" directory with the same directory structure as the "update" directory if you want to quickly be able to change back to your old files.
6. In the basefolder (the folder where this README is located) run:
_npm install_
7. Update config.js to match your environment. For more documentation on configuration refer to the config.js file comments.
8. Start the application from the basefolder with the command: _npm start_ to start updating your Mirth server with new functionality.    

## Choosing what resources to update
In the config.js file there are options to pick and choose what resources to update. Under the "updates" attribute you can specify if you want to update the global scripts, the code templates, configuration map and channelGroups with included channels.

At the top of the config.js file you can choose from what folder to read the files. For example "update" or "backup" files.
You can also choose if you want to remove the automatically created files after the update has completed, and choose whether to redeploy the channels. 

## Adding Java libraries
Unfortunately, the Mirth REST API does not support uploading jar-files to the server. All external libraries need to be uploaded manually to the /custom-lib folder on the server.
